//const mongoConnectionString = 'mongodb://localhost:27017/application-filters';
const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a0-external.dcg93.fleet.mlab.com:41240/application-filters?authSource=admin'
// for UAT const mongoConnectionString = 'mongodb://ce-uat:S!gmA143@ds155030-a1-external.mlab.com:55030/application-filters?authSource=admin';
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');
const spinner = require('ora')('');

MongoClient.connect(mongoConnectionString)
	.then((db) => {
		var i = 0;
		const prepareUpdateOne = (map) => (done) => db.collection('status-management').find(
			{ 'EntityId': map.ApplicationNumber }
		).toArray()
			.then((apps) => {

				apps.map(value => {
					//console.log("Appid: " + map.ApplicationNumber + " appstatus : " + value.Status + " appfilterStatus:" + map.Status);
					if (value.Status != map.Status) {
						console.log("Appid: " + map.ApplicationNumber + " appstatus : " + value.Status + " appfilterStatus:" + map.Status);
					}
				});
				done(null);
			})
			.catch((e) => {
				console.log(e);
				done(null);
			});


		db.collection('application-filters').aggregate([
			{
				$match: {
					ApplicationNumber: { $gt: "0070568" }
				}
			},
			{
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
					"Status": "$StatusCode",
					"_id": 0
				}
			}
		]).toArray()
			.then((codeMap) => {
				//console.log(codeMap);
				console.log(`\n Total: ${codeMap.length}`);

				series(map(prepareUpdateOne)(codeMap), () => {
					console.log('done');
					db.close();

				});
			}).catch(function (e) {
				console.log(e.message);
			});
	})
	.catch((e) => {
		spinner.fail(e.message);
	});

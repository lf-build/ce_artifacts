const mongoConnectionString = 'mongodb://localhost:27017/applications';
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');
const spinner = require('ora')('');

MongoClient.connect(mongoConnectionString)
    .then((db) => {
        let success = 0;
        let failed = 0;

        const prepareUpdateOne = (map) => (done) => db.collection('application')
            .findOneAndUpdate(
            { "ApplicationNumber": map.ApplicationNumber },
            { $set: { "Source.TrackingCode": map.TrackingCode } },
			{ returnNewDocument: true }
				)
            .then(({ value }) => {
				if((value.ApplicationNumber !== map.ApplicationNumber && value.Source.TrackingCode !== map.TrackingCode)) {
				console.log(`ApplicationNumber: ${map.ApplicationNumber} -> ${value.ApplicationNumber}; Code: ${map.TrackingCode} -> ${value.Source.TrackingCode}`);					
				}
                success ++;
                spinner.text = (`Done: ${success}: ApplicationNumber: ${map.ApplicationNumber}`);
                done(null);
            })
            .catch(() => {
                failed ++;
                spinner.text = (`\Failed: ${failed}: ApplicationNumber: ${map.ApplicationNumber}`);
                done(null);
            });
        
        db.collection('store').aggregate([
            {
                $match: { "on-boarding.sign-up.application.trackingCode": { $nin: ["BorrowerPortal", null] } }
            },
            {
                $project: {
                    "ApplicationNumber": "$on-boarding.sign-up.application.applicationNumber",
                    "TrackingCode": "$on-boarding.sign-up.application.trackingCode",
                    "_id": 0
                }
            },
        ]).toArray()
            .then((codeMap) => {
                console.log(`\n Total: ${codeMap.length}`);
                spinner.start();
                series(map(prepareUpdateOne)(codeMap), () => {
                    db.close();
                    spinner.succeed(`Total: ${codeMap.length}, Success: ${success}, failed: ${failed}`);
                });
            });
    })
    .catch((e) => {
        spinner.fail(e.message);
    });

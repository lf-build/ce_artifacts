const mongoConnectionString = 'mongodb://192.168.1.71:27017/ETL_dumpV1';
//const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a0-external.dcg93.fleet.mlab.com:41240/applications?authSource=admin'
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');

var csvwriter = require('csvwriter');
const fs = require('fs');


MongoClient.connect(mongoConnectionString)
	.then((db) => {
		db.collection('app-filter-new').aggregate([
			//{
			//	$match: { "ApplicationNumber": { $gt: "0090864" } }
			//},
			{
				$sort: { "ApplicationNumber": 1 }
			},
			{
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
					"ApplicationDate": "$ApplicationSubmittedDate",
					"RequestedAmount": "$AmountRequested",
					"PurposeOfLoan": "$LoanPurpose",
					"ApplicantFirstName": "$ApplicantFirstName",
					"ApplicantMiddleName": "$ApplicantMiddleName",
					"ApplicantLastName": "$ApplicantLastName",
					"ApplicantBirthDate": "$ApplicantBirthDate",
					"ApplicantAadharNumber": "$ApplicantAadharNumber",
					"ApplicantPanNumber": "$ApplicantPanNumber",
					"Gender": "$Gender",
					"MaritalStatus": "$MaritalStatus",
					"ApplicantPhone": "$ApplicantPhone",
					"Monthly_Income": "$Monthly_Income",
					"ResidenceType": "$ResidenceType",
					"ApplicantPersonalEmail": "$ApplicantPersonalEmail",
					"ApplicantWorkEmail": "$ApplicantWorkEmail",
					"ApplicantEmployer": "$ApplicantEmployer",
					"TrackingCode": "$TrackingCode",
					"CurrentAddress_AddressLine1": "$CurrentAddress_AddressLine1",
					"CurrentAddress_AddressLine2": "$CurrentAddress_AddressLine2",
					"CurrentAddress_Location": "$CurrentAddress_Location",
					"City": "$City",
					"CurrentAddress_State": "$CurrentAddress_State",
					"CurrentAddress_PinCode": "$CurrentAddress_PinCode",
					"PermanentAddress_AddressLine1": "$PermanentAddress_AddressLine1",
					"PermanentAddress_AddressLine2": "$PermanentAddress_AddressLine2",
					"PermanentAddress_Location": "$PermanentAddress_Location",
					"PermanentCity": "$PermanentCity",
					"PermanentAddress_State": "$PermanentAddress_State",
					"PermanentAddress_PinCode": "$PermanentAddress_PinCode",
					"StatusName": "$StatusName",
					"StatusReasons": "$StatusReasons",
					"FinalOfferInterestRate": "$FinalOfferInterestRate",
					"FinalOfferProcessingFee": "$FinalOfferProcessingFee",
					"FinalOfferAmount": "$FinalOfferAmount",
					"BureauScore": "$BureauScore",
					"CompanyCategory": "$CompanyCategory",
					"ECN Number": "$ECNnumber",
					"_id": 0
				}
			}
		]).toArray()
			.then((codeMap) => {

				csvwriter(codeMap, function (err, csv) {
					//console.log(csv);
					fs.writeFileSync("Application_Dump_02022018.csv", csv, 'utf8');
					console.log("File write successfully........");
				});
			});
	})
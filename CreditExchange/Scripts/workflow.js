//const mongoConnectionString = 'mongodb://192.168.1.65:27017/workflow';
const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a0-external.dcg93.fleet.mlab.com:41240/workflow?authSource=admin'
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');

var csvwriter = require('csvwriter');
const fs = require('fs');

MongoClient.connect(mongoConnectionString)
	.then((db) => {
		db.collection('store').aggregate([
			{
				$match: {
					$and: [{ "on-boarding.application": { $exists: false } },
					{ "on-boarding.qualify.details": { $exists: true } }
					]
				}

			},
			{
				$sort: { "_id": 1 }
			},
			{
				$project: {
					"Application": "$on-boarding.sign-up.application",
					"Qualify": "$on-boarding.qualify",
					"MobileNumber": "$on-boarding.sign-up.mobile.mobileNumber",
					"Amount": "$on-boarding.opportunity.amount",
					"_id": 0
				}
			}
		]).toArray()
			.then((codeMap) => {
				//console.log(codeMap);
				csvwriter(codeMap, function (err, csv) {
					//console.log(csv);
					fs.writeFileSync("PROD_2_workflow_01022018.csv", csv, 'utf8');
					console.log("File write successfully........");
				});
			});
	})
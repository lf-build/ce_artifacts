const mongoConnectionString = 'mongodb://localhost:27017/companydb';
//const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a0-external.dcg93.fleet.mlab.com:41240/workflow?authSource=admin'
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');


const fs = require('fs');

MongoClient.connect(mongoConnectionString)
	.then((db) => {

		const prepareUpdateOne = (map) => (done) => db.collection('companymaster')
			.findOneAndUpdate(
			{ "Cin": map.cin },
			{ $set: { "BSNot": "Yes" } },
			{ returnNewDocument: true }
			)
			.then(({ value }) => {
				if ((value.Cin == map.cin)) {
					console.log(`Bsnot is there`);
				}
				done(null);
			})
			.catch(() => {
				done(null);
			});

		db.collection('bsnot').aggregate([
			{
				$project: {
					"cin": "$Cin",
					"_id": 0
				}
			},
		]).toArray()
			.then((codeMap) => {
				console.log(`\n Total: ${codeMap.length}`);
				series(map(prepareUpdateOne)(codeMap), () => {
					db.close();

				});
			});
	})
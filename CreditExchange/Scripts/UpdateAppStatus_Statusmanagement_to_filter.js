const mongoConnectionString = 'mongodb://192.168.1.71:27017/application-filters';
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');
const spinner = require('ora')('');

MongoClient.connect(mongoConnectionString)
	.then((db) => {
		var i = 0;

		// const prepareUpdateTwo = (data) => (done) => db.collection('application-filters').findOneAndUpdate(
		// 	{ "ApplicationNumber": data.EntityId },
		// 	{ $set: { "StatusCode": data.Status, "StatusName": "Rejected", "StatusReasons": data.Reason } },
		// 	{ returnNewDocument: true }
		// ).then(({ value }) => {
		// 	// console.log(map);
		// 	//console.log(data);
		// 	//console.log(value);
		// 	console.log(`Application -> ${data.EntityId}; updated successfully.`);
		// 	done(null);
		// })
		// 	.catch((e) => {
		// 		console.log(e.message);
		// 		done(null);
		// 	});


		const prepareUpdateOne = (map1) => (done) => db.collection('status-management').find(
			{ 'EntityId': map1.ApplicationNumber }
		).toArray()
			.then((apps) => {
				console.log(apps[0].Status != map1.Status);
				if (apps.length <= 0) {

					//console.log("Blank");
				} else {
					// apps.map(value => {
					// 	if (value.Status != map1.Status) {
					// 		//	console.log("Appid: " + map.ApplicationNumber + " appstatus : " + value.Status + " appfilterStatus:" + map.Status);
					// 		series(map(prepareUpdateTwo)(value), () => {
					// 			console.log('done two');
					// 			// db.close();
					// 		});
					// 	}
					// });

					if (apps[0].Status != map1.Status) {
						console.log("Appid: " + map1.ApplicationNumber + " appstatus : " + apps.Status + " appfilterStatus:" + map1.Status);
						// series(map(prepareUpdateTwo)(apps), () => {
						// 	//console.log('done two');
						// 	// db.close();
						// });

					}
				}


				done(null);
			})
			.catch((e) => {
				console.log(e.message);
				done(null);
			});


		db.collection('application-filters').aggregate([
			{
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
					"Status": "$StatusCode",
					"_id": 0
				}
			}
		]).toArray()
			.then((codeMap) => {
				console.log(codeMap);
				//console.log(`\n Total: ${codeMap.length}`);
				series(map(prepareUpdateOne)(codeMap), () => {
					//console.log('done one');
					db.close();

				});
			}).catch(function (e) {
				console.log(e.message);
			});
	})
	.catch((e) => {
		spinner.fail(e.message);
	});

//const mongoConnectionString = 'mongodb://192.168.1.71:27017/application-filters';
const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a0-external.dcg93.fleet.mlab.com:41240/application-filters?authSource=admin'
const { MongoClient } = require('mongodb');
const { map } = require('ramda');
const { series } = require('async');
const spinner = require('ora')('');


var Status = {
	"200.01": "Lead Created",
	"200.02": "Application Submitted",
	"200.03": "Initial Offer Given",
	"200.04": "Initial Offer Selected",
	"200.05": "Credit Review",
	"200.06": "Final offer made",
	"200.07": "Final offer Accepted",
	"200.08": "CE Approved",
	"200.09": "Bank Fraud Review",
	"200.10": "Bank Credit Review",
	"200.11": "Approved",
	"200.12": "Funded",
	"200.14": "Not Interested",
	"200.15": "Expired",
	"200.16": "Rejected"
};

const api_url = "https://services.qbera.com:9443/service-eventhub-eventhub/ApplicationStatusChanged";
const api_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDctMDhUMDk6NTI6NDMuMTIwODk2WiIsImV4cCI6IjIwMTctMDctMDhUMTA6NTI6NDMuMTIwNzA1WiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImNyZWRpdC1leGNoYW5nZSIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.7pg3ctuFBZA3DW5gbXYjSNNmQIEa7_BRMHV5kyCKws8";
var api = require('./api_module.js')(api_token);

MongoClient.connect(mongoConnectionString)
	.then((db) => {
		function saveRule(payload) {
			return new Promise(function (resolve, reject) {
				console.log("\r\nIn");
				api.post(api_url, payload).then(function (result) {
					console.log("\r\nResult : " + JSON.stringify(result));
					if (result.statusCode == 204) {
						console.log("\r\nINFO:Event raised successfully... ");
						resolve(result);
					} else {
						console.error("\r\n\r\nSave Rule Failed ");
						//reject(response);
						resolve(result);
					}
				});
			});
		}

		const prepareUpdateOne = (map1) => (done) => db.collection('status-management').find(
			{ 'EntityId': map1.ApplicationNumber }
		).toArray()
			.then((apps) => {
				console.log(JSON.stringify(map1));
				if (apps.length <= 0) {
					console.log("Blank");
				} else {
					if (apps[0].Status != map1.OldStatus) {

						var payload = {
							"EntityType": apps[0].EntityType,
							"EntityId": apps[0].EntityId,
							"OldStatus": map1.OldStatus,
							"NewStatus": apps[0].Status,
							"OldStatusName": map1.OldStatusName,
							"NewStatusName": Status[apps[0].Status],
							"Reason": apps[0].Reason,
							"ActiveOn": {
								"Time": "2017-12-15",
								"Hour": "2017-12-15-19-h",
								"Day": "2017-12-15-d",
								"Week": "2017-50-w",
								"Month": "2017-12-m",
								"Quarter": "2017-4-q",
								"Year": "2017-y"
							}
						};
						saveRule(payload)
							.then(() => {
								done(null);
							})
							.catch((e) => {
								console.log(e);
								done(null);
							});
					}
				}
				done(null);
			})
			.catch((e) => {
				console.log(e.message);
				done(null);
			});

		db.collection('application-filters').aggregate([
			{
				$match: {
					"ApplicationNumber": {
						$in: ["0012686", "0012762", "0012899", "0012946", "0013017", "0013557", "0013689", "0013776", "0014717", "0015149", "0015326", "0016385", "0017742", "0018051", "0018626", "0021069", "0021282", "0021670", "0057429", "0067056", "0067090", "0067171", "0067199", "0067249", "0067427", "0067578", "0067613", "0067679", "0067788", "0067881", "0067891", "0068028", "0068091", "0068123", "0068178", "0068220", "0068397", "0068601", "0068771", "0068825", "0068893", "0068936", "0068961", "0068970", "0069055", "0069064", "0069111", "0069327", "0069374", "0069381", "0069696", "0069756", "0069884", "0069938", "0070086", "0070248", "0070269", "0070311", "0070420", "0070568", " 0088444", " 0089253", "0095455", "0095584", "0095672", "0095673", "0096236", "0096239", "0096240", "0096248", "0096382", "0096408", "0096527", "0096548", "0096788", "0096799", "0096800", "0096876", "0096946", "0097079", "0097131", "0097213", "0097226", "0097243", "0097562", "0097754", "0097893", "0097903", "0097918", "0098028", "0098122", "0098127", "0098143"
						]
					}
				}
			},
			{
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
					"OldStatus": "$StatusCode",
					"OldStatusName": "$StatusName",
					"_id": 0
				}
			}
		]).toArray()
			.then((codeMap) => {

				//console.log(codeMap);
				//console.log(`\n Total: ${codeMap.length}`);
				series(map(prepareUpdateOne)(codeMap), () => {
					//console.log('done one');
					db.close();

				});
			}).catch(function (e) {
				console.log(e.message);
			});
	})
	.catch((e) => {
		spinner.fail(e.message);
	});

```
{
    "swaggerurls": [
        {
            "url": "http://{{external_url}}:6025/swagger/docs/swagger.json",
            "name": "ce-expiry-extension"
        },
        {
            "url": "http://{{external_url}}:7006/swagger/docs/swagger.json",
            "name": "ce-scorecard"
        },
        {
            "url": "http://{{external_url}}:5113/swagger/docs/swagger.json",
            "name": "ce-syndication-cibil"
        },
        {
            "url": "http://{{external_url}}:5208/swagger/docs/swagger.json",
            "name": "consent"
        },
        {
            "url": "http://{{external_url}}:5204/swagger/docs/swagger.json",
            "name": "application-document"
        },
        {
            "url": "http://{{external_url}}:5010/swagger/docs/swagger.json",
            "name": "tlsproxy"
        },
        {
            "url": "http://{{external_url}}:5106/swagger/docs/swagger.json",
            "name": "ce-syndication-lenddo"
        },
        {
            "url": "http://{{external_url}}:5199/swagger/docs/swagger.json",
            "name": "simulation"
        },
        {
            "url": "http://{{external_url}}:5101/swagger/docs/swagger.json",
            "name": "ce-syndication-perfios"
        },
        {
            "url": "http://{{external_url}}:5110/swagger/docs/swagger.json",
            "name": "ce-syndication-zumigo"
        },
        {
            "url": "http://{{external_url}}:5108/swagger/docs/swagger.json",
            "name": "ce-syndication-probe"
        },
        {
            "url": "http://{{external_url}}:5111/swagger/docs/swagger.json",
            "name": "ce-syndication-view-dns"
        },
        {
            "url": "http://{{external_url}}:6005/swagger/docs/swagger.json",
            "name": "ce-company-db"
        },
        {
            "url": "http://{{external_url}}:6001/swagger/docs/swagger.json",
            "name": "ce-application-filters"
        },
        {
            "url": "http://{{external_url}}:6003/swagger/docs/swagger.json",
            "name": "ce-application"
        },
        {
            "url": "http://{{external_url}}:6012/swagger/docs/swagger.json",
            "name": "ce-application-processor"
        },
        {
            "url": "http://{{external_url}}:6006/swagger/docs/swagger.json",
            "name": "ce-geoprofile"
        },
        {
            "url": "http://{{external_url}}:6004/swagger/docs/swagger.json",
            "name": "ce-applicant"
        },
        {
            "url": "http://{{external_url}}:5008/swagger/docs/swagger.json",
            "name": "configuration"
        },
        {
            "url": "http://{{external_url}}:5001/swagger/docs/swagger.json",
            "name": "security-identity"
        },
        {
            "url": "http://{{external_url}}:5014/swagger/docs/swagger.json",
            "name": "tenant"
        },
        {
            "url": "http://{{external_url}}:5203/swagger/docs/swagger.json",
            "name": "email-verification"
        },
        {
            "url": "http://{{external_url}}:5211/swagger/docs/swagger.json",
            "name": "sms"
        },
        {
            "url": "http://{{external_url}}:5212/swagger/docs/swagger.json",
            "name": "verification-engine"
        },
        {
            "url": "http://{{external_url}}:7003/swagger/docs/swagger.json",
            "name": "data-attributes-v3"
        },
        {
            "url": "http://{{external_url}}:5202/swagger/docs/swagger.json",
            "name": "mobile-verification"
        },
        {
            "url": "http://{{external_url}}:6020/swagger/docs/swagger.json",
            "name": "ce-eventdriven-notifications"
        },
        {
            "url": "http://{{external_url}}:5055/swagger/docs/swagger.json",
            "name": "template-manager"
        },
        {
            "url": "http://{{external_url}}:5059/swagger/docs/swagger.json",
            "name": "event-store"
        },
        {
            "url": "http://{{external_url}}:5032/swagger/docs/swagger.json",
            "name": "number-generator"
        },
        {
            "url": "http://{{external_url}}:5022/swagger/docs/swagger.json",
            "name": "activity-log"
        },
        {
            "url": "http://{{external_url}}:5050/swagger/docs/swagger.json",
            "name": "document-manager"
        },
        {
            "url": "http://{{external_url}}:5041/swagger/docs/swagger.json",
            "name": "lookup"
        },
        {
            "url": "http://{{external_url}}:5056/swagger/docs/swagger.json",
            "name": "document-generator"
        },
        {
            "url": "http://{{external_url}}:5112/swagger/docs/swagger.json",
            "name": "syndication-store"
        },
        {
            "url": "http://{{external_url}}:6002/swagger/docs/swagger.json",
            "name": "status-management"
        },
        {
            "url": "http://{{external_url}}:5076/swagger/docs/swagger.json",
            "name": "decision-engine"
        },
        {
            "url": "http://{{external_url}}:5060/swagger/docs/swagger.json",
            "name": "email"
        },
        {
            "url": "http://{{external_url}}:5058/swagger/docs/swagger.json",
            "name": "alert-engine"
        },
        {
            "url": "http://{{external_url}}:5065/swagger/docs/swagger.json",
            "name": "assignment"
        },
        {
            "url": "http://{{external_url}}:5011/swagger/docs/swagger.json",
            "name": "inbox"
        },
        {
            "url": "http://{{external_url}}:7102/swagger/docs/swagger.json",
            "name": "asset-manager"
        },
        {
            "url": "http://{{external_url}}:5004/swagger/docs/swagger.json",
            "name": "eventhub"
        },
        {
            "url": "http://{{external_url}}:5066/swagger/docs/swagger.json",
            "name": "eventhub-viewer"
        },
        {
            "url": "http://{{external_url}}:7042/swagger/docs/swagger.json",
            "name": "product-rule"
        },
        {
            "url": "http://{{external_url}}:8080/swagger/docs/swagger.json",
            "name": "static-server"
        },
        {
            "url": "http://{{external_url}}:7103/swagger/docs/swagger.json",
            "name": "image-processor"
        },
        {
            "url": "http://{{external_url}}:7052/swagger/docs/swagger.json",
            "name": "bl-filter-search"
        },
        {
            "url": "http://{{external_url}}:7053/swagger/docs/swagger.json",
            "name": "webhook-notifier"
        }
    ]
}
```


could you please 

commit this new configuration name as swagger-urls.json file in your ce-artifacts....in develop branch.

Thanks...
var path = require('path');
var fs = require('fs');
var config = require('../dev.json');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveTemplate(serviceUrl, templateFile, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(templateFile);
    var data = "";
    if (pathObject.ext.toLocaleLowerCase() == ".html") {
        var html = fs.readFileSync(templateFile, 'utf8').replace(/\"/g, '\\"').replace(/\r|\n|\t/g, ' ').replace('<<verification_url>>',config.verification_url);
        data = fs.readFileSync(templateFile.replace('.html', '.json'), 'utf8');
        data = data.replace("{{body}}", html);
    }
    console.log('Posting template.....');
    data = JSON.parse(data);
    return api.delete(serviceUrl + '/' + data.name + '/' + data.version + '/' + data.format, data).then(function (result) {
        if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
            console.log("Template Deleted " + data.name);
            return api.post(serviceUrl, data).then(function (result) {
                if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                    console.log("Template Published " + data.name);
                }
                else {
                    console.log("Failed to Publish template " + data.name);
                }
            });
        }
        else {
            console.error("Deleted template Failed for " + data.name + ",message:" + result.statusMessage + ",response:" + result.body);
        }
    });
}

function importTemplates(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var templateToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var pathObject = path.parse(files[i]);
        if (pathObject.ext.toLocaleLowerCase() == ".html") {
            templateToProcess.push(files[i]);
        }
    }

    templateToProcess = templateToProcess.map(r => () => saveTemplate(serviceUrl, r, token));

    return templateToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Template failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveTemplate: function (serviceUrl, templateFile) {
            return saveTemplate(serviceUrl, templateFile, token);
        },
        importTemplates: function (serviceUrl, folderPath) {
            return importTemplates(serviceUrl, folderPath, token);
        }
    }
}
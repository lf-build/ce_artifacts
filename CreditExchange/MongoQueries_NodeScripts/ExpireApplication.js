const { map } = require('ramda');
const { series } = require('async');
const MongoClient = require('mongodb').MongoClient;
const Long = require('mongodb').Long;

const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a1-external.dcg93.fleet.mlab.com:41240/application-filters?authSource=admin';
const eventhub_url = "https://services.qbera.com:9443/service-eventhub-eventhub/ApplicationExpired";
var epochTicks = 621355968000000000;
var ticksPerMillisecond = 10000;
var datetimeTimestamp = new Date();
var ticks = (datetimeTimestamp.getTime() * ticksPerMillisecond) + epochTicks;
console.log(ticks);

const api_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDctMDhUMDk6NTI6NDMuMTIwODk2WiIsImV4cCI6IjIwMTctMDctMDhUMTA6NTI6NDMuMTIwNzA1WiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImNyZWRpdC1leGNoYW5nZSIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.7pg3ctuFBZA3DW5gbXYjSNNmQIEa7_BRMHV5kyCKws8";
var api = require('./api_module.js')(api_token);


MongoClient.connect(mongoConnectionString)
	.then((db) => {

		function changeStatuses(application) {
			console.log(`\r\nIn Status Management for ${application.ApplicationNumber}`);
			var statusmanagement_url = `https://services.qbera.com:9443/service-status-management-status-management/application/${application.ApplicationNumber}/200.15`;
			var payload = {
				"Note": "",
				"reasons": ["r9"]
			};

			return api.post(statusmanagement_url, payload).then(function (statusResponse) {
				if (statusResponse.statusCode == 204) {
					console.log(`\r\nStatus Change successfully for ${application.ApplicationNumber} : ${new Date()}`);
					return api.post(eventhub_url, application).then(function (response) {
						if (response.statusCode == 204) {
							console.log(`\r\nEvent published for ${application.ApplicationNumber} : ${new Date()}`);
						} else {
							console.error(`\r\nFailed to publish event for ${application.ApplicationNumber}`);
						}
					});
				} else {
					console.error(`\r\nStatus not changed for ${application.ApplicationNumber} : ${new Date()}`);
					console.dir(statusResponse.body);
				}
			})
		}

		console.log(`Started fetching data from Application-filters DB`);

		db.collection('application-filters').aggregate([
			{
				$match: {
					"StatusCode": { $nin: ["200.12", "200.13", "200.14", "200.16", "200.15"] }
				}
			}
		]).toArray()
			.then((codeMap) => {
				var filterResult = codeMap.filter((x) => { return parseInt(x.ExpirationDate.Ticks) < ticks });
				 var i = 1;
				 filterResult.forEach(item => {
					if (item === null) {
						db.close();
					} else {
						setTimeout(function() {
							changeStatuses(item);
						}, 15000*i);
						i++;
					}
				  }) 
			}).catch(function (e) {
				db.close();
				console.log(e.message);
			})
	})
	.catch((e) => {
		db.close();
});
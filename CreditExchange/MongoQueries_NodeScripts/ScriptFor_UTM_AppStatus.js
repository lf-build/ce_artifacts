//const mongoConnectionString = 'mongodb://192.168.1.71:27017/status-management';
const mongoConnectionString = 'mongodb://ce-qa:hX69!JV3jP@ds141240-a1-external.dcg93.fleet.mlab.com:41240/status-management?authSource=admin&slaveOk=true';

const MongoClient = require('mongodb').MongoClient;
const { map } = require('ramda');
const { series } = require('async');

var csvwriter = require('csvwriter');
const fs = require('fs');
var newList = {};
var assList = [];
var tempList = [];
var uniqList = [];
var st='';

MongoClient.connect(mongoConnectionString)
.then((db) => {
    db.collection('status-management-history',{readPreference:'secondaryPreferred'}).aggregate([
        {
            $match: { "EntityId": { $in: [
                "0107518","0107521","0107523","0107524","0107526","0107529","0107531","0107532","0107535","0107537","0107539","0107541","0107542","0107543","0107549","0107550","0107552","0107554","0107556","0107558","0107559","0107560","0107562","0107565","0107566","0107568","0107570","0107574","0107575","0107576","0107578","0107579","0107580","0107582","0107583","0107584","0107585","0107586","0107588","0107589","0107593","0107595","0107596","0107598","0107601","0107602","0107604","0107608","0107609","0107613","0107615","0107617","0107618","0107619","0107622","0107623","0107625","0107626","0107628","0107629","0107632","0107634","0107636","0107637","0107639","0107642","0107643","0107644","0107646","0107647","0107649","0107656","0107659","0107661","0107662","0107674","0107685","0107689","0107697","0107702","0107707","0107720","0107726","0107727","0107728","0107729","0107733","0107734","0107735","0107739","0107740","0107741","0107742","0107743","0107744","0107745","0107747","0107748","0107749","0107754","0107757","0107758","0107762","0107765","0107769","0107770","0107780","0107781","0107785","0107789","0107795","0107796","0107802","0107809","0107813","0107815","0107816","0107827","0107829","0107834","0107836","0107837","0107846","0107859","0107860","0107861","0107862","0107863","0107865","0107868","0107871","0107872","0107875","0107876","0107878","0107879","0107881","0107882","0107885","0107886","0107887","0107888","0107890","0107892","0107894","0107895","0107896","0107897","0107898","0107899","0107900","0107904","0107905","0107906","0107907","0107908","0107909","0107910","0107911","0107912","0107914","0107916","0107917","0107920","0107923","0107925","0107926","0107927","0107928","0107929","0107935","0107936","0107937","0107938","0107939","0107940","0107941","0107942","0107943","0107947","0107949","0107950","0107951","0107953","0107954","0107958","0107959","0107960","0107961","0107962","0107964","0107965","0107967","0107968","0107969","0107971","0107973","0107974","0107976","0107979","0107980","0107981","0107983","0107984","0107985","0107988","0107990","0107992","0107993","0107994","0107996","0107998","0107999","0108000","0108001","0108005","0108006","0108008","0108009","0108011","0108014","0108018","0108019","0108023","0108027","0108028","0108029","0108030","0108031","0108032","0108033","0108037","0108039","0108040","0108041","0108043","0108044","0108045","0108046","0108047","0108048","0108049","0108050","0108051","0108052","0108054","0108059","0108062","0108063","0108066","0108068","0108069","0108070","0108071","0108072","0108073","0108075","0108076","0108082","0108085","0108086","0108087","0108088","0108089","0108091","0108092","0108093","0108095","0108096","0108097","0108098","0108099","0108100","0108102","0108103","0108108","0108109","0108110","0108112","0108115","0108116","0108117","0108118","0108119","0108121","0108122","0108123","0108124","0108125","0108127","0108128","0108129","0108131","0108132","0108133","0108137","0108138","0108140","0108141","0108142","0108143","0108147","0108148","0108149","0108150","0108153","0108154","0108156","0108157","0108159","0108161","0108162"
            ] } }
        },
        {
            $project: {
                "ApplicationNumber":"$EntityId",
                "Status":"$Status"
            }

        }
    ]).toArray()
    .then((codeMap) => {
                        //console.log(codeMap);
                        var convertedObjects = JSON.stringify(codeMap);
                        objects = JSON.parse(convertedObjects);

                       // console.log(objects);

                        for (var i=0; i < objects.length; i++) 
                        {
                            if(objects[i].Status=="200.01")
                            {
                                objects[i].Status="Lead Created";
                            }
                            else if(objects[i].Status=="200.02")
                            {
                                objects[i].Status="Application Submitted";
                            }
                            else if(objects[i].Status=="200.03")
                            {
                                objects[i].Status="Initial Offer Given";
                            }
                            else if(objects[i].Status=="200.04")
                            {
                                objects[i].Status="Initial Offer Selected";
                            }
                            else if(objects[i].Status=="200.05")
                            {
                                objects[i].Status="Credit Review";
                            }
                            else if(objects[i].Status=="200.06")
                            {
                                objects[i].Status="Final offer made";
                            }
                            else if(objects[i].Status=="200.07")
                            {
                                objects[i].Status="Final offer Accepted";
                            }
                            else if(objects[i].Status=="200.08")
                            {
                                objects[i].Status="CE Approved";
                            }
                            else if(objects[i].Status=="200.09")
                            {
                                objects[i].Status="Bank Fraud Review";
                            }
                            else if(objects[i].Status=="200.10")
                            {
                                objects[i].Status="Bank Credit Review";
                            }
                            else if(objects[i].Status=="200.11")
                            {
                                objects[i].Status="Approved";
                            }
                            else if(objects[i].Status=="200.12")
                            {
                                objects[i].Status="Funded";
                            }
                            else if(objects[i].Status=="200.14")
                            {
                                objects[i].Status="Not Interested";
                            }
                            else if(objects[i].Status=="200.15")
                            {
                                objects[i].Status="Expired";
                            }
                            else if(objects[i].Status=="200.16")
                            {
                                objects[i].Status="Rejected";
                            }
                           if(i > 0)     
                           var old =  objects[i-1].ApplicationNumber;
                          // console.log("Old:"+old) ;
                           if(old !=  objects[i].ApplicationNumber && old != undefined)
                           {
                               assList["ApplicationNumber"] = objects[i].ApplicationNumber;
                               assList[objects[i].Status] = 1;
                               newList = Object.assign({},assList);
                           }
                           else
                           {
                            newList["ApplicationNumber"] = objects[i].ApplicationNumber;
                            newList[objects[i].Status] = 1;                           
                           }
                           tempList.push(newList) ;            
                        }
                      //  console.log(tempList);
                        uniqList = [...new Set(tempList)];
                       // console.log(uniqList);
                       csvwriter(uniqList, function (err, csv) {
                                        console.log(csv);
                                        fs.writeFileSync("UTMNew_27122017.csv", csv, 'utf8');
                                        console.log("File write successfully........");
                                     });

                });

            });
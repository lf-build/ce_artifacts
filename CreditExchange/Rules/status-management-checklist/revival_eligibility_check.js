function revival_eligibility_check(payload) {
	var result = 'Failed';
	var Data = {};
	var eligibleStatus = ['200.14', '200.15', '200.16'];
	var reviveDaysLimit = 0;

	if (eligibleStatus.indexOf(payload.ApplicationStatus.Code) >= 0) {
		if (payload.Application.ApplicationDate && payload.Application.ApplicationDate.Time) {
			var appDate = new Date(payload.Application.ApplicationDate.Time);
			var todaysDate = new Date();
			var dayDiff = this.call('datedifference', ['D', appDate, todaysDate]);
			if (dayDiff >= reviveDaysLimit) {
				result = 'Passed';
			}
			Data = {
				'dayDiff': dayDiff
			};
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}

function ruleForPersonalEmailVerificationForChecklist(payload) {
     var factService=this.call('factVerification');
    function getVerificationFactDetails(input, factName) {
        for (var i = 0; i < input.length; i++) {
            if (input[i]['factName'] == factName) {
                return input[i];
            }
        }
    }
    if (typeof (payload) != 'undefined') {
        return factService.get(payload.entityType, payload.entityId).then(function (response) {
            var personalEmailVerification = getVerificationFactDetails(response, 'EmailVerifyForPersonal');
            if (typeof (personalEmailVerification) != 'undefined') {
                if (personalEmailVerification.currentStatus == 'Completed') {
                      return {
                    'result': true,
                    'detail': ''               
                };
                } else {
                      return {
                    'result': false,
                    'detail': 'Verification Pending'                
                };
                }
            } else {
                  return {
                    'result': false,
                    'detail': 'Verification Pending'                
                };
            }
        }).catch(function (error) {
              return {
                    'result': false,
                    'detail': error.message                
                };
        });
    } else {

          return {
                    'result': false,
                    'detail': 'Verification Pending'                
                };
    }



}

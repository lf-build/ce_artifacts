function ruleForAncillaryDataVerificationForChecklist(payload) {
    var dataAttributeService = this.call('dataAttributeVerification');
  
    if (typeof (payload) != 'undefined') {
        return dataAttributeService.get(payload.entityType, payload.entityId ,'ancillaryData').then(function (response) {
          
            if (typeof (response) != 'undefined' && response!= null ) {
                return {
                    'result': true,
                    'detail': ''                
                };
            } else {
                 return {
                    'result': false,
                    'detail': 'AncillaryData is not found'                
                };
            }
        }).catch(function (error) {
             return {
                    'result': false,
                    'detail': 'AncillaryData is not found'               
                };
        });
    } else {

         return {
                    'result': false,
                    'detail': 'AncillaryData is not found'                
                };
    }



}

function ruleForBankDetailsForChecklist(payload) {
	var dataAttributeService = this.call('dataAttributeVerification');

	if (typeof(payload) != 'undefined') {
		return dataAttributeService.get(payload.entityType, payload.entityId, 'application').then(function (response) {
			if (typeof(response) != 'undefined' && response != null) {
				if(response.bankInformation && response.bankInformation.length > 0){
					if((response.bankInformation[0].IfscCode && response.bankInformation[0].IfscCode.length) &&
					   (response.bankInformation[0].BankAddresses && response.bankInformation[0].BankAddresses.length)){
						return {
							'result': true,
							'detail': 'Required bank details are available'
						};
					}
					else {
						return {
							'result': false,
							'detail': 'Required bank details not found'
						};
					}
				}
				else {
					return {
						'result': false,
						'detail': 'Required bank details not found'
					};
				}
			}
			else {
				return {
					'result': false,
					'detail': 'Required bank details not found'
				};
			}
		}).catch (function (error) {
			return {
				'result': false,
				'detail': 'Required bank details not found'
			};
		});
	} else {

		return {
			'result': false,
			'detail': 'Required bank details not found'
		};
	}
}

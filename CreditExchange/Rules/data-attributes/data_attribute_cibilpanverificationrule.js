function data_attribute_cibilpanverificationrule (entityType, entityId, event) {
    var cibilReport = event.Data.Response.CibilReport;
    if (cibilReport == null) {
        return false;
    } else if (cibilReport.Pan == null) {
        return false;
    } else {
        if (isNaN(cibilReport.PanNameMatch) || isNaN(cibilReport.PanNoMatch)) {
            return false;
        } else {
            if (cibilReport.PanNameMatch >= 80 && cibilReport.PanNoMatch >= 100) {
                return true;
            } else {
                return false;
            }
        }
    }
}
function data_attribute_zumigovalidationonlinetransaction (entityType, entityId, event) {
    var result = {
        'zumigoReport': {
            type: event.Data.EntityType,
            name: event.Data.EntityId,
            averageDistance: event.Data.Response.AverageDistance,
            referenceNumber: event.Data.ReferenceNumber
        }  };
    return result;
}
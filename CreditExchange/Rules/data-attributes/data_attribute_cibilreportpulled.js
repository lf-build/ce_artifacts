function data_attribute_cibilreportpulled(entityType, entityId, event) {
	function GetSmallestDate(DateArray) {
		var SmallestDate = new Date(DateArray[0]);
		for (var i = 1; i < DateArray.length; i++) {
			var TempDate = new Date(DateArray[i]);
			if (TempDate < SmallestDate) {
				SmallestDate = TempDate;
			}
		}
		var month = SmallestDate.getMonth() + 1;
		var day = SmallestDate.getDate();
		var year = SmallestDate.getFullYear();
		return month + '/' + day + '/' + year;
	}
	function getpreviousmonthdate(currdate, monthcount) {

		var previousmonthdate = new Date(currdate.getFullYear(), currdate.getMonth() - monthcount, 1);
		return previousmonthdate;
	}
	function GetMultiplier(accountType) {
		var Multiplier = 0;
		switch (parseInt(accountType)) {
			case 2:
				Multiplier = 0.010;
				break;
			case 3:
				Multiplier = 0.015;
				break;
			case 1:
				Multiplier = 0.025;
				break;
			case 8:
				Multiplier = 0.025;
				break;
			case 32:
				Multiplier = 0.025;
				break;
			case 17:
				Multiplier = 0.025;
				break;
			case 5:
				Multiplier = 0.025;
				break;
			case 13:
				Multiplier = 0.035;
				break;
			case 50:
				Multiplier = 0.035;
				break;
			case 51:
				Multiplier = 0.035;
				break;
			case 61:
				Multiplier = 0.035;
				break;
			case 15:
				Multiplier = 0.09;
				break;
			case 12:
				Multiplier = 0.09;
				break;
			case 6:
				Multiplier = 0.125;
				break;
			case 7:
				Multiplier = 0.048;
				break;
			case 4:
				Multiplier = 0.048;
				break;
		}
		return Multiplier;
	};
	function CheckDPD(dpdtradehistory, startdate, dateClosed, dpdcountcheck, pulldate, monthrange) {
		var dpdCount = 0;
		var dpd180plus = false;
		var IsDPD = false;
		var charactercount = 0;
		var dpddate = new Date(startdate);
		var dpddaterange = new Date(pulldate);
		var pulldate = new Date(pulldate);
		var monthdifference = pulldate.getMonth() - dpddate.getMonth() + (12 * (pulldate.getFullYear() - dpddate.getFullYear()));
		if (monthdifference == 0) {
			dpddaterange = new Date(getpreviousmonthdate(dpddaterange, monthrange - 1));
		}
		if (monthdifference > 0) {
			dpddaterange = new Date(getpreviousmonthdate(dpddaterange, monthrange));
		}
		dpddaterange.setDate(1);
		var closedaterange = new Date(pulldate.toDateString());
		closedaterange = new Date(getpreviousmonthdate(closedaterange, monthrange));
		var strvalues = ['SMA', 'SUB', 'RES', 'LSS', 'DBT', '900'];
		for (var m = 0; m < dpdtradehistory.length - 1; m += 3) {
			charactercount = m;
			if ((!isNaN(parseInt(dpdtradehistory.substr(m, 3))))) {
				dpdCount = parseInt(dpdtradehistory.substr(m, 3));
			} else {
				dpdCount = dpdtradehistory.substr(m, 3);
			}
			if ((dateClosed >= closedaterange || dateClosed == null || dateClosed == '') && dpddate >= dpddaterange) {
				if (dpdCount >= dpdcountcheck || strvalues.indexOf(dpdCount) != -1) {
					IsDPD = true;
					if (dpdCount >= 180 || strvalues.indexOf(dpdCount) != -1) {
						dpd180plus = true;
					}
				}
			} else {
				break;
			}
			dpddate = new Date(getpreviousmonthdate(dpddate, 1));
		}
		if (dpd180plus == true) {
			for (var m = charactercount; m < dpdtradehistory.length - 1; m += 3) {
				if ((!isNaN(parseInt(dpdtradehistory.substr(m, 3))))) {
					dpdCount = parseInt(dpdtradehistory.substr(m, 3));
				} else {
					dpdCount = dpdtradehistory.substr(m, 3);
				}
				if (dpdCount >= 180 || strvalues.indexOf(dpdCount) != -1) {
					IsDPD = false;
				}
			}
		}
		return IsDPD;
	}
	var cibilBureauScore = 0;
	var creditCardBalance = 0;
	var creditCardLimit = 0;
	var noOfOpenTrades = 0;
	var revolvingTrades = 0;
	var bureauTenure = 0;
	var homeLoanIndicator = false;
	var pLInquiryInLast6months = 0;
	var noOfCreditCards = 0;
	var unsecuredloanbalance = 0;
	var unsecuredloanCreditLimit = 0;
	var agOfYoungestUnsecuredTrade = 0;
	var pl_count = 0;
	var sum_pl_limit = 0;
	var bureauEmi = 0;
	var avgPLLimit = 0;
	var fileType = null;
	var delinquencyFlag = false;
	var minDisbursedDate = null;
	var recentDisbursedDate = null;
	var isLiveloan = false;
	var isRBLDefaultLoan = false;
	var isRBLOverdue = false;
	var overdueamount = 0;
	var IsRBL = false;
	var isRBLPLInquiry = false;
	var isRBLDPDRule1 = false;
	var isRBLDPDRule2 = false;
	var isRBLDPDRule3 = false;
	var isDerogatoryStatus = false;
	var isHighDPDTrade = false;
	var isHighDPDTrade1 = false;
	var PanNoMatch = '';
	var PanNameMatch = '';
	var AddressMatch = false;
	var inquiryScore = 0;
	var accountinformation = [];
	var IsSecondaryReportExist = false;
	var enquiryDate = new Array();
	var creditInfomationReport = event.Data.Response.CreditInfomationReport;
	var cibilreport = event.Data.Response.CibilReport;
	var AdditionalFieldReport = event.Data.Response.AdditionalFieldReport;
	var reportDate = null;
	var highCreditOrSanctionedAmount = 0;
	if (typeof (AdditionalFieldReport) != 'undefined') {
		if (AdditionalFieldReport != null) {
			if (AdditionalFieldReport.length > 0) {
				IsSecondaryReportExist = true;
			}
		}
	}
	if (creditInfomationReport != null) {
		if (creditInfomationReport.ScoreSegment != null) {
			if (creditInfomationReport.ScoreSegment.ScoreName == 'CIBILTUSC2') {
				if (creditInfomationReport.ScoreSegment.Score == '000-1') {
					cibilBureauScore = -1;
				} else if (!isNaN(parseInt(creditInfomationReport.ScoreSegment.Score))) {
					cibilBureauScore = parseInt(creditInfomationReport.ScoreSegment.Score);
				}
			}
		}
		if (creditInfomationReport.DateProcessed != null) {
			var reportDatestr = creditInfomationReport.DateProcessed;
			if (reportDatestr != null) {
				reportDate = new Date(reportDatestr.substr(4, 4), reportDatestr.substr(2, 2) - 1, reportDatestr.substr(0, 2));
			}
		}
		var LastTwelveMonthEndDate = new Date(reportDate);
		LastTwelveMonthEndDate = new Date(getpreviousmonthdate(LastTwelveMonthEndDate, 12));
		if (creditInfomationReport.Account != null) {
			for (var k = 0; k < creditInfomationReport.Account.length; k++) {
				var disbursedDate = null;
				var flag = false;
				var datestr = creditInfomationReport.Account[k]['DateOpenedOrDisbursed'];
				if (datestr != null) {
					disbursedDate = new Date(datestr.substr(4, 4), datestr.substr(2, 2) - 1, datestr.substr(0, 2));
				}
				var dateClosed = null;
				var dateClosedstr = creditInfomationReport.Account[k]['DateClosed'];
				if (dateClosedstr != null) {
					dateClosed = new Date(dateClosedstr.substr(4, 4), dateClosedstr.substr(2, 2) - 1, dateClosedstr.substr(0, 2));
				}
				if (dateClosed == null || dateClosed > reportDate) {
					var accountinfo = {
						'AccountType': creditInfomationReport.Account[k]['AccountType'],
						'LoanAmount': creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount'],
						'CurrentBalance': creditInfomationReport.Account[k]['CurrentBalance'],
						'EMI': creditInfomationReport.Account[k]['EmiAmount'],
						'OpenDate': disbursedDate,
						'ClosedDate': dateClosed,
						'AmountOverDue': creditInfomationReport.Account[k]['AmountOverdue'],
						'W/offOrsettled': creditInfomationReport.Account[k]['WrittenOffAndSettled'],
						'SuitFiledOrWilfulDefault': creditInfomationReport.Account[k]['SuitFiledOrWilfulDefault']
					};
					accountinformation.push(accountinfo);
				}
				IsRBL = false;
				if (creditInfomationReport.Account[k]['ReportingMemberShortName'].toUpperCase() == 'RBL' || creditInfomationReport.Account[k]['ReportingMemberShortName'] == 'RBL BANK' || creditInfomationReport.Account[k]['ReportingMemberShortName'] == 'RBLBANK' || creditInfomationReport.Account[k]['ReportingMemberShortName'] == 'RATNAKAR BANK LTD') {
					IsRBL = true;
				}
				if (!isNaN(parseFloat(creditInfomationReport.Account[k]['AmountOverdue'])) && dateClosed == null) {
					overdueamount = overdueamount + parseFloat(creditInfomationReport.Account[k]['AmountOverdue']);
				}
				var writtenOffAndSettled = creditInfomationReport.Account[k]['WrittenOffAndSettled'];
				if (IsRBL) {
					if (dateClosed == null) {
						if (creditInfomationReport.Account[k]['AccountType'] == '05' || creditInfomationReport.Account[k]['AccountType'] == '41' || creditInfomationReport.Account[k]['AccountType'] == '06') {
							if (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4') {
								isLiveloan = true;
							}
						}
					}
					var suitFiledOrWilfulDefault = creditInfomationReport.Account[k]['SuitFiledOrWilfulDefault'];
					if (suitFiledOrWilfulDefault == '01' || suitFiledOrWilfulDefault == '02' || suitFiledOrWilfulDefault == '03' || writtenOffAndSettled == '00' || writtenOffAndSettled == '01' || writtenOffAndSettled == '02' || writtenOffAndSettled == '04' || writtenOffAndSettled == '06' || writtenOffAndSettled == '08' || writtenOffAndSettled == '09' || writtenOffAndSettled == '10') {
						isRBLDefaultLoan = true;
					}
				}
				var paymentHistoryStartDatestr = creditInfomationReport.Account[k]['PaymentHistoryStartDate'];
				var paymentHistoryStartDate = null;
				if (paymentHistoryStartDatestr != null) {
					paymentHistoryStartDate = new Date(paymentHistoryStartDatestr.substr(4, 4), paymentHistoryStartDatestr.substr(2, 2) - 1, paymentHistoryStartDatestr.substr(0, 2));
				}
				var paymentHistoryEndDatestr = creditInfomationReport.Account[k]['PaymentHistoryEndDate'];
				var paymentHistoryEndDate = null;
				if (paymentHistoryEndDatestr != null) {
					paymentHistoryEndDate = new Date(paymentHistoryEndDatestr.substr(4, 4), paymentHistoryEndDatestr.substr(2, 2) - 1, paymentHistoryEndDatestr.substr(0, 2));
				}
				var PaymentHistory1FieldLength = parseInt(creditInfomationReport.Account[k]['PaymentHistory1FieldLength']);
				var paymentHistory1 = creditInfomationReport.Account[k]['PaymentHistory1'] != null ? creditInfomationReport.Account[k]['PaymentHistory1'] : '';
				var paymentHistory2 = creditInfomationReport.Account[k]['PaymentHistory2'] != null ? creditInfomationReport.Account[k]['PaymentHistory2'] : '';
				var paymentHistory = paymentHistory1 + paymentHistory2;
				var dpddate = new Date(paymentHistoryStartDate.toDateString());
				isRBLDPDRule1 = isRBLDPDRule1 != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 15, reportDate, 1) : isRBLDPDRule1;
				isHighDPDTrade = isHighDPDTrade != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 30, reportDate, 3) : isHighDPDTrade;
				isHighDPDTrade1 = isHighDPDTrade1 != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 60, reportDate, 6) : isHighDPDTrade1;
				delinquencyFlag = delinquencyFlag != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 60, reportDate, 12) : delinquencyFlag;
				isRBLDPDRule2 = isRBLDPDRule2 != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 90, reportDate, 12) : isRBLDPDRule2;
				isRBLDPDRule3 = isRBLDPDRule3 != true ? CheckDPD(paymentHistory, paymentHistoryStartDate, dateClosed, 180, reportDate, 24) : isRBLDPDRule3;
				if ((creditInfomationReport.Account[k]['AccountType'] == '02' || creditInfomationReport.Account[k]['AccountType'] == '42') && dateClosed == null) {
					if (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4') {
						homeLoanIndicator = true;
					}
				}
				if (creditInfomationReport.Account[k]['AccountType'] == '10') {
					revolvingTrades = revolvingTrades + 1;
					if (writtenOffAndSettled != '02' && writtenOffAndSettled != '2') {
						if (dateClosed == null && (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4') && creditInfomationReport.Account[k]['CurrentBalance'] != null) {
							bureauEmi = bureauEmi + parseFloat(creditInfomationReport.Account[k]['CurrentBalance']) * 0.05;
						}
					}
					if (creditInfomationReport.Account[k]['CurrentBalance'] != null) {
						creditCardBalance = creditCardBalance + parseFloat(creditInfomationReport.Account[k]['CurrentBalance']);
					}
					if (parseFloat(creditInfomationReport.Account[k]['CurrentBalance']) > 500) {
						noOfCreditCards = noOfCreditCards + 1;
					}
					if (creditInfomationReport.Account[k]['CreditLimit'] != null) {
						creditCardLimit = (creditCardLimit != null ? creditCardLimit : 0) + parseFloat(creditInfomationReport.Account[k]['CreditLimit']);
					} else {
						if (creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount'] != null) {
							highCreditOrSanctionedAmount = (highCreditOrSanctionedAmount != null ? highCreditOrSanctionedAmount : 0) + parseFloat(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']);
							creditCardLimit = creditCardLimit + parseFloat(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']);
						}
					}
				} else {
					if (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4') {
						if (creditInfomationReport.Account[k]['CurrentBalance'] > 0 && writtenOffAndSettled != '02' && writtenOffAndSettled != '2') {
							if (dateClosed == null && !isNaN(creditInfomationReport.Account[k]['EmiAmount']) && creditInfomationReport.Account[k]['EmiAmount'] != null && creditInfomationReport.Account[k]['EmiAmount'] != '' && parseFloat(creditInfomationReport.Account[k]['EmiAmount']) > 0) {
								var loanSanctionedAmount = parseFloat(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']);
								var emiAmount = parseFloat(creditInfomationReport.Account[k]['EmiAmount']);
								var currentBalance = parseFloat(creditInfomationReport.Account[k]['CurrentBalance']);
								if (emiAmount > 0) {
									if ((currentBalance / emiAmount) > 3.1) {
										if ((emiAmount * 100) / loanSanctionedAmount <= 33) {
											bureauEmi = bureauEmi + emiAmount;
										}
									}
								}
							} else if (dateClosed == null && !isNaN(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']) && creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount'] != null) {
								var multiplier = GetMultiplier(creditInfomationReport.Account[k]['AccountType']);
								var ImputedEMI = parseFloat(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']) * multiplier;
								var currentBalance = parseFloat(creditInfomationReport.Account[k]['CurrentBalance']);
								var noofEmi = 1 / multiplier;
								var closingdate = new Date(disbursedDate);
								if (closingdate != null) {
									closingdate.setMonth(closingdate.getMonth() + noofEmi);
									if ((currentBalance / ImputedEMI) > 3) {
										if (!(creditInfomationReport.Account[k]['AccountType'] == '05' && closingdate < reportDate && currentBalance > 0)) {
											bureauEmi = bureauEmi + ImputedEMI;
										}
									}
								}
							}
						}
					}
				}
				if (creditInfomationReport.Account[k]['DateOpenedOrDisbursed'] != null) {
					if (disbursedDate <= reportDate && disbursedDate > LastTwelveMonthEndDate) {
						noOfOpenTrades = noOfOpenTrades + 1;
					}
				}
				if (creditInfomationReport.Account[k]['AccountType'] == '05' || creditInfomationReport.Account[k]['AccountType'] == '41' || creditInfomationReport.Account[k]['AccountType'] == '06' || creditInfomationReport.Account[k]['AccountType'] == '10') {
					if (parseFloat(creditInfomationReport.Account[k]['CurrentBalance']) >= 500 && (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4')) {
						if (recentDisbursedDate == null) {
							recentDisbursedDate = new Date(disbursedDate);
						} else if (disbursedDate > recentDisbursedDate) {
							recentDisbursedDate = new Date(disbursedDate);
						}
					}
				}
				if (disbursedDate != null) {
					if (minDisbursedDate == null) {
						minDisbursedDate = new Date(disbursedDate.toDateString());
					} else {
						if (disbursedDate < minDisbursedDate) {
							minDisbursedDate = new Date(disbursedDate.toDateString());
						}
					}
				}
				if (creditInfomationReport.Account[k]['AccountType'] == '05' || creditInfomationReport.Account[k]['AccountType'] == '41' || creditInfomationReport.Account[k]['AccountType'] == '06') {
					if (creditInfomationReport.Account[k]['OwenershipIndicator'] == '1' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '2' || creditInfomationReport.Account[k]['OwenershipIndicator'] == '4') {
						pl_count = pl_count + 1;
						if (creditInfomationReport.Account[k]['CurrentBalance'] != null) {
							unsecuredloanbalance = unsecuredloanbalance + parseFloat(creditInfomationReport.Account[k]['CurrentBalance']);
						}
						if (creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount'] != null) {
							unsecuredloanCreditLimit = unsecuredloanCreditLimit + parseFloat(creditInfomationReport.Account[k]['HighCreditOrSanctionedAmount']);
						}
					}
				}
			}
		}
		if (minDisbursedDate != null && reportDate != null) {
			bureauTenure = reportDate.getMonth() - minDisbursedDate.getMonth() + (12 * (reportDate.getFullYear() - minDisbursedDate.getFullYear()));
		}
		if (recentDisbursedDate != null) {
			agOfYoungestUnsecuredTrade = reportDate.getMonth() - recentDisbursedDate.getMonth() + (12 * (reportDate.getFullYear() - recentDisbursedDate.getFullYear()));
		}
		if (creditInfomationReport.Enquiry != null) {
			var expirydate = new Date(reportDate.toDateString());
			expirydate = new Date(getpreviousmonthdate(expirydate, 6));
			for (var k = 0; k < creditInfomationReport.Enquiry.length; k++) {
				var dateOfEnquirystr = creditInfomationReport.Enquiry[k]['DateOfEnquiryFields'];
				if (dateOfEnquirystr != null) {
					var dateOfEnquiry = new Date(dateOfEnquirystr.substr(4, 4), dateOfEnquirystr.substr(2, 2) - 1, dateOfEnquirystr.substr(0, 2));
					enquiryDate.push(dateOfEnquiry);
					if (dateOfEnquiry <= reportDate && dateOfEnquiry > expirydate) {
						if (creditInfomationReport.Enquiry[k]['EnquiryPurpose'].replace(' ', '') == '05' || creditInfomationReport.Enquiry[k]['EnquiryPurpose'].replace(' ', '') == '06' || creditInfomationReport.Enquiry[k]['EnquiryPurpose'].replace(' ', '') == '41') {
							if (creditInfomationReport.Enquiry[k]['EnquiringMemberShortName'].toUpperCase() == 'RBL' || creditInfomationReport.Enquiry[k]['EnquiringMemberShortName'] == 'RBL BANK' || creditInfomationReport.Enquiry[k]['EnquiringMemberShortName'] == 'RBLBANK' || creditInfomationReport.Enquiry[k]['EnquiringMemberShortName'] == 'RATNAKAR BANK LTD') {
								isRBLPLInquiry = true;
							}
							pLInquiryInLast6months = pLInquiryInLast6months + 1;
						}
					}
				}
			}
		}
	}
	if (pl_count > 0) {
		avgPLLimit = unsecuredloanCreditLimit / pl_count;
	}
	if (cibilBureauScore == -1) {
		fileType = 'NTC';
	} else if (cibilBureauScore == 0) {
		fileType = 'NotFound';
	} else if ((cibilBureauScore >= 1 && cibilBureauScore <= 5) || (bureauTenure < 6)) {
		fileType = 'Thin';
	} else {
		fileType = 'Thick';
	}
	if(event.Data.Response.PanMatch!=null)
		{
			PanNameMatch=event.Data.Response.PanMatch.PANNameMatch;
			PanNoMatch = event.Data.Response.PanMatch.PANNoMatch;
		}
	if (cibilreport != null) {
		
		
		if (cibilreport.DataSource != null) {
			if (cibilreport.DataSource.Address != null) {
				for (var k = 0; k < cibilreport.DataSource.Address.length; k++) {
					if (cibilreport.DataSource.Address[k]['AddressCategory'] == '01' || cibilreport.DataSource.Address[k]['AddressCategory'] == '02') {
						var dateReportedstr = cibilreport.DataSource.Address[k]['DateReported'];
						if (dateReportedstr != null) {
							var dateReported = new Date(dateReportedstr.substr(4, 4), dateReportedstr.substr(2, 2) - 1, dateReportedstr.substr(0, 2));
							var monthcount = reportDate.getMonth() - dateReported.getMonth() + (12 * (reportDate.getFullYear() - dateReported.getFullYear()));
							if (monthcount < 6) {
								if (!isNaN(cibilreport.DataSource.Address[k]['Match'])) {
									if (parseFloat(cibilreport.DataSource.Address[k]['Match']) > 70) {
										AddressMatch = true;
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	if (overdueamount >= 10000) {
		isRBLOverdue = true;
	}
	var minEnquiryDate = enquiryDate.length > 0 ? GetSmallestDate(enquiryDate) : null;
	var result = {
		'cibilReport': {
			'bureauScore': cibilBureauScore,
			'creditCardBalance': creditCardBalance,
			'creditCardLimit': creditCardLimit,
			'noOfOpenedTrades': noOfOpenTrades,
			'bureauTenure': bureauTenure,
			'homeLoan': homeLoanIndicator,
			'noOfPLInquiries': pLInquiryInLast6months,
			'noOfCreditCard': noOfCreditCards,
			'unsecuredLoanBalance': unsecuredloanbalance,
			'unsecuredLoanLimit': unsecuredloanCreditLimit,
			'youngestUnsecuredTrageAgeInMonths': agOfYoungestUnsecuredTrade,
			'delinquencyFlag': delinquencyFlag,
			'avg_PL_Limit': avgPLLimit,
			'bureauEmi': bureauEmi,
			'isDerogatoryStatus': isDerogatoryStatus,
			'fileType': fileType,
			'referenceNumber': event.Data.ReferenceNumber,
			'isRBLLive': isLiveloan,
			'isRBLDefaultLoan': isRBLDefaultLoan,
			'isRBLOverdue': isRBLOverdue,
			'isRBLPLInquiry': isRBLPLInquiry,
			'isRBLDPDRule1': isRBLDPDRule1,
			'isRBLDPDRule2': isRBLDPDRule2,
			'isRBLDPDRule3': isRBLDPDRule3,
			'panNameMatch': PanNameMatch,
			'panNoMatch': PanNoMatch,
			'addressMatch': AddressMatch,
			'inquiryScore': inquiryScore,
			'isHighDPDTrade': isHighDPDTrade,
			'isHighDPDTrade1': isHighDPDTrade1,
			'revolvingTrades': revolvingTrades,
			'sourceType': 'Cibil',
			'firstEnquiryDate': minEnquiryDate,
			'creditCardUtilization': (creditCardLimit != null || highCreditOrSanctionedAmount > 0) ? (creditCardBalance / ((creditCardLimit != null ? creditCardLimit : 0) + highCreditOrSanctionedAmount)) * 100 : 0,
			'highCreditOrSanctionedAmount': highCreditOrSanctionedAmount,
			'overdueamount': overdueamount,
			'accountinformation': accountinformation,
			'officeaddressMatch': false,
			'mobileMatch': false,
			'permanentaddressMatch': false,
			'IsSecondaryReportExist': IsSecondaryReportExist,
			'ecnnumber': event.Data.Response.ApplicationId
		}
	};
	return result;
};
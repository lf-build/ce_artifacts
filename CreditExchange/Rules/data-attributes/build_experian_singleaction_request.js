function build_experian_singleaction_request(payload, configuration) {
	var message = '';
	function getStateCode(state) {
		var stateCodeEnum = {
			jammukashmir: '01',
			himachalpradesh: '02',
			punjab: '03',
			chandigarh: '04',
			uttaranchal: '05',
			haryana: '06',
			delhi: '07',
			rajasthan: '08',
			uttarpradesh: '09',
			bihar: '10',
			sikkim: '11',
			arunachalpradesh: '12',
			nagaland: '13',
			manipur: '14',
			mizoram: '15',
			tripura: '16',
			meghalaya: '17',
			assam: '18',
			westbengal: '19',
			jharkhand: '20',
			orissa: '21',
			chhattisgarh: '22',
			madhyapradesh: '23',
			gujarat: '24',
			damananddiu: '25',
			dadraandnagahaveli: '26',
			maharashtra: '27',
			andhrapradesh: '28',
			karnataka: '29',
			goa: '30',
			lakshadweep: '31',
			kerala: '32',
			tamilnadu: '33',
			pondicherry: '34',
			andamanandnicobarislands: '35',
			telangana: '36',
		};
		var statestr = state.toLowerCase().replace(' ', '');
		if (statestr == 'jammu&kashmir' || statestr == 'jammukashmir') {
			return '01';
		} else if (statestr == 'westbengal') {
			return '19';
		} else if (statestr == 'daman&diu' || statestr == 'damandiu') {
			return '25';
		} else if (statestr == 'andhrapradesh') {
			return '28';
		} else if (statestr == 'himachalpradesh') {
			return '02';
		} else if (statestr == 'madhyapradesh') {
			return '23';
		} else if (statestr == 'dadra&nagarhaveli') {
			return '26';
		} else if (statestr == 'andaman&nicobarislands' || statestr == 'andamannicobarislands') {
			return '35';
		} else if (statestr == 'uttarpradesh') {
			return '09';
		} else if (statestr == 'arunachalpradesh') {
			return '12';
		} else if (statestr == 'tamilnadu') {
			return '33';
		} else if (statestr == 'delhi') {
			return '07';
		}
		return (typeof (stateCodeEnum[state.toLowerCase()]) != 'undefined') ? stateCodeEnum[statestr] : '00';
	}
	try {
		function validateEmail(email) {
			var t = email;
			var x = t.indexOf('@');
			var y = t.lastIndexOf('.');
			if (x == -1 || y == -1 || (x + 2) >= y) {
				throw 'Email address is not valid';
			}
		}
		validateEmail(payload.Email);
		if (payload.FirstName.toString().length > 26) {
			throw 'Maximum 26 character allow for FirstName';
		}
		if (payload.LastName != null && payload.LastName != '') {
			if (payload.LastName.toString().length > 26) {
				throw 'Maximum 26 character allow for LastName';
			}
		}
		if (payload.MiddleName != null) {
			if (payload.MiddleName.toString().length > 26) {
				throw 'Maximum 26 character allow for MiddleName';
			}
		}
		if (payload.Flatno.toString().length > 40) {
			throw 'Maximum 40 character allow for Flatno';
		}
		if (payload.BuildingName != null) {
			if (payload.BuildingName.toString().length > 40) {
				throw 'Maximum 40 character allow for BuildingName';
			}
		}
		if (payload.Road != null) {
			if (payload.Road.toString().length > 26) {
				throw 'Maximum 40 character allow for Road';
			}
		}
		if (payload.City != null) {
			if (payload.City.toString().length > 26) {
				throw 'Maximum 40 character allow for City';
			}
		}
		if (payload.Pincode != null) {
			var pincode = Number(payload.Pincode);
			if (pincode.toString().length != 6) {
				throw 'PinCode must be 6 Digits Numeric value';
			}
			if (!(Number(pincode.toString().charAt(0)) >= 1 && Number(pincode.toString().charAt(0)) < 9)) {
				throw 'PinCode 1st Digit should be from 1-9';
			}
			if (Number(pincode.toString().charAt(5)) == 0 && Number(pincode.toString().charAt(4)) == 0 && Number(pincode.toString().charAt(3)) == 0) {
				throw 'PinCode Last Three Digits should not be zero';
			}
		}
		if (payload.Pan != null) {
			var Pan = payload.Pan;
			if (Pan.toString().length != 10) {
				throw 'Pan must be a minimum of 10 characters';
			}
		}
		if (payload.Aadhaar != null) {
			var aadhaar = payload.Aadhaar;
			if (aadhaar.toString().length != 12) {
				throw 'Aadhaar must be must be 12 Digits Numeric value';
			}
			if (Number(aadhaar.toString().charAt(0)) != 0) {
				throw 'Aadhaar 1st digit should not be zero.';
			}
		}
		if (payload.MobileNo != null) {
			var mobileNo = payload.MobileNo;
			if (mobileNo.toString().length != 10) {
				throw 'MobileNo must be 10 Digits Numeric value.';
			}
			if (!(Number(mobileNo.toString().charAt(0)) >= 7 && Number(mobileNo.toString().charAt(0)) <= 9)) {
				throw 'MobileNo 1st Digit should be from 7,8,9';
			}
		}
		var gender = '';
		if (payload.Gender.toLowerCase() == 'male') {
			gender = '1';
		} else if (payload.Gender.toLowerCase() == 'female') {
			gender = '2';
		}
		var dateOfBirth = new Date(payload.DateOfBirth);
		if (dateOfBirth != null) {
			if (dateOfBirth.toString() == 'Invalid Date') {
				throw 'Invalid Date format.Format must be yyyy-mm-dd';
			}
		}
		var month_name = function (dt) {
			var mlist = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			return mlist[dateOfBirth.getMonth()];
		};
		var dateOfBirthStr = dateOfBirth.getDate() + '-' + month_name(dateOfBirth) + '-' + dateOfBirth.getFullYear();
		var request = 'surName=' + payload.SurName +
			'&flatno=' + payload.Flatno +
			'&city=' + payload.City +
			'&gender=' + gender +
			'&allowVoucher=' + configuration.AllowVoucher +
			'&allowConsent_additional=' + configuration.AllowConsent_additional +
			'&pincode=' + payload.Pincode +
			'&pan=' + payload.Pan + '&email=' + payload.Email +
			'&emailConditionalByPass=' + configuration.EmailConditionalByPass +
			'&reason=Test&allowCaptcha=' + configuration.AllowCaptcha +
			'&voucherCode=' + configuration.VoucherCode + '&allowEdit=' + configuration.AllowEdit +
			'&allowConsent=' + configuration.AllowConsent + '&mobileNo=' + payload.MobileNo +
			'&allowInput=' + configuration.AllowInput + '&firstName=' + payload.FirstName +
			'&noValidationByPass=' + configuration.NoValidationByPass +
			'&state=' + getStateCode(payload.State) + '&clientName=' + configuration.ClientName +
			'&dateOfBirth=' + dateOfBirthStr + '&allowEmailVerify=' + configuration.AllowEmailVerify;
			if(payload.BuildingName!=null)
			{
				if(payload.BuildingName!='')
				{
					request =request+ '&buildingName='+payload.BuildingName;
				}
			}
	} catch (err) {
		message = err;
	}
	var result = {
		'request': request,
		'message': message
	};
	return result;
}
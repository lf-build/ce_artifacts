function data_attribute_georiskpincodeverified (entityType, entityId, event) {
    var result = {
        'geoRiskPinCode': {
        zone: event.Data.Response.Zone,
        referenceNumber: event.Data.ReferenceNumber,
        geoRiskRanking: event.Data.Response.GeoRiskRanking
    }};
     return result;
 }
function data_attribute_DomainSearched(entityType, entityId, event) {
	var result = {
		'domainSearched':{
		'domainMatched': event.Data.Response.DomainMatched,
		'verificationMethod': event.Data.Response.VerificationMethod
		}
	};
	return result;
};
function data_attribute_getbureauData(crifData, cibilData) {
    if (typeof (cibilData) == 'undefined') {
        return crifData;
    } else if (typeof (crifData) == 'undefined') {
        return cibilData;
    }
    if (cibilData.fileType.toUpperCase() == 'THICK' || cibilData.fileType.toUpperCase() == 'THIN') {
        return cibilData;
    } else if ((cibilData.fileType.toUpperCase() == 'NOTFOUND' || cibilData.fileType.toUpperCase() == 'NTC') && (crifData.fileType.toUpperCase() != 'NTC' && crifData.fileType.toUpperCase() != 'NOTFOUND')) {
        return crifData;
    } else {
        return cibilData;
    }
};
function data_attribute_InitialOfferSelected (entityType, entityId, event) {
    if (typeof(event) != 'undefined') {
        var result = {
            'SelectedInitialOffer': {
                FileType: event.Data.SelectedInitialOffer.FileType,
                InterestRate: event.Data.SelectedInitialOffer.InterestRate,
                LoanTenure: event.Data.SelectedInitialOffer.LoanTenure,
                Score: event.Data.SelectedInitialOffer.Score,
                OfferAmount: event.Data.SelectedInitialOffer.OfferAmount,
                FinalOfferAmount: event.Data.SelectedInitialOffer.FinalOfferAmount,
                MaxPermittedEMI: event.Data.SelectedInitialOffer.MaxPermittedEMI,
                NetMonthlyIncome: event.Data.SelectedInitialOffer.netMonthlyIncome,
                CurrentEMINMI: event.Data.SelectedInitialOffer.currentEMINMI,
                MonthlyExpense: event.Data.SelectedInitialOffer.monthlyExpense,
                IsSelected: event.Data.SelectedInitialOffer.IsSelected,
                OfferSelectedDate: event.Time.Time
            } };
        return result;
    }
}
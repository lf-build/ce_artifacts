function data_attribute_companycategorydatarequested(entityType, entityId, event) {
    var companyCategory = '';
    var referenceNumber = '';
    var experianCategory = 'Unclassified';
    var subCategory = '';
    var typeofcompany = '';
    var category = '';
    var cin = '';
    var bsNot = '';
    if (typeof (event.Data.Response.Result) != 'undefined') {
        companyCategory = (event.Data.Response.Result).toString().toUpperCase().replace('Cat'.toUpperCase(), '').replace(' ', '');
        referenceNumber = event.Data.ReferenceNumber;
        cin = event.Data.Response.Cin;
    }
    if (typeof (event.Data.Request) != 'undefined') {
        if (typeof (event.Data.Request.Company) != 'undefined' && event.Data.Request.Company != null) {
            experianCategory = (event.Data.Request.Company.ExperianCategory).toString().toUpperCase().replace('Cat'.toUpperCase(), '').replace(' ', '');;
            subCategory = event.Data.Request.Company.SubCategory;
            typeofcompany = event.Data.Request.Company.Type;
            category = event.Data.Request.Company.Category == 'Government' ? 'Government' : typeofcompany;
            if (typeof (event.Data.Request.Company.BSNot) != 'undefined') {
                bsNot = event.Data.Request.Company.BSNot;
            }
        }
    }
    return {
        'companyCategoryReport':
            {
                'companyCategory': companyCategory,
                'referenceNumber': referenceNumber,
                'experianCategory': experianCategory,
                'subCategory': subCategory,
                'typeofcompany': typeofcompany,
                'cin': cin,
                'category': category,
                'bsNot': bsNot
            }
    };
};
function data_attribute_lendosocialscorerequested(entityType, entityId, event) {
    var result ={'clientScoreCard': {
        score: event.Data.Response.Score,
        referenceNumber: event.Data.ReferenceNumber
    }};
    return result;
}

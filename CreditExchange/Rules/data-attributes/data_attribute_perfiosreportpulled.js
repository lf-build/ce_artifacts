function data_attribute_perfiosreportpulled(entityType, entityId, event) {
	var CurrentEMI = 0;
	var MonthlyExpense = 0;
	var Income = 0;
	var AvgInvestmentExpensePerMonth = 0;
	var AvgDailyBalance = 0;
	var SalaryDate = null;
	var last3Salary = [];
	var MaxCredit = 0;
	var ecsInwardBounce = 0;
	var ecsOutwardBounce = 0;
	var chqInwardBounce = 0;
	var chqOutwardBounce = 0;
	var otherInwardBounce = 0;
	var form26ASDetails = false;
	var statementPassword = null;
	var salarycredit =0;
	if(typeof (event.Data.Response.Form26ASInfo ) != 'undefined')
	{
		if(event.Data.Response.Form26ASInfo!=null)
			{
		form26ASDetails = true;
			}
	}
	if(typeof (event.Data.Response.StatementPassword ) != 'undefined')
	{
		if(event.Data.Response.StatementPassword!=null)
			{
				statementPassword = event.Data.Response.StatementPassword;
			}
	}
	if (event.Data.Response.StatementSummary.InwardEcsBounces != null) {
		ecsInwardBounce = parseInt(event.Data.Response.StatementSummary.InwardEcsBounces);
	}
	if (event.Data.Response.StatementSummary.OutwardEcsBounces != null) {
		ecsOutwardBounce = parseInt(event.Data.Response.StatementSummary.OutwardEcsBounces);
	}
	if (event.Data.Response.StatementSummary.InwardChqBounces != null) {
		chqInwardBounce = parseInt(event.Data.Response.StatementSummary.InwardChqBounces);
	}
	if (event.Data.Response.StatementSummary.OutwardChqBounces != null) {
		chqOutwardBounce = parseInt(event.Data.Response.StatementSummary.OutwardChqBounces);
	}
	if (event.Data.Response.StatementSummary.InwardOtherBounces != null) {
		otherInwardBounce = parseInt(event.Data.Response.StatementSummary.InwardOtherBounces);
	}
	MonthlyExpense = event.Data.Response.StatementSummary.AvgMonthlyExpense;
	AvgInvestmentExpensePerMonth = event.Data.Response.StatementSummary.AvgInvestmentExpensePerMonth;
	AvgDailyBalance = event.Data.Response.StatementSummary.AvgDailyBalance;
	
	MaxCredit = event.Data.Response.StatementSummary.MaxCredit;
	var emiAmount = 0;
	var emiCount = 0;
	var recurringAmount = 0;
	var recurringExpeseCount = 0;
	var RecurringExpenses = event.Data.Response.RecurringExpenses;
	if (RecurringExpenses != null) {
		for (var k = 0; k < RecurringExpenses.length; k++) {
			if (RecurringExpenses[k]['Category'] == 'Loan' || RecurringExpenses[k]['Category'] == 'House Loan' || RecurringExpenses[k]['Category'] == 'EMI Payments') {
				var monthwiseBreakup = RecurringExpenses[k]['MonthwiseBreakups'];
				for (var i = 0; i < monthwiseBreakup.length; i++) {
					emiAmount = emiAmount + parseFloat(monthwiseBreakup[i]['Amount']);
					emiCount = emiCount + 1;
				}
			} else {
				var monthwiseBreakup = RecurringExpenses[k]['MonthwiseBreakups'];
				for (var i = 0; i < monthwiseBreakup.length; i++) {
					recurringAmount = recurringAmount + parseFloat(monthwiseBreakup[i]['Amount']);
					recurringExpeseCount = recurringExpeseCount + 1;
				}
			}
		}
	}
	var transactions = event.Data.Response.Transactions;
	for (var i = (transactions.length); i--; ) {
		if (transactions[i]['Category'] == 'Salary') {
			var transactiondate = new Date(transactions[i]['Date']);
			if (last3Salary.length < 3) {
				var Salarydetail = {
					'SalaryDate': transactiondate,
					'narration': transactions[i]['Narration'],
					'amount': transactions[i]['Amount']
				};
				last3Salary.push(Salarydetail);
			}
			if (SalaryDate == null) {
				SalaryDate = transactiondate;
			}
			if (transactiondate > SalaryDate) {
				SalaryDate = transactiondate;
			}
			if(transactions[i]['Amount']!=null && !isNaN(parseFloat(transactions[i]['Amount'])))
				{
                   Income = Income + parseFloat(transactions[i]['Amount']);
				}
				salarycredit = salarycredit+1;
		}
	}
	if(Income>0 && salarycredit>0 )
		{
			Income = Income/salarycredit;
		}
	if (emiAmount > 0) {
		CurrentEMI = emiAmount / emiCount;
	}
	MonthlyExpense = recurringAmount / recurringExpeseCount;
	return {
		'perfiosReport': {
			'monthlyEmi': CurrentEMI,
			'monthlyExpense': MonthlyExpense,
			'monthlyIncome': Income,
			'avgInvestmentExpensePerMonth': AvgInvestmentExpensePerMonth,
			'avgDailyBalance': AvgDailyBalance,
			'salaryDate': SalaryDate,
			'ecsInwardBounce': event.Data.Response.StatementSummary.InwardEcsBounces,
			'ecsOutwardBounce': event.Data.Response.StatementSummary.OutwardEcsBounces,
			'chqInwardBounce': event.Data.Response.StatementSummary.InwardChqBounces,
			'chqOutwardBounce': event.Data.Response.StatementSummary.OutwardChqBounces,
			'ecsBounce': ecsInwardBounce + chqInwardBounce + otherInwardBounce,
			'referenceNumber': event.Data.ReferenceNumber,
			'maxCredit': MaxCredit,
			'accountNo': event.Data.Response.AccountNo,
			'accountType': event.Data.Response.AccountType,
			'name': event.Data.Response.CustomerInfo.Name,
			'address': event.Data.Response.CustomerInfo.Address,
			'landline': event.Data.Response.CustomerInfo.Landline,
			'mobile': event.Data.Response.CustomerInfo.Mobile,
			'email': event.Data.Response.CustomerInfo.Email,
			'pan': event.Data.Response.CustomerInfo.Pan,
			'bankSourceType': 'perfios',
			'Last3SalaryDeposit': last3Salary,
			'salaryIdentifiedby':'Bank Statement and Perfios',
			'perfiosresult':'Positive',
            'form26ASDetails' :form26ASDetails,
			'sourceOfData': event.Data.Response.CustomerInfo.SourceOfData,
			'statementPassword':statementPassword
		}
	};
}
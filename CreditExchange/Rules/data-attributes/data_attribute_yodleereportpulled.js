function data_attribute_yodleereportpulled(entityType, entityId, event) {
    var CurrentEMI = 0;
    var MonthlyExpense = 0;
    var Income = 0;
    var AvgInvestmentExpensePerMonth = 0;
    var AvgDailyBalance = 0;
    var SalaryDate = null;
    var MaxCredit = 0;
    var ecsBounce = 0;
    var transactionDataObjects = event.Data.Response.transactionDataObjects;
    var ambDetails = event.Data.Response.ambDetails;
    var AvgDailyBalance = event.Data.Response.adbDetails != null ? parseFloat(event.Data.Response.adbDetails.avgDailyBalance.replace(/,/, '')) : 0;
    Income = event.Data.Response.categoryDetails.income != null ? parseFloat(event.Data.Response.categoryDetails.income.avgIncome.replace(/,/, '')) : 0;
    MonthlyExpense = event.Data.Response.categoryDetails.expense != null ? parseFloat(event.Data.Response.categoryDetails.expense.avgExpense.replace(/,/, '')) : 0;
    if (transactionDataObjects != null && transactionDataObjects.length > 0) {
        var totalAmount = 0;
        for (var k = 0; k < transactionDataObjects.length; k++) {

            if (transactionDataObjects[k]['txnListType'].toLowerCase() == 'emi') {
                totalAmount = totalAmount + parseFloat(transactionDataObjects[k]['totalAmt'].replace(/,/, ''));
            }

            if (transactionDataObjects[k].transactionDataObjects != null && transactionDataObjects[k].transactionDataObjects.length > 0) {
                for (var i = 0; i < transactionDataObjects[k].transactionDataObjects.length; i++) {
                    if (parseInt(transactionDataObjects[k].transactionDataObjects[i]['categoryId']) == 36 && parseFloat(transactionDataObjects[k].transactionDataObjects[i]['amount']) > 0) {
                        AvgInvestmentExpensePerMonth = parseFloat(transactionDataObjects[k].transactionDataObjects[i]['amount']);
                    }

                    if ((transactionDataObjects[k].transactionDataObjects[i]['category']).toUpperCase() == 'PAYCHECK/SALARY') {
                        var transactiondate = new Date(transactionDataObjects[k].transactionDataObjects[i]['dateToString']);
                        if (SalaryDate == null) {
                            SalaryDate = transactiondate;
                        }
                        if (transactiondate > SalaryDate) {
                            SalaryDate = transactiondate;
                        }
                    }
                }
            }
            if (transactionDataObjects[k]['txnListType'] == 'Inward Return') {
                ecsBounce = parseInt(transactionDataObjects[k]['count']) > 0 ? parseInt(transactionDataObjects[k]['count']) : 0;
            }
            if (transactionDataObjects[k]['txnListType'] == 'Top 5 Credits') {
                if (transactionDataObjects[k].transactionDataObjects != null && transactionDataObjects[k].transactionDataObjects.length > 0) {
                    MaxCredit = parseFloat(transactionDataObjects[k].transactionDataObjects[0]['amount']);
                    for (var j = 1 ; j < transactionDataObjects[k].transactionDataObjects.length; j++) {
                        if (parseFloat(transactionDataObjects[k].transactionDataObjects[j]['amount']) > MaxCredit) {
                            MaxCredit = parseFloat(transactionDataObjects[k].transactionDataObjects[j]['amount']);
                        }
                    }
                }
            }
        }

        if (ambDetails != null && ambDetails.length > 0) {
            CurrentEMI = parseFloat(totalAmount / ambDetails.length);
        }
    }


    return {
        'yodleeCashFlowReport': {
            'monthlyEmi': CurrentEMI,
            'monthlyExpense': MonthlyExpense,
            'monthlyIncome': Income,
            'avgInvestmentExpensePerMonth': AvgInvestmentExpensePerMonth,
            'avgDailyBalance': AvgDailyBalance,
            'salaryDate': SalaryDate,
            'ecsInwardBounce': 0,
            'ecsOutwardBounce': 0,
            'chqInwardBounce': 0,
            'chqOutwardBounce': 0,
            'ecsBounce': ecsBounce,
            'referenceNumber': null,
            'maxCredit': MaxCredit,
            'accountNo': event.Data.Response.metaData.accountNumber,
            'accountType':null,
            'name': event.Data.Response.metaData.accountHolder,
            'address': null,
            'landline': null,
            'mobile': null,
            'email': null,
            'pan': null,
            'bankSourceType': 'yodlee'
        }
    };
}
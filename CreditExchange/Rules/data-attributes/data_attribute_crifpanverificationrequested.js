function data_attribute_crifpanverificationrequested(entityType, entityId, event) {
	var status = null;
	if (typeof(event.Data.Response.Status) != 'undefined') { {
			status = event.Data.Response.Status;
		}
		var result = {
			'crifPanReport': {
				'score': event.Data.Response.Score,
				'referenceNumber': event.Data.ReferenceNumber,
				'status': status,
			}
		};
		return result;
	}
}

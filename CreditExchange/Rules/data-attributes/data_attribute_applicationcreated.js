function data_attribute_applicationcreated(entityType, entityId, event) {
    var CreditCardBalance = 0;
    var requestedAmount = 0;
    var SelfEmi = 0;
    var monthlyExpense = 0;
    var monthlyRent = 0;
    var Income = 0;
    var OtherEmi = 0;
    var EmployeeType = null;
    var Age = 0;
    var LandlineNo = null;
    var MobileNo = null;
    var City = null;
    var ZipCode = null;
    var CinNumber = null;
    var CompanyEmailAddress = null;
    var CompanyName = null;
    var EmploymentAddress = null;
    var Designation = null;
    var TotalWorkExp = null;
    var PersonalEmail = null;
    var EducationalQualification = null;
    var EducationalInstitution = null;
    var WorkPhoneNumbers = null;
    var PermanentAddress = null;
    var CurrentAddress = null;
    var HunterStatus = null;
    var SodexoCode = null;
    var TrackingCode = null;
    var TrackingCodeMedium = null;
    var HunterId = null;
    var SchemeCode = null;
    var DocumentCollectedSource = null;
    var ScheduleDate = null;
    var PreferredAddress = null;
    var ScheduleTime = null;
    var GCLId = null;
    if (event != null) {
        var application = event.Data.Application;
        var applicant = event.Data.Applicant;
        requestedAmount = application.RequestedAmount;
        if (application.SelfDeclareExpense != null) {

            monthlyExpense = application.SelfDeclareExpense.MonthlyExpenses;
            monthlyRent = application.SelfDeclareExpense.MonthlyRent;
            CreditCardBalance = application.SelfDeclareExpense.CreditCardBalances;
            OtherEmi = application.SelfDeclareExpense.DebtPayments;
            SelfEmi = application.SelfDeclareExpense.DebtPayments + 0.05 * CreditCardBalance;
        }
        if (application.EmploymentDetail != null) {
            Income = application.EmploymentDetail.IncomeInformation.Income;
            EmployeeType = application.EmploymentDetail.EmploymentStatus;
            CinNumber = application.EmploymentDetail.CinNumber;
            CompanyEmailAddress = application.EmploymentDetail.WorkEmail;
            CompanyName = application.EmploymentDetail.Name;
            WorkPhoneNumbers = application.EmploymentDetail.WorkPhoneNumbers;
        }
        if (applicant.DateOfBirth != null) {
            var today = new Date();
            var birthDate = new Date(applicant.DateOfBirth);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            Age = age;
        }
        for (var i = 0; i < application.PhoneNumbers.length; i++) {
            if (application.PhoneNumbers[i].PhoneType == 'Residence') {
                LandlineNo = application.PhoneNumbers[i].Phone;
                break;
            }
        }
        for (var i = 0; i < application.PhoneNumbers.length; i++) {
            if (application.PhoneNumbers[i].PhoneType == 'Mobile') {
                MobileNo = application.PhoneNumbers[i].Phone;
                break;
            }
        }
        if (applicant.Addresses != null) {
            if (applicant.Addresses.length > 0) {
                City = applicant.Addresses[0].City;
                ZipCode = applicant.Addresses[0].PinCode;
            }
        }
        for (var i = 0; i < application.Addresses.length; i++) {
            if (application.Addresses[i].AddressType == 'Permanant') {
                PermanentAddress = application.Addresses[i];
                break;
            }
        }
        for (var i = 0; i < application.Addresses.length; i++) {
            if (application.Addresses[i].AddressType == 'Current') {
                CurrentAddress = application.Addresses[i];
                break;
            }
        }
        if (application.EmploymentDetail != null) {
            Designation = application.EmploymentDetail.Designation;
            EmploymentAddress = application.EmploymentDetail.Addresses;
            TotalWorkExp = application.EmploymentDetail.LengthOfEmploymentInMonths / 12;
        }
        if (application.EmailAddress != null && application.EmailAddress.EmailType == 'Personal') {
            PersonalEmail = application.EmailAddress.Email;
        }
        if (applicant.HighestEducationInformation != null) {
            EducationalQualification = applicant.HighestEducationInformation.LevelOfEducation;
            EducationalInstitution = applicant.HighestEducationInformation.EducationalInstitution;
        }
        if (typeof (application.HunterStatus) != 'undefined') {
            HunterStatus = application.HunterStatus;
        }
        if (typeof (application.SodexoCode) != 'undefined') {
            SodexoCode = application.SodexoCode;
        }
        if (typeof (application.Source.TrackingCode) != 'undefined') {
            TrackingCode = application.Source.TrackingCode;
        }
        if (typeof (application.Source.TrackingCodeMedium) != 'undefined') {
            TrackingCodeMedium = application.Source.TrackingCodeMedium;
        }
        if (typeof (application.HunterId) != 'undefined') {
            HunterId = application.HunterId;
        }
        if (typeof (application.SchemeCode) != 'undefined') {
            SchemeCode = application.SchemeCode;
        }
        if (typeof (application.DocumentCollectedSource) != 'undefined') {
            DocumentCollectedSource = application.DocumentCollectedSource;
        }
        if (typeof (application.ScheduleDate) != 'undefined') {
            ScheduleDate = application.ScheduleDate;
        }
        if (typeof (application.PreferredAddress) != 'undefined') {
            PreferredAddress = application.PreferredAddress;
        }
        if (typeof (application.ScheduleTime) != 'undefined') {
            ScheduleTime = application.ScheduleTime;
        }
        if (typeof (application.Source.GCLId) != 'undefined') {
            GCLId = application.Source.GCLId;
        }
        var result = {
            'application': {
                requestedAmount: application.RequestedAmount,
                selfEmi: SelfEmi,
                creditCardBalance: CreditCardBalance,
                purposeOfLoan: application.PurposeOfLoan,
                applicationDate: application.ApplicationDate,
                age: Age,
                city: City,
                zipCode: ZipCode,
                income: Income,
                employeeType: EmployeeType,
                pan: applicant.PermanentAccountNumber,
                firstName: applicant.FirstName,
                middleName: applicant.MiddleName,
                lastName: applicant.LastName,
                gender: applicant.Gender,
                maritalStatus: applicant.MaritalStatus,
                landlineNo: LandlineNo,
                mobileNo: MobileNo,
                addresses: applicant.Addresses,
                monthlyExpense: monthlyExpense,
                monthlyRent: monthlyRent,
                residenceType: application.ResidenceType,
                cinNumber: CinNumber,
                companyEmailAddress: CompanyEmailAddress,
                companyName: CompanyName,
                dateOfBirth: applicant.DateOfBirth,
                employmentAddress: EmploymentAddress,
                designation: Designation,
                totalWorkExp: TotalWorkExp,
                personalEmail: PersonalEmail,
                aadharNumber: applicant.AadhaarNumber,
                educationalQualification: EducationalQualification,
                educationalInstitution: EducationalInstitution,
                salutation: applicant.Salutation,
                referenceNumber: event.Data.ReferenceNumber,
                bankInformation: applicant.BankInformation,
                applicantEmailAddress: applicant.emailAddress,
                applicantPhoneNumbers: applicant.phoneNumbers,
                requestedTermType: application.RequestedTermType,
                requestedTermValue: application.RequestedTermValue,
                source: application.Source,
                currentAddress: CurrentAddress,
                permanentAddress: PermanentAddress,
                expiryDate: application.ExpiryDate,
                OtherEmi: OtherEmi,
                userId: applicant.UserId,
                hunterStatus: HunterStatus,
                sodexoCode: SodexoCode,
                trackingCode: TrackingCode,
                trackingCodeMedium: TrackingCodeMedium,
                hunterId: HunterId,
                schemeCode: SchemeCode,
                documentCollectedSource: DocumentCollectedSource,
                scheduleDate: ScheduleDate,
                preferredAddress: PreferredAddress,
                scheduleTime: ScheduleTime,
                gclId: GCLId
            }
        };
        return result;
    };
}
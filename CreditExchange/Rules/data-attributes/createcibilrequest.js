function createcibilrequest(payload) {
	function pad(n) {
		return (parseInt(n) < 10) ? ('0' + n) : n;
	};

	function getGender(gender) {
		if (gender != null) {
			var genderEnum = {
				female: 1,
				male: 2,
				transgender: 3
			};
			return genderEnum[gender.toLowerCase()];
		}
	};

	function getresidenceOccupation(residenceOccupation) {
		if (residenceOccupation != null) {
			if (residenceOccupation.toLowerCase() == 'owned' || residenceOccupation.toLowerCase() == 'owned-family' || residenceOccupation.toLowerCase() == 'ownedself') {
				return 1;
			} else if (residenceOccupation.toLowerCase() == 'rented' || residenceOccupation.toLowerCase() == 'rented-friends' || residenceOccupation.toLowerCase() == 'rented-family' || residenceOccupation.toLowerCase() == 'rented-alone' || residenceOccupation.toLowerCase() == 'pg-hostel' || residenceOccupation.toLowerCase() == 'company-acco') {
				return 2;
			}
		}
	};

	function getaddressCategory(addressCategory) {
		if (addressCategory == null) {
			return 4;
		}
		var addressCategoryEnum = {
			notcategorized: 4,
			permanent: 1,
			residence: 2,
			office: 3
		};
		if (addressCategory.toLowerCase() == 'current' || addressCategory.toLowerCase() == 'residence') {
			return 2;
		} else if (addressCategory.toLowerCase() == 'permanent' || addressCategory.toLowerCase() == 'permanant') {
			return 1;
		} else if (addressCategory.toLowerCase() == 'office' || addressCategory.toLowerCase() == 'work') {
			return 3;
		} else {
			return 4;
		}
	};

	function getStateCode(state) {
		if (state == null) {
			return '99';
		}
		var statCodeEnum = {
			jammukashmir: '1',
			himachalpradesh: '2',
			punjab: '3',
			chandigarh: '4',
			uttaranchal: '5',
			haryana: '6',
			delhi: '7',
			rajasthan: '8',
			uttarpradesh: '9',
			bihar: '10',
			sikkim: '11',
			arunachalpradesh: '12',
			nagaland: '13',
			manipur: '14',
			mizoram: '15',
			tripura: '16',
			meghalaya: '17',
			assam: '18',
			westbengal: '19',
			jharkhand: '20',
			orissa: '21',
			chhattisgarh: '22',
			madhyapradesh: '23',
			gujarat: '24',
			damananddiu: '25',
			dadraandnagahaveli: '26',
			maharashtra: '27',
			andhrapradesh: '28',
			karnataka: '29',
			goa: '30',
			lakshadweep: '31',
			kerala: '32',
			tamilnadu: '33',
			pondicherry: '34',
			andamanandnicobarislands: '35',
			telangana: '36',
			defaultstatecode: '99'
		};
		if (state.toLowerCase().replace(' ', '') == 'jammu&kashmir' || state.toLowerCase().replace(' ', '') == 'jammukashmir') {
			return '1';
		} else if (state.toLowerCase().replace(' ', '') == 'westbengal') {
			return '19';
		} else if (state.toLowerCase().replace(' ', '') == 'daman&diu' || state.toLowerCase().replace(' ', '') == 'damandiu') {
			return '25';
		} else if (state.toLowerCase().replace(' ', '') == 'andhrapradesh') {
			return '28';
		} else if (state.toLowerCase().replace(' ', '') == 'himachalpradesh') {
			return '2';
		} else if (state.toLowerCase().replace(' ', '') == 'madhyapradesh') {
			return '23';
		} else if (state.toLowerCase().replace(' ', '') == 'dadra&nagarhaveli') {
			return '26';
		} else if (state.toLowerCase().replace(' ', '') == 'andaman&nicobarislands' || state.toLowerCase().replace(' ', '') == 'andamannicobarislands') {
			return '35';
		} else if (state.toLowerCase().replace(' ', '') == 'uttarpradesh') {
			return '9';
		} else if (state.toLowerCase().replace(' ', '') == 'arunachalpradesh') {
			return '12';
		} else if (state.toLowerCase().replace(' ', '') == 'tamilnadu') {
			return '33';
		} else if (state.toLowerCase().replace(' ', '') == 'delhi' || state.toLowerCase().replace(' ', '') == 'newdelhi') {
			return '7';
		}
		return (typeof (statCodeEnum[state.toLowerCase()]) != 'undefined') ? statCodeEnum[state.toLowerCase().replace(' ', '')] : '99';
	};
	if (payload != null || typeof (payload) != 'undefined') {
		try {
			if (typeof (payload.Address1Line1) != undefined) {

				payload.Address1Line1 = payload.Address1Line1.replace('&', '');

			}

			if (typeof (payload.Address1Line2) != undefined) {

				payload.Address1Line2 = payload.Address1Line2.replace('&', '');

			}

			payload.Address1ResidentOccupation = (typeof (payload.Address1ResidentOccupation) != 'undefined') ? pad(getresidenceOccupation(payload.Address1ResidentOccupation)) : '';
			payload.Address1State = pad(getStateCode(payload.Address1State));
			payload.Address1AddressCategory = (typeof (payload.Address1AddressCategory) != 'undefined') ? pad(getaddressCategory(payload.Address1AddressCategory)) : '';
			payload.Address2State = (typeof (payload.Address2State) != 'undefined') ? pad(getStateCode(payload.Address2State)) : '';
			payload.Address2ResidentOccupation = (typeof (payload.Address2ResidentOccupation) != 'undefined' && payload.address2ResidentOccupation != null) ? pad(getresidenceOccupation(payload.Address2ResidentOccupation)) : '';
			payload.Address2AddressCategory = (typeof (payload.Address2AddressCategory) != 'undefined') ? pad(getaddressCategory(payload.Address2AddressCategory)) : '';
			return payload;
		} catch (e) {
			return 'Error occured :' + e.message;
		}
	};
}
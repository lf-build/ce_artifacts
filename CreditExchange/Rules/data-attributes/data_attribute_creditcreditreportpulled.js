function data_attribute_creditcreditreportpulled(entityType, entityId, event) {


    function GetMonth(month) {
        var Monthnumber = 0;
        switch (month) {
            case 'Jan':
                Monthnumber = 1;
                break;
            case 'Feb':
                Monthnumber = 2;
                break;
            case 'Mar':
                Monthnumber = 3;
                break;
            case 'Apr':
                Monthnumber = 4;
                break;
            case 'Jun':
                Monthnumber = 5;
                break;
            case 'Jul':
                Monthnumber = 6;
                break;
            case 'Aug':
                Monthnumber = 8;
            case 'Sep':
                Monthnumber = 9;
                break;
            case 'Oct':
                Monthnumber = 10;
                break;
            case 'Nov':
                Monthnumber = 11;
                break;
            case 'Dec':
                Monthnumber = 12;
                break;
        }
        return Monthnumber;
    };
    var revolvingTrades = 0;
    var crifBureauScore = 0;
    var creditCardBalance = 0;
    var creditCardLimit = 0;
    var noOfOpenTrades = 0;
    var bureauTenure = 0;
    var homeLoanIndicator = false;
    var pLInquiryInLast6months = 0;
    var noOfCreditCards = 0;
    var unsecuredLoanPaymentHistory = 0;
    var unsecuredloanbalance = 0;
    var unsecuredloanCreditLimit = 0;
    var agOfYoungestUnsecuredTrade = 0;
    var sum_PL_Limit = 0;
    var pl_count = 0;
    var bureauEmi = 0;
    var avg_PL_Limit = 0;
    var isRBLDPDRule1 = false;
    var isRBLDPDRule2 = false;
    var isRBLDPDRule3 = false;
    var fileType = 'NTC';
    var isLiveloan = false;
    var isRBLDefaultLoan = false;
    var isRBLOverdue = false;
    var RBLOverdueamount = 0;
    var isRBLPLInquiry = false;
    var minDisbursedDate = new Date();
    var recentDisbursedDate = new Date();
    var inquiryScore = 0;
    var reportDate = null;
    var isDerogatoryStatus = false;
    var individualReponse = event.Data.Response.IndividualReponseList;

    if (individualReponse.Header != null) {
        var strreportDate = individualReponse.Header.DateOfIssue.replace('-', '').replace('-', '');
        reportDate = new Date(strreportDate.substr(4, 4), strreportDate.substr(2, 2) - 1, strreportDate.substr(0, 2));
    }
    var lastSixMonthDate = new Date(reportDate);
    lastSixMonthDate.setMonth(lastSixMonthDate.getMonth() - 6);
    if (individualReponse.InquiryHistory != null) {


        for (var k = 0; k < individualReponse.InquiryHistory.length; k++) {
            if (individualReponse.InquiryHistory[k]['MemberName'].toUpperCase() == 'RBL' || individualReponse.InquiryHistory[k]['MemberName'].toUpperCase() == 'RBL BANK' || individualReponse.InquiryHistory[k]['MemberName'].toUpperCase() == 'RBLBANK' || individualReponse.InquiryHistory[k]['MemberName'].toUpperCase() == 'RATNAKAR BANK LTD') {
                if (individualReponse.InquiryHistory[k]['InquiryDate'] != null) {
                    var strinquiryDate = individualReponse.InquiryHistory[k]['InquiryDate'].replace('-', '').replace('-', '');
                    var inquiryDate = new Date(strinquiryDate.substr(4, 4), strinquiryDate.substr(2, 2) - 1, strinquiryDate.substr(0, 2));
                    var perpose = individualReponse.InquiryHistory[k]['Purpose'];
                    if (inquiryDate >= lastSixMonthDate && inquiryDate < reportDate && (perpose.indexOf('PERSONAL') != -1 || perpose.indexOf('PL') != -1 || perpose.indexOf('CONSUMER') != -1 || perpose.indexOf('CL') != -1)) {
                        isRBLPLInquiry = true;
                    }
                }
            }
        }
    }
    if (individualReponse.Responses != null) {
        for (var k = 0; k < individualReponse.Responses.length; k++) {

            if (individualReponse.Responses[k]['AccountStatus'].toUpperCase() == 'ACTIVE' || individualReponse.Responses[k]['AccountStatus'].toUpperCase() == 'CURRENT' || individualReponse.Responses[k]['AccountStatus'].toUpperCase() == 'CLOSED') {
                var combinedPaymentHistory = individualReponse.Responses[k]['CombinedPaymentHistory'];
                var historyObj = combinedPaymentHistory.split('|')[0];
                var dateVal = historyObj.split(',')[0];
                var dpdVal = historyObj.split(',')[1].split('/')[0];
                var Month = GetMonth(dateVal.split(':')[0]);
                var FinalDate = new Date(parseInt(dateVal.split(':')[1]), Month - 1, 1);
                var LastTwentyFourMonthEndDate = new Date(reportDate);
                LastTwentyFourMonthEndDate.setMonth(LastTwentyFourMonthEndDate.getMonth() - 24);
                var LastTwelveMonthEndDate = new Date(reportDate);
                LastTwelveMonthEndDate.setMonth(LastTwelveMonthEndDate.getMonth() - 12);
                var LastSixMonthEndDate = new Date(reportDate);
                LastSixMonthEndDate.setMonth(LastSixMonthEndDate.getMonth() - 6);
                var LastOneMonthEndDate = new Date(reportDate);
                LastOneMonthEndDate.setMonth(LastSixMonthEndDate.getMonth() - 1);
                if ((FinalDate >= LastOneMonthEndDate && FinalDate < reportDate) && (parseInt(dpdVal) > 15)) {
                    isRBLDPDRule1 = true
                }
                if ((FinalDate >= LastTwelveMonthEndDate && FinalDate < reportDate) && (parseInt(dpdVal) > 90)) {
                    isRBLDPDRule2 = true
                }
                if ((FinalDate >= LastTwentyFourMonthEndDate && FinalDate < reportDate) && (parseInt(dpdVal) > 180)) {
                    isRBLDPDRule3 = true
                }
            }

            if (individualReponse.Responses[k]['AccountStatus'] == 'Active' || individualReponse.Responses[k]['AccountStatus'] == 'Current') {
                if (individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBL' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBL BANK' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBLBANK' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RATNAKAR BANK LTD') {
                    isLiveloan = true;
                    if (individualReponse.Responses[k]['OverdueAmount'] != null) {
                        RBLOverdueamount = RBLOverdueamount + parseFloat(individualReponse.Responses[k]['OverdueAmount']);
                    }
                }
                if (individualReponse.Responses[k]['AccountType'] == 'Credit Card' || individualReponse.Responses[k]['AccountType'] == 'Charge Card' || individualReponse.Responses[k]['AccountType'] == 'Loan on Credit Card' || individualReponse.Responses[k]['AccountType'] == 'Secured Credit Card' || individualReponse.Responses[k]['AccountType'] == 'Corporate Credit Card') {
                    revolvingTrades = revolvingTrades + 1;
                    if (!isNaN(parseFloat(individualReponse.Responses[k]['CurrentBalance']))) {
                        creditCardBalance = creditCardBalance + parseFloat(individualReponse.Responses[k]['CurrentBalance'].toString().replace(',', ''));
                    }
                    if (!isNaN(parseFloat(individualReponse.Responses[k]['CreditLimit']))) {
                        creditCardLimit = creditCardLimit + parseFloat(individualReponse.Responses[k]['CreditLimit'].toString().replace(',', ''));
                    }
                    if (parseFloat(individualReponse.Responses[k]['CurrentBalance'].toString().replace(',', '')) > 500) {
                        noOfCreditCards = noOfCreditCards + 1;
                    }
                } else {
                    if (!isNaN(parseFloat(individualReponse.Responses[k]['InstallmentAmount']))) {
                        var installment = individualReponse.Responses[k]['InstallmentAmount'];
                        var frequency = installment.substr(installment.lastIndexOf('/') + 1, installment.length);
                        var amount = parseFloat(installment.substr(0, installment.lastIndexOf('/')).replace(',', ''));
                        if (frequency == 'Weekly') {
                            bureauEmi = bureauEmi + amount * 4;
                        } else if (frequency == 'Bi-weekly') {
                            bureauEmi = bureauEmi + amount * 2;
                        } else if (frequency == 'Monthly') {
                            bureauEmi = bureauEmi + amount;
                        } else if (frequency == 'Yearly') {
                            bureauEmi = bureauEmi + (amount / 12);
                        }
                    } else {
                        if (!isNaN(parseFloat(individualReponse.Responses[k]['DisbursedAmount']))) {
                            if (individualReponse.Responses[k]['AccountType'] == 'Education Loan' || individualReponse.Responses[k]['AccountType'] == 'Auto Loan (Personal)' || individualReponse.Responses[k]['AccountType'] == 'Auto Overdraft' || individualReponse.Responses[k]['AccountType'] == 'Commercial Vehicle Loan' || individualReponse.Responses[k]['AccountType'] == 'Commercial Equipment Loan' || individualReponse.Responses[k]['AccountType'] == 'Used Car Loan' || individualReponse.Responses[k]['AccountType'] == 'Construction Equipment Loan' || individualReponse.Responses[k]['AccountType'] == 'Used Tractor Loan') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 2.5) / 100;
                            } else if (individualReponse.Responses[k]['AccountType'] == 'Personal Loan' || individualReponse.Responses[k]['AccountType'] == 'Loan to Professional' || individualReponse.Responses[k]['AccountType'] == 'Microfinance Personal Loan' || individualReponse.Responses[k]['AccountType'] == 'Staff Loan' || individualReponse.Responses[k]['AccountType'] == 'Two-Wheeler Loan' || individualReponse.Responses[k]['AccountType'] == 'Business Loan Priority Sector  Small Business' || individualReponse.Responses[k]['AccountType'] == 'Business Loan Priority Sector  Agriculture' || individualReponse.Responses[k]['AccountType'] == 'Business Loan Priority Sector  Others' || individualReponse.Responses[k]['AccountType'] == 'Business Non-Funded Credit Facility General' || individualReponse.Responses[k]['AccountType'] == 'Business Non-Funded Credit Facility-Priority Sector- Small Business' || individualReponse.Responses[k]['AccountType'] == 'Business Non-Funded Credit Facility-Priority Sector-Agriculture' || individualReponse.Responses[k]['AccountType'] == 'Business Non-Funded Credit Facility-Priority Sector-Others' || individualReponse.Responses[k]['AccountType'] == 'Business Loan Against Bank Deposits' || individualReponse.Responses[k]['AccountType'] == 'Microfinance Business Loan' || individualReponse.Responses[k]['AccountType'] == 'Microfinance Others' || individualReponse.Responses[k]['AccountType'] == 'Business Loan - Secured') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 3.5) / 100;
                            } else if (individualReponse.Responses[k]['AccountType'] == 'Consumer Loan') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 12.5) / 100;
                            } else if (individualReponse.Responses[k]['AccountType'] == 'Loan Against Bank Deposits') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 4.8) / 100;
                            } else if (individualReponse.Responses[k]['AccountType'] == 'Overdraft' || individualReponse.Responses[k]['AccountType'] == 'OD on Savings Account' || individualReponse.Responses[k]['AccountType'] == 'Prime Minister Jaan Dhan Yojana - Overdraft') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 9) / 100;
                            } else if (individualReponse.Responses[k]['AccountType'] == 'Housing Loan' || individualReponse.Responses[k]['AccountType'] == 'Property Loan' || individualReponse.Responses[k]['AccountType'] == 'Microfinance Housing Loan') {
                                bureauEmi = bureauEmi + (parseFloat(individualReponse.Responses[k]['DisbursedAmount'].toString().replace(',', '')) * 1.5) / 100;
                            }
                        }
                    }
                }
                if (individualReponse.Responses[k]['DisbursedDate'] != null && individualReponse.Responses[k]['DisbursedDate'] != '') {
                    var strdisbursedDate = individualReponse.Responses[k]['DisbursedDate'].replace('-', '').replace('-', '');
                    var disbursedDate = new Date(strdisbursedDate.substr(4, 4), strdisbursedDate.substr(2, 2) - 1, strdisbursedDate.substr(0, 2));
                    if (individualReponse.Responses[k]['AccountType'] == 'Education Loan' || individualReponse.Responses[k]['AccountType'] == 'Consumer Loan' || individualReponse.Responses[k]['AccountType'] == 'Credit Card' || individualReponse.Responses[k]['AccountType'] == 'Loan against Card' || individualReponse.Responses[k]['AccountType'] == 'Personal Loan' || individualReponse.Responses[k]['AccountType'] == 'Charge Card') {
                        if (!isNaN(parseFloat(individualReponse.Responses[k]['CurrentBalance']))) {
                            unsecuredloanbalance = unsecuredloanbalance + parseFloat(individualReponse.Responses[k]['CurrentBalance'].toString().replace(',', ''));
                        }
                        if (!isNaN(parseFloat(individualReponse.Responses[k]['CreditLimit']))) {
                            unsecuredloanCreditLimit = unsecuredloanCreditLimit + parseFloat(individualReponse.Responses[k]['CreditLimit'].toString().replace(',', ''));
                        }

                        if (individualReponse.Responses[k]['AccountType'] == 'Personal Loan') {
                            if (!isNaN(parseFloat(individualReponse.Responses[k]['CreditLimit']))) {
                                sum_PL_Limit = sum_PL_Limit + parseFloat(individualReponse.Responses[k]['CreditLimit'].toString().replace(',', ''));
                            }
                            pl_count = pl_count + 1;

                        }

                        if (agOfYoungestUnsecuredTrade == 0) {
                            recentDisbursedDate = disbursedDate;
                            agOfYoungestUnsecuredTrade = reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear()));
                        } else {
                            if (reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear())) <= agOfYoungestUnsecuredTrade) {
                                agOfYoungestUnsecuredTrade = reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear()));
                                recentDisbursedDate = disbursedDate;
                            }
                        }


                        var expirydate = new Date(disbursedDate.setMonth(disbursedDate.getMonth() - 12));
                        if (disbursedDate <= reportDate && disbursedDate > expirydate) {
                            noOfOpenTrades = noOfOpenTrades + 1;
                        }
                    }
                    if (bureauTenure == 0) {
                        minDisbursedDate = disbursedDate;
                        bureauTenure = reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear()));
                    } else {
                        if (reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear())) >= bureauTenure) {
                            bureauTenure = reportDate.getMonth() - disbursedDate.getMonth() + (12 * (reportDate.getFullYear() - disbursedDate.getFullYear()));
                            minDisbursedDate = disbursedDate;
                        }
                    }
                }
                if (individualReponse.Responses[k]['AccountType'] == 'Home-loan') {
                    homeLoanIndicator = true;
                }
            }
            if (individualReponse.Responses[k]['AccountStatus'].toUpperCase() == 'WRITTEN-OFF' || (individualReponse.Responses[k]['AccountStatus'] == 'CLOSED' && (individualReponse.Responses[k]['WriteoffAmount'] != null && parseFloat(individualReponse.Responses[k]['WriteoffAmount']) > 0))) {
                if (individualReponse.Responses[k]['ClosedDate'] != null) {
                    var strclosedDate = individualReponse.Responses[k]['ClosedDate'].replace('-', '').replace('-', '');
                    var closedDate = new Date(strclosedDate.substr(4, 4), strclosedDate.substr(2, 2) - 1, strclosedDate.substr(0, 2));
                    if (closedDate >= LastSixMonthEndDate) {
                        isDerogatoryStatus = true
                    }
                }
                if (individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBL' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBL BANK' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RBLBANK' || individualReponse.Responses[k]['CreditGuarantor'].toUpperCase() == 'RATNAKAR BANK LTD') {
                    isRBLDefaultLoan = true;
                }
            }
        }
        if (RBLOverdueamount >= 10000) {
            isRBLOverdue = true;
        }
    }
    bureauEmi = bureauEmi + (creditCardBalance * (0.05)) / 100;
    if (event.Data.Response.derivedAttributes != null) {
        for (var k = 0; k < event.Data.Response.derivedAttributes.length; k++) {
            if (parseFloat(event.Data.Response.derivedAttributes[k]['INQUIRIES-IN-LAST-SIX-MONTHS']) > 0) {
                pLInquiryInLast6months = pLInquiryInLast6months + 1;
            }
        }
    }
    unsecuredLoanPaymentHistory = unsecuredloanbalance / unsecuredloanCreditLimit;
    if (individualReponse.Scores != null) {
        for (var k = 0; k < individualReponse.Scores.length; k++) {
            if (individualReponse.Scores[k]['ScoreType'] == 'PERFORM-Consumer') {
                if (!isNaN(parseInt(individualReponse.Scores[k]['ScoreValue']))) {
                    crifBureauScore = parseInt(individualReponse.Scores[k]['ScoreValue']);
                }

                if (crifBureauScore == 0) {
                    fileType = 'NTC';
                } else if ((cibilBureauScore >= 1 && cibilBureauScore <= 5) || (bureauTenure < 6)) {
                    fileType = 'Thin';
                } else {
                    fileType = 'Thick';
                }
            }
        }
    } else {
        fileType = 'NotFound';
    }
    if (pl_count != 0) {
        avg_PL_Limit = sum_PL_Limit / pl_count;
    }
    var result = {
        'crifCreditReport': {
            'bureauScore': crifBureauScore,
            'creditCardBalance': creditCardBalance,
            'creditCardLimit': creditCardLimit,
            'noOfOpenedTrades': noOfOpenTrades,
            'bureauTenure': bureauTenure,
            'homeLoan': homeLoanIndicator,
            'noOfPLInquiries': pLInquiryInLast6months,
            'noOfCreditCard': noOfCreditCards,
            'unsecuredLoanBalance': unsecuredloanbalance,
            'unsecuredLoanLimit': unsecuredloanCreditLimit,
            'youngestUnsecuredTrageAgeInMonths': agOfYoungestUnsecuredTrade,
            'delinquencyFlag': 0,
            'avg_PL_Limit': avg_PL_Limit,
            'bureauEmi': bureauEmi,
            'isDerogatoryStatus': isDerogatoryStatus,
            'fileType': fileType,
            'referenceNumber': event.Data.ReferenceNumber,
            'isRBLLive': isLiveloan,
            'isRBLDefaultLoan': isRBLDefaultLoan,
            'isRBLOverdue': isRBLOverdue,
            'isRBLPLInquiry': isRBLPLInquiry,
            'isRBLDPDRule1': isRBLDPDRule1,
            'isRBLDPDRule2': isRBLDPDRule2,
            'isRBLDPDRule3': isRBLDPDRule3,
            'inquiryScore': inquiryScore,
            'isHighDPDTrade': false,
            'revolvingTrades': revolvingTrades,
            'sourceType': 'Crif'
        }
    };
    return result;
};

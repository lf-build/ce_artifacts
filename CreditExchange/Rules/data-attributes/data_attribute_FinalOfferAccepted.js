function data_attribute_FinalOfferAccepted(entityType, entityId, event) {
	function checkstartwith(slab, str) {
		for (var i = 0; i < slab.length; i++) {
			if (str.startsWith(slab[i])) {
				return true;
			}
		}
		return false;
	}
	if (typeof(event) != 'undefined') {
		var GST = 0;
		var GSTPercentage = 18;
		var frankingcharges = 0;
		var processingFee = parseFloat(event.Data.SelectedFinalOffer.ProcessingFee) > 0 ? parseFloat(event.Data.SelectedFinalOffer.ProcessingFee) : 0;
		if (processingFee > 0) {
			GST = processingFee * 0.18;
			var hyderabad = ['50']; 
			var NCR =['12','20'];
			var delihi=['11'];
			var chennai=['60'];
			var bangalore=['56'];
			var mumbai=['40', '42', '41'];
			if (typeof(event.Data.Applicaiton) != 'undefined') {
				if (event.Data.Applicaiton.application.zipCode != null) {
					var zipCode = event.Data.Applicaiton.application.zipCode.toLowerCase().trim();
					if (checkstartwith(delihi, zipCode)) {
						frankingcharges = 185;
					} else if (checkstartwith(chennai, zipCode)) {
						frankingcharges = 20;
					} else if (checkstartwith(bangalore, zipCode)) {
						frankingcharges = 200;
					} else if (checkstartwith(hyderabad, zipCode)) {
						frankingcharges = 100;}
						else if (checkstartwith(NCR, zipCode)) {
							frankingcharges = 160;
					} else if (checkstartwith(mumbai, zipCode)) {
						
						frankingcharges = parseFloat(event.Data.SelectedFinalOffer.FinalOfferAmount) * 0.001;
					}
				}
			}
		}
		var processingfeewithtax = Math.round(processingFee) + Math.round(GST) + Math.round(frankingcharges);
		var result = {
			'SelectedFinalOffer': {
				LoanTenure: event.Data.SelectedFinalOffer.Loantenure,
				OfferAmount: event.Data.SelectedFinalOffer.OfferAmount,
				Emi: event.Data.SelectedFinalOffer.Emi,
				FinalOfferAmount: event.Data.SelectedFinalOffer.FinalOfferAmount,
				NetMonthlyIncome: event.Data.SelectedFinalOffer.NetMonthlyIncome,
				CurrentEMINMI: event.Data.SelectedFinalOffer.CurrentEMINMI,
				MaxPermittedEMI: event.Data.SelectedFinalOffer.MaxPermittedEMI,
				ProcessingFee: event.Data.SelectedFinalOffer.ProcessingFee,
				InterestRate: event.Data.SelectedFinalOffer.InterestRate,
				ManualReview: event.Data.SelectedFinalOffer.ManualReview,
				IsSelected: event.Data.SelectedFinalOffer.IsSelected,
				OfferSelectedDate: event.Time.Time,
				ProcessingFeePercentage: event.Data.SelectedFinalOffer.ProcessingFeePercentage,
				GSTPercentage: GSTPercentage.toFixed(2),
				GST: GST > 0 ? GST.toFixed(0) : GST,
				Frankingcharges: frankingcharges > 0 ? frankingcharges.toFixed(0) : frankingcharges,
				ProcessingFeeWithTax: processingfeewithtax > 0 ? processingfeewithtax.toFixed(0) : processingfeewithtax
			}
		};
		return result;
	}
}
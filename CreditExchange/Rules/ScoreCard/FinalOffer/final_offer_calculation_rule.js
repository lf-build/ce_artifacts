function final_offer_calculation_rule(payload) {
	try {
		function ManualVerificationRequired(input, bureauData) {
			var finalScore = input.finalOfferScoreCalculation;
			var reasons = [];
			if (typeof(finalScore) == 'undefined') {
				finalScore = input.FinalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
			} else {
				finalScore = input.FinalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
			}
			var appScore = finalScore[0].finalOfferScore;
			var bureauScore = bureauData.bureauScore;
			var MRFlag = 0;
			if (input.companyCategoryReport != null) {
				if (input.companyCategoryReport.companyCategory != null) {
					if (input.companyCategoryReport.companyCategory.toString().toLowerCase() == 'Unclassified'.toLowerCase()) {
						MRFlag = MRFlag + 1;
						var reason = 'CompUnclass';
						reasons.push(reason);
					}
				}
			}
			var ecsBounce = input.perfiosReport.ecsBounce != null ? input.perfiosReport.ecsBounce : 0;
			if (ecsBounce > 0) {
				MRFlag = MRFlag + 1;
				var reason = 'NoECSBounce';
				reasons.push(reason);
			}
			var monthlyIncome = input.perfiosReport.monthlyIncome != null ? parseFloat(input.perfiosReport.monthlyIncome) : 0;
			var applicationIncome = input.application.income != null ? input.application.income : 0;
			var perc = (applicationIncome - monthlyIncome) * 100 / applicationIncome;
			if (perc > 20) {
				MRFlag = MRFlag + 1;
				var reason = 'IncomeDeviation';
				reasons.push(reason);
			}
			if (!monthlyIncome > 0) {
				MRFlag = MRFlag + 1;
				var reason = 'MissingSalaryNarrations';
				reasons.push(reason);
			}
			if (input.application.purposeOfLoan.toString().toLowerCase().substring(0, 6) == 'other') {
				MRFlag = MRFlag + 1;
				var reason = 'OtherPurpose';
				reasons.push(reason);
			}
			var finalIncome = Math.min(input.perfiosReport.monthlyIncome, input.application.income);
			if (parseFloat(input.perfiosReport.maxCredit) >= 2 * finalIncome) {
				MRFlag = MRFlag + 1;
				var reason = 'HigherMaxCredit';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thin' && (appScore >= 44 && appScore <= 59)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThinLowScore';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'ntc' && (appScore >= 44 && appScore <= 59)) {
				MRFlag = MRFlag + 1;
				var reason = 'NTCLowScore';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thick' && (bureauScore >= 600 && bureauScore <= 649) && (appScore >= 44 && appScore <= 75)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThickLowScore';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thick' && (bureauScore >= 650 && bureauScore <= 699) && (appScore >= 44 && appScore <= 68)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThickLowScore';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thick' && (bureauScore >= 700 && bureauScore <= 749) && (appScore >= 44 && appScore <= 61)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThickAppBureauMisMatch';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thick' && (bureauScore >= 750 && bureauScore <= 799) && (appScore >= 44 && appScore <= 58)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThickAppBureauMisMatch';
				reasons.push(reason);
			}
			if (bureauData.fileType.toLowerCase() == 'thick' && (bureauScore > 800) && (appScore >= 44 && appScore <= 47)) {
				MRFlag = MRFlag + 1;
				var reason = 'ThickAppBureauMisMatch';
				reasons.push(reason);
			}
			if (bureauData.addressMatch == false) {
				MRFlag = MRFlag + 1;
				var reason = 'AddressUnVerified';
				reasons.push(reason);
			}
			var finalIncome = Math.min(input.perfiosReport.monthlyIncome, input.application.income);
			if (finalIncome > 75000) {
				MRFlag = MRFlag + 1;
				var reason = 'HighIncome';
				reasons.push(reason);
			}
			if (input.perfiosReport.name == null || input.perfiosReport.name == '') {
				MRFlag = MRFlag + 1;
				var reason = 'PerfiosNameNotExist';
				reasons.push(reason);
			}
			if (input.perfiosReport.name != null) {
				if (input.application.firstName != null && (input.perfiosReport.name.toLowerCase()).includes(input.application.firstName.toLowerCase()) != true) {
					if (input.application.lastName != null && (input.perfiosReport.name.toLowerCase()).includes(input.application.lastName.toLowerCase()) != true) {
						MRFlag = MRFlag + 1;
						var reason = 'PerfiosNameNotMatch';
						reasons.push(reason);
					}
				}
			}
			if (bureauData.sourceType.toLowerCase() == 'cibil') {
				if (bureauData.panNameMatch < 80 || bureauData.panNoMatch < 100) {
					MRFlag = MRFlag + 1;
					var reason = 'PANNotVerfied';
					reasons.push(reason);
				}
			} else if (bureauData.sourceType.toLowerCase() == 'crif') {
				if ((typeof(payload.crifPanReport) == 'undefined' && payload.crifPanReport.score == null) || payload.crifPanReport.score < 40) {
					MRFlag = MRFlag + 1;
					var reason = 'PANNotVerfied';
					reasons.push(reason);
				}
			}
			if (typeof(bureauData.IsSecondaryReportExist) != 'undefined') {
				if (bureauData.IsSecondaryReportExist == true) {
					MRFlag = MRFlag + 1;
					var reason = 'CIBILAdditionalMatchFound';
					reasons.push(reason);
				}
			}
			if (MRFlag > 0) {
				return {
					'IsManualReview': true,
					'ManualReviewReason': reasons
				};
			} else {
				return {
					'IsManualReview': false,
					'ManualReviewReason': null
				};
			}
		}
		function CalculateFinalOffers(input, manualReview, bureauData) {
			var interestYears = GetInterestRateLookup();
			var interestRate = 0;
			var result = false;
			var returnObject = {
				'data': [],
				'detail': [],
				'result': false,
				'rejectCode': '',
				'exception': null
			};
			var finalScore = GetScore(input);
			var interestYear;
			var grade;
			var interestScoreRange;
			try {
				for (var i = 0; i < interestYears.interestRateLookUp.length; i++) {
					if (interestYears.interestRateLookUp[i].FileType.toLowerCase() == bureauData.fileType.toLowerCase()) {
						interestYear = interestYears.interestRateLookUp[i].interestYears;
						break;
					}
				}
				if (typeof(interestYear) != 'undefined') {
					for (var k = 0; k < interestYear.length; k++) {
						var year = parseInt(interestYear[k].Year);
						var finalScore = 69;
						for (var j = 0; j < interestYear[k].intrestScoreRange.length; j++) {
							if (Math.ceil(finalScore) >= interestYear[k].intrestScoreRange[j].ScoreFrom && Math.ceil(finalScore) <= interestYear[k].intrestScoreRange[j].ScoreTo) {
								interestRate = interestYear[k].intrestScoreRange[j].Rate;
								grade = interestYear[k].intrestScoreRange[j].Grade;
								returnObject.data.push(FinalOfferCalculation(input, year, interestRate, bureauData, manualReview, grade));
								break;
							}
						}
					}
				}
				result = true;
			} catch (e) {
				result = false;
			}
			var objData = [];
			if (returnObject.data != null) {
				if (returnObject.data.length > 0) {
					objData['offerData'] = returnObject.data[0];
				} else {
					objData['offerData'] = returnObject.data;
				}
			}
			return {
				'data': {
					'offerData': returnObject.data
				},
				'detail': null,
				'result': result,
				'rejectCode': '',
				'exception': null
			};
		}
		function FinalOfferCalculation(input, year, interestRate, bureauData, manualReview, grade) {
			var actualcore = GetScore(input);
		   var	finalScore=69;
			var monthlyIncome = 0;
			if (input.application.income != null) {
				monthlyIncome = input.application.income;
			}
			var monthlyExpense = 0;
			if(input.application.income<5000){
			monthlyExpense = getMonthlyExpense(bureauData.fileType, input);}
			var monthlyRent = 0;
			if (input.application.monthlyRent != null) {
				monthlyRent = input.application.monthlyRent;
			}
			var monthlyEmi = 0;
			monthlyEmi = bureauData.bureauEmi;
			
			var expenseNMIRatio = 0;
			if (monthlyIncome > 0) {
				expenseNMIRatio = ( (monthlyExpense+monthlyRent) / monthlyIncome) * 100;
			}
			var emiDeviation = 0;
			emiDeviation = (input.perfiosReport.monthlyEmi == 0 || input.perfiosReport.monthlyEmi == null) ? 0 : ((input.perfiosReport.monthlyEmi - input.application.selfEmi) * 100) / input.perfiosReport.monthlyEmi;
			var incomeDeviation = 0;
			incomeDeviation = input.application.income == 0 ? 0 : (input.application.income - parseFloat(input.perfiosReport.monthlyIncome)) * 100 / input.application.income;
			var netMonthlyIncome = monthlyIncome - monthlyExpense - monthlyRent - monthlyEmi;
			var currentEMINMI = 0;
			if (monthlyIncome > 0) {
				currentEMINMI = (monthlyEmi / monthlyIncome);
			}
			currentEMINMI = parseFloat(currentEMINMI.toFixed(6));
			var maxPermittedNMENMI = 0;
			var maxPermittedEMINMI = 0;
			var maxPermittedEMI = 0;
			if (monthlyIncome < 50000) {
				var netMonthlyratio = 85;
			
				maxPermittedEMINMI = GetMaxPermittedEMINMI(finalScore, monthlyIncome, bureauData.fileType, bureauData.bureauScore);
				maxPermittedEMI = Math.max(Math.min((monthlyIncome * (maxPermittedEMINMI - currentEMINMI)), (netMonthlyratio * (netMonthlyIncome))), 0);
			} else {
				maxPermittedNMENMI = GetMaxPermittedNMENMI(finalScore, monthlyIncome, bureauData.fileType);
				maxPermittedEMINMI = GetMaxPermittedEMINMI(finalScore, monthlyIncome, bureauData.fileType, bureauData.bureauScore);
				maxPermittedEMI = Math.max(Math.min((monthlyIncome * (maxPermittedEMINMI - currentEMINMI)), (maxPermittedNMENMI * monthlyIncome - monthlyRent-monthlyEmi)), 0);
			}
			maxPermittedEMI = parseFloat(maxPermittedEMI.toFixed(2));
			var rate = (interestRate / 100) / 12;
			var nper = year * 12;
			var offerAmount = -pv(rate, nper, maxPermittedEMI, 0);
			var finalOfferAmount = 0;
			if (bureauData.fileType.toLowerCase() == 'thick') {
				finalOfferAmount = Math.min(offerAmount, 10000000, input.application.requestedAmount);
			} else if (bureauData.fileType.toLowerCase() == 'thin') {
				finalOfferAmount = Math.min(offerAmount, 250000, input.application.requestedAmount);
			} else {
				if(monthlyIncome>=50000){
				finalOfferAmount = Math.min(offerAmount, 300000, input.application.requestedAmount);
				}else{
					finalOfferAmount = Math.min(offerAmount, 150000, input.application.requestedAmount);
				}
			}
			finalOfferAmount = (Math.round(finalOfferAmount) - (Math.round(finalOfferAmount) % 1000));
			var emi = 0;
			var processingFee = 0;
			var processingFeePercentage = 0;
			emi = CalculateEMI(finalOfferAmount, nper, rate);
			processingFeePercentage = GetProcessingFeeInPercentage(finalScore, nper, bureauData.fileType);
			processingFee = CalculateProcesingFee(finalOfferAmount, processingFeePercentage);
			var sReturn = {
				'fileType': bureauData.fileType,
				'score': finalScore,
				'finalOfferAmount': finalOfferAmount,
				'offerAmount': parseFloat(offerAmount.toFixed(2)),
				'netMonthlyIncome': parseFloat(netMonthlyIncome.toFixed(2)),
				'currentEMINMI': (currentEMINMI * 100).toFixed(0),
				'maxPermittedEMI': parseFloat(maxPermittedEMI.toFixed(2)),
				'maxPermittedNMENMI': parseFloat(maxPermittedNMENMI.toFixed(2)),
				'maxPermittedEMINMI': parseFloat(maxPermittedEMINMI.toFixed(2)),
				'emi': Math.ceil(emi),
				'processingFee': parseFloat(processingFee.toFixed(2)),
				'calculatedProcessingFee': parseFloat(processingFee.toFixed(2)),
				'processingFeePercentage': parseFloat(processingFeePercentage.toFixed(2)),
				'emiDeviation': parseFloat(emiDeviation.toFixed(2)),
				'monthlyExpense': input.application.income<5000? parseFloat(monthlyExpense.toFixed(2)) : monthlyRent,
				'loantenure': year * 12,
				'interestRate': interestRate,
				'consideredEMI':  parseFloat(monthlyEmi.toFixed(2)),
				'consideredIncome': monthlyIncome,
				'expenseNMIRatio': parseFloat(expenseNMIRatio.toFixed(2)),
				'incomeDeviation': parseFloat(incomeDeviation.toFixed(2)),
				'manualReview': manualReview.IsManualReview,
				'bureauSourceType': bureauData.sourceType,
				'bankSourceType': input.perfiosReport.bankSourceType,
				'grade': grade,
				'actualcore':actualcore
			};
			return sReturn;
		};
		function getMonthlyExpense(fileType, input) {
			var finalIncome =  input.application.income;
			if (finalIncome < 50000) {
				if (fileType.toLowerCase() == 'thick') {
					return finalIncome * 0.2;
				} else if (fileType.toLowerCase() == 'thin') {
					return finalIncome * 0.25;
				} else if (fileType.toLowerCase() == 'ntc') {
					return finalIncome * 0.3;
				}
			} 
			return finalIncome;
		};
		function GetMaxPermittedNMENMI(finalscore, income, fileType) {
			var maxPermittedNMENMI = 0;
			if (fileType.toLowerCase() == 'ntc') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedNMENMI = 0.35;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.45;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.55;
					}
				} else if (finalscore >= 68.1) {
					if (income >= 30000 && income < 50000) {
						maxPermittedNMENMI = 0.4;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.5;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.6;
					}
				}
			} else if (fileType.toLowerCase() == 'thin') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedNMENMI = 0.4;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.5;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.6;
					}
				} else if (finalscore >= 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedNMENMI = 0.45;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.55;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.65;
					}
				}
			} else if (fileType.toLowerCase() == 'thick') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 20000 && income < 50000) {
						maxPermittedNMENMI = 0.55;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.65;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.75;
					}
				} else if (finalscore >= 68.1) {
					if (income >= 20000 && income < 50000) {
						maxPermittedNMENMI = 0.6;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedNMENMI = 0.7;
					} else if (income >= 75000) {
						maxPermittedNMENMI = 0.8;
					}
				}
			}
			return maxPermittedNMENMI;
		};
		function GetMaxPermittedEMINMI(finalscore, income, fileType, bureauScore) {
			var maxPermittedEMINMI = 0;
			if (fileType.toLowerCase() == 'ntc') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedEMINMI = 0.25;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedEMINMI = 0.35;
					} else if (income >= 75000) {
						maxPermittedEMINMI = 0.35;
					}
				} else if (finalscore >= 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedEMINMI = 0.3;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedEMINMI = 0.4;
					} else if (income >= 75000) {
						maxPermittedEMINMI = 0.4;
					}
				}
			} else if (fileType.toLowerCase() == 'thin') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedEMINMI = 0.25;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedEMINMI = 0.35;
					} else if (income >= 75000) {
						maxPermittedEMINMI = 0.4;
					}
				} else if (finalscore >= 68.1) {
					if (income >= 25000 && income < 50000) {
						maxPermittedEMINMI = 0.30;
					} else if (income >= 50000 && income < 75000) {
						maxPermittedEMINMI = 0.40;
					} else if (income >= 75000) {
						maxPermittedEMINMI = 0.45;
					}
				}
			} else if (fileType.toLowerCase() == 'thick') {
				if (finalscore >= 0 && finalscore < 68.1) {
					if (income >= 20000 && income < 50000) {
						if (income >= 35000 && income < 50000 && bureauScore >= 700) {
							maxPermittedEMINMI = 0.55;
						} else {
							maxPermittedEMINMI = 0.45;
						}
					} else if (income >= 50000 && income < 75000) {
						if (bureauScore >= 680) {
							maxPermittedEMINMI = 0.60;
						} else {
							maxPermittedEMINMI = 0.50;	
						}
					} else if (income >= 75000) {
						if (bureauScore >= 680) {
							maxPermittedEMINMI = 0.65;
						} else {
							maxPermittedEMINMI = 0.60;
						}
					}
				} else if (finalscore >= 68.1) {
					if (income >= 20000 && income < 50000) {
						if (income >= 35000 && income < 50000 && bureauScore >= 700) {
							maxPermittedEMINMI = 0.60;
						} else {
							maxPermittedEMINMI = 0.50;
						}
					} else if (income >= 50000 && income < 75000) {
						if (bureauScore >= 680) {
							maxPermittedEMINMI = 0.65;
						} else {
							maxPermittedEMINMI = 0.55;
						}
					} else if (income >= 75000) {
						if (bureauScore >= 680) {
							maxPermittedEMINMI = 0.70;
						} else {
							maxPermittedEMINMI = 0.65;
						}
					}
				}
			}
			return maxPermittedEMINMI;
		};
		function conv_number(expr, decplaces) {
			var str = '' + Math.round(eval(expr) * Math.pow(10, decplaces));
			while (str.length <= decplaces) {
				str = '0' + str;
			}
			var decpoint = str.length - decplaces;
			return (str.substring(0, decpoint) + '.' + str.substring(decpoint, str.length));
		}
		function pv(rate, nper, pmt, fv) {
			var pv_value;
			var x;
			var y;
			rate = parseFloat(rate);
			nper = parseFloat(nper);
			pmt = parseFloat(pmt);
			fv = parseFloat(fv);
			if (rate == 0) {
				pv_value =  - (fv + (pmt * nper));
			} else {
				x = Math.pow(1 + rate, -nper);
				y = Math.pow(1 + rate, nper);
				pv_value =  - (x * (fv * rate - pmt + y * pmt)) / rate;
			}
			pv_value = conv_number(pv_value, 2);
			return (pv_value);
		}
		function CalculateEMI(principal, term, interest) {
			var emi = 0;
			var termInMonths = term * 1;
			var interestPerMonth = interest;
			emi = principal * interestPerMonth / (1 - (Math.pow(1 / (1 + interestPerMonth), termInMonths)));
			return emi;
		}
		function GetProcessingFeeInPercentage(score, term, fileType) {
			var perct = 0;
			if (fileType.toLowerCase() == 'thick') {
				if (term == 12) {
					perct = 3;
				} else if (term == 24) {
					perct = 2.75;
				} else if (term == 36 || term == 48) {
					perct = 2.50;
				}
			} else {
				if (score > 68) {
					if (term == 12) {
						perct = 3.50;
					} else if (term == 24) {
						perct = 3.25;
					} else if (term == 36 || term == 48) {
						perct = 3;
					}
				} else if (score <= 68) {
					if (term == 12) {
						perct = 4;
					} else if (term == 24) {
						perct = 3.75;
					} else if (term == 36 || term == 48) {
						perct = 3.5;
					}
				}
			}
			return perct;
		}
		function CalculateProcesingFee(finalOffer, ProcessingFeePercentage) {
			var processingFee = (ProcessingFeePercentage * finalOffer) / 100;
			if (processingFee < 1500) {
				processingFee = 1500;
			}
			if (processingFee > 10000) {
				processingFee = 10000;
			}
			return processingFee;
		}
		function GetInterestRateLookup() {
			return {
				'interestRateLookUp': [{
						'FileType': 'Thick',
						'interestYears': [{
								'Year': 1,
								'intrestScoreRange': [{
										'ScoreFrom': 99,
										'ScoreTo': 100,
										'Rate': 14,
										'Grade': 'A1'
									}, {
										'ScoreFrom': 97,
										'ScoreTo': 98,
										'Rate': 14.07,
										'Grade': 'A2'
									}, {
										'ScoreFrom': 95,
										'ScoreTo': 96,
										'Rate': 14.14,
										'Grade': 'A3'
									}, {
										'ScoreFrom': 93,
										'ScoreTo': 94,
										'Rate': 14.2,
										'Grade': 'A4'
									}, {
										'ScoreFrom': 91,
										'ScoreTo': 92,
										'Rate': 14.27,
										'Grade': 'B1'
									}, {
										'ScoreFrom': 89,
										'ScoreTo': 90,
										'Rate': 14.34,
										'Grade': 'B2'
									}, {
										'ScoreFrom': 87,
										'ScoreTo': 88,
										'Rate': 14.41,
										'Grade': 'B3'
									}, {
										'ScoreFrom': 85,
										'ScoreTo': 86,
										'Rate': 14.48,
										'Grade': 'B4'
									}, {
										'ScoreFrom': 83,
										'ScoreTo': 84,
										'Rate': 14.67,
										'Grade': 'C1'
									}, {
										'ScoreFrom': 81,
										'ScoreTo': 82,
										'Rate': 14.75,
										'Grade': 'C2'
									}, {
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 14.83,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 14.91,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 15.08,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 15.22,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 15.35,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 15.48,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 16.26,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 16.43,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 16.6,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 16.77,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 17.98,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 18.15,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 18.33,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 18.5,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 20.3,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 20.48,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 20.66,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 20.84,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 2,
								'intrestScoreRange': [{
										'ScoreFrom': 99,
										'ScoreTo': 100,
										'Rate': 14.5,
										'Grade': 'A1'
									}, {
										'ScoreFrom': 97,
										'ScoreTo': 98,
										'Rate': 14.59,
										'Grade': 'A2'
									}, {
										'ScoreFrom': 95,
										'ScoreTo': 96,
										'Rate': 14.68,
										'Grade': 'A3'
									}, {
										'ScoreFrom': 93,
										'ScoreTo': 94,
										'Rate': 14.77,
										'Grade': 'A4'
									}, {
										'ScoreFrom': 91,
										'ScoreTo': 92,
										'Rate': 14.86,
										'Grade': 'B1'
									}, {
										'ScoreFrom': 89,
										'ScoreTo': 90,
										'Rate': 14.95,
										'Grade': 'B2'
									}, {
										'ScoreFrom': 87,
										'ScoreTo': 88,
										'Rate': 15.04,
										'Grade': 'B3'
									}, {
										'ScoreFrom': 85,
										'ScoreTo': 86,
										'Rate': 15.14,
										'Grade': 'B4'
									}, {
										'ScoreFrom': 83,
										'ScoreTo': 84,
										'Rate': 15.4,
										'Grade': 'C1'
									}, {
										'ScoreFrom': 81,
										'ScoreTo': 82,
										'Rate': 15.5,
										'Grade': 'C2'
									}, {
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 15.61,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 15.71,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 15.94,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 16.11,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 16.29,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 16.46,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 17.51,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 17.73,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 17.96,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 18.19,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 19.82,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 20.05,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 20.29,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 20.52,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 22.93,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 23.17,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 23.42,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 23.66,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 3,
								'intrestScoreRange': [{
										'ScoreFrom': 99,
										'ScoreTo': 100,
										'Rate': 15,
										'Grade': 'A1'
									}, {
										'ScoreFrom': 97,
										'ScoreTo': 98,
										'Rate': 15.12,
										'Grade': 'A2'
									}, {
										'ScoreFrom': 95,
										'ScoreTo': 96,
										'Rate': 15.24,
										'Grade': 'A3'
									}, {
										'ScoreFrom': 93,
										'ScoreTo': 94,
										'Rate': 15.36,
										'Grade': 'A4'
									}, {
										'ScoreFrom': 91,
										'ScoreTo': 92,
										'Rate': 15.47,
										'Grade': 'B1'
									}, {
										'ScoreFrom': 89,
										'ScoreTo': 90,
										'Rate': 15.59,
										'Grade': 'B2'
									}, {
										'ScoreFrom': 87,
										'ScoreTo': 88,
										'Rate': 15.72,
										'Grade': 'B3'
									}, {
										'ScoreFrom': 85,
										'ScoreTo': 86,
										'Rate': 15.84,
										'Grade': 'B4'
									}, {
										'ScoreFrom': 83,
										'ScoreTo': 84,
										'Rate': 16.19,
										'Grade': 'C1'
									}, {
										'ScoreFrom': 81,
										'ScoreTo': 82,
										'Rate': 16.34,
										'Grade': 'C2'
									}, {
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 16.48,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 16.63,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 16.93,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 17.17,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 17.41,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 17.65,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 18.99,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 19.28,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 19.58,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 19.87,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 21.96,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 22.27,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 22.57,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 22.87,
										'Grade': 'F4'
									}
								]
							}, {
								'Year': 4,
								'intrestScoreRange': [{
										'ScoreFrom': 99,
										'ScoreTo': 100,
										'Rate': 15,
										'Grade': 'A1'
									}, {
										'ScoreFrom': 97,
										'ScoreTo': 98,
										'Rate': 15.12,
										'Grade': 'A2'
									}, {
										'ScoreFrom': 95,
										'ScoreTo': 96,
										'Rate': 15.24,
										'Grade': 'A3'
									}, {
										'ScoreFrom': 93,
										'ScoreTo': 94,
										'Rate': 15.36,
										'Grade': 'A4'
									}, {
										'ScoreFrom': 91,
										'ScoreTo': 92,
										'Rate': 15.47,
										'Grade': 'B1'
									}, {
										'ScoreFrom': 89,
										'ScoreTo': 90,
										'Rate': 15.59,
										'Grade': 'B2'
									}, {
										'ScoreFrom': 87,
										'ScoreTo': 88,
										'Rate': 15.72,
										'Grade': 'B3'
									}, {
										'ScoreFrom': 85,
										'ScoreTo': 86,
										'Rate': 15.84,
										'Grade': 'B4'
									}, {
										'ScoreFrom': 83,
										'ScoreTo': 84,
										'Rate': 16.19,
										'Grade': 'C1'
									}, {
										'ScoreFrom': 81,
										'ScoreTo': 82,
										'Rate': 16.34,
										'Grade': 'C2'
									}, {
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 16.48,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 16.63,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 16.93,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 17.17,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 17.41,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 17.65,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 18.99,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 19.28,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 19.58,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 19.87,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 21.96,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 22.27,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 22.57,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 22.87,
										'Grade': 'F4'
									}
								]
							}
						]
					}, {
						'FileType': 'Thin',
						'interestYears': [{
								'Year': 1,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 20,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 20.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 20.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 20.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 21,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 21.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 21.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 21.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 22,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 22.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 22.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 22.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 23,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 23.25,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 23.5,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 23.75,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 24,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 24.25,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 2,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 21,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 21.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 21.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 21.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 22,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 22.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 22.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 22.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 23,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 23.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 23.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 23.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 24,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 24.25,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 24.5,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 24.75,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 25,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 25.25,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 3,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 22,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 22.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 22.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 22.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 23,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 23.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 23.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 23.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 24,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 24.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 24.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 24.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 25,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 25.25,
										'Grade': 'F4'
									}
								]
							}, {
								'Year': 4,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 22,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 22.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 22.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 22.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 23,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 23.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 23.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 23.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 24,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 24.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 24.5,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 24.75,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 25,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 25.25,
										'Grade': 'G4'
									}
								]
							}
						]
					}, {
						'FileType': 'NTC',
						'interestYears': [{
								'Year': 1,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 20,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 20.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 20.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 20.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 21,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 21.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 21.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 21.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 22,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 22.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 22.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 22.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 23,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 23.25,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 23.5,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 23.75,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 24,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 24.25,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 2,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 21,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 21.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 21.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 21.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 22,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 22.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 22.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 22.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 23,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 23.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 23.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 23.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 24,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 24.25,
										'Grade': 'F4'
									}, {
										'ScoreFrom': 51,
										'ScoreTo': 52,
										'Rate': 24.5,
										'Grade': 'G1'
									}, {
										'ScoreFrom': 49,
										'ScoreTo': 50,
										'Rate': 24.75,
										'Grade': 'G2'
									}, {
										'ScoreFrom': 47,
										'ScoreTo': 48,
										'Rate': 25,
										'Grade': 'G3'
									}, {
										'ScoreFrom': 0,
										'ScoreTo': 46,
										'Rate': 25.25,
										'Grade': 'G4'
									}
								]
							}, {
								'Year': 3,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 22,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 22.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 22.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 22.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 23,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 23.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 23.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 23.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 24,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 24.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 24.,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 24.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 25,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 25.25,
										'Grade': 'F4'
									}
								]
							}, {
								'Year': 4,
								'intrestScoreRange': [{
										'ScoreFrom': 79,
										'ScoreTo': 80,
										'Rate': 22,
										'Grade': 'C3'
									}, {
										'ScoreFrom': 77,
										'ScoreTo': 78,
										'Rate': 22.25,
										'Grade': 'C4'
									}, {
										'ScoreFrom': 75,
										'ScoreTo': 76,
										'Rate': 22.5,
										'Grade': 'D1'
									}, {
										'ScoreFrom': 73,
										'ScoreTo': 74,
										'Rate': 22.75,
										'Grade': 'D2'
									}, {
										'ScoreFrom': 71,
										'ScoreTo': 72,
										'Rate': 23,
										'Grade': 'D3'
									}, {
										'ScoreFrom': 69,
										'ScoreTo': 70,
										'Rate': 23.25,
										'Grade': 'D4'
									}, {
										'ScoreFrom': 67,
										'ScoreTo': 68,
										'Rate': 23.5,
										'Grade': 'E1'
									}, {
										'ScoreFrom': 65,
										'ScoreTo': 66,
										'Rate': 23.75,
										'Grade': 'E2'
									}, {
										'ScoreFrom': 63,
										'ScoreTo': 64,
										'Rate': 24,
										'Grade': 'E3'
									}, {
										'ScoreFrom': 61,
										'ScoreTo': 62,
										'Rate': 24.25,
										'Grade': 'E4'
									}, {
										'ScoreFrom': 59,
										'ScoreTo': 60,
										'Rate': 24.5,
										'Grade': 'F1'
									}, {
										'ScoreFrom': 57,
										'ScoreTo': 58,
										'Rate': 24.75,
										'Grade': 'F2'
									}, {
										'ScoreFrom': 55,
										'ScoreTo': 56,
										'Rate': 25,
										'Grade': 'F3'
									}, {
										'ScoreFrom': 53,
										'ScoreTo': 54,
										'Rate': 25.25,
										'Grade': 'F4'
									}
								]
							}
						]
					}
				]
			};
		}
		function validateRequiredInput(context, input) {
			var requiredPerfiosProperty = ['monthlyEmi', 'monthlyIncome', 'monthlyExpense', 'maxCredit'];
			var requiredfinalofferscorecalculationProperty = ['finalOfferScore'];
			var requiredApplicationProperty = ['monthlyRent', 'purposeOfLoan', 'requestedAmount', 'selfEmi', 'income'];
			var requiredcrifCreditReportProperty = ['fileType', 'bureauEmi', 'bureauScore'];
			var errorData = [];
			var bureauData;
			var errorMessage = '';
			if (typeof(input) != 'undefined') {
				var finalScore;
				finalScore = input.finalOfferScoreCalculation;
				if (typeof(finalScore) === 'undefined') {
					finalScore = input.FinalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
				} else {
					finalScore = input.FinalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
				}
				if (typeof(finalScore) != 'undefined') {
					for (var i = 0; i < requiredfinalofferscorecalculationProperty.length; i++) {
						var el = finalScore[0][requiredfinalofferscorecalculationProperty[i]];
						if (typeof(el) === 'undefined') {
							errorMessage = 'Input property : ' + requiredfinalofferscorecalculationProperty[i] + ' not found.';
							errorData.push(errorMessage);
						}
					};
				} else {
					errorMessage = 'Final Offer Score data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'rejectCode': '',
						'exception': errorData
					};
				}
				var bankData;
				if (typeof(input.bankPreferenceData) != 'undefined' && input.bankPreferenceData.BankPreferenceSource != null) {
					if (input.bankPreferenceData.BankPreferenceSource.toLowerCase() == 'manual') {
						bankData = typeof(input.manualCashFlowReport) != 'undefined' ? input.manualCashFlowReport : undefined;
					} else if (input.bankPreferenceData.BankPreferenceSource.toLowerCase() == 'yodlee') {
						bankData = typeof(input.yodleeCashFlowReport) != 'undefined' ? input.yodleeCashFlowReport : undefined;
					} else {
						bankData = typeof(input.perfiosReport) != 'undefined' ? input.perfiosReport : undefined;
					}
				} else {
					var perfios = input.perfiosReport;
					var yodlee = input.yodleeCashFlowReport;
					var manualReport = input.manualCashFlowReport;
					if (typeof(yodlee) != 'undefined') {
						bankData = yodlee;
					} else if (typeof(perfios) != 'undefined') {
						bankData = perfios;
					} else if (typeof(manualReport) != 'undefined') {
						bankData = manualReport;
					} else {
						errorMessage = 'Bank data not found';
						errorData.push(errorMessage);
						return {
							'result': false,
							'detail': null,
							'data': null,
							'rejectCode': '',
							'exception': errorData
						};
					}
				}
				if (typeof(bankData) != 'undefined') {
					for (var i = 0; i < requiredPerfiosProperty.length; i++) {
						var el = bankData[requiredPerfiosProperty[i]];
						if (typeof(el) === 'undefined') {
							errorMessage = 'Input property : ' + requiredPerfiosProperty[i] + ' not found.';
							errorData.push(errorMessage);
						}
					}
					input.perfiosReport = bankData;
				}
				if (typeof(input.crifCreditReport) == 'undefined' && typeof(input.cibilReport) == 'undefined') {
					errorMessage = 'bureau data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'rejectCode': '',
						'data': null,
						'exception': errorData
					};
				} else {
					bureauData = context.call('data_attribute_getbureauData', [input.crifCreditReport, input.cibilReport]);
					if (typeof(bureauData) != 'undefined') {
						for (var i = 0; i < requiredcrifCreditReportProperty.length; i++) {
							var el = bureauData[requiredcrifCreditReportProperty[i]];
							if (typeof(el) === 'undefined') {
								errorMessage = 'Input property : ' + requiredcrifCreditReportProperty[i] + ' not found.';
								errorData.push(errorMessage);
							};
						};
					} else {
						errorMessage = 'credit report data not found';
						errorData.push(errorMessage);
						return {
							'result': false,
							'detail': null,
							'data': null,
							'rejectCode': '',
							'exception': errorData
						};
					}
				}
				var application = input.application;
				if (typeof(application) != 'undefined') {
					for (var i = 0; i < requiredApplicationProperty.length; i++) {
						var el = application[requiredApplicationProperty[i]];
						if (typeof(el) === 'undefined') {
							errorMessage = 'Input property : ' + requiredApplicationProperty[i] + ' not found.';
							errorData.push(errorMessage);
						}
					};
				} else {
					errorMessage = 'application data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'rejectCode': '',
						'exception': errorData
					};
				}
				var finalOfferScore = GetScore(input);
				if (typeof(finalOfferScore) === 'undefined') {
					errorMessage = 'Final offer score data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'rejectCode': '',
						'exception': errorData
					};
				}
				if (errorData.length > 0) {
					return {
						'result': false,
						'detail': null,
						'data': null,
						'rejectCode': '',
						'exception': errorData
					};
				} else {
					return {
						'result': true,
						'detail': errorData,
						'data': bureauData,
						'rejectCode': ''
					};
				}
			} else {
				errorMessage = 'input data not found.';
				errorData.push(errorMessage);
				return {
					'result': false,
					'detail': null,
					'data': null,
					'rejectCode': '',
					'exception': errorData
				};
			}
		}
		function GetScore(input) {
			var finalScore = input.finalOfferScoreCalculation;
			if (typeof(finalScore) == 'undefined') {
				finalScore = input.FinalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
			} else {
				finalScore = input.finalOfferScoreCalculation.FinalOfferScoreCalculation.finalScore;
			}
			if (finalScore.length > 0) {
				return finalScore[0].finalOfferScore;
			} else {
				var errorData = [];
				errorData['error'] = 'Final Score data is missing';
				return {
					'result': false,
					'detail': null,
					'data': null,
					'rejectCode': '',
					'exception': errorData
				};
			}
		}
		function calculatePreVerificationRules(input, bureauData) {
			var fileType = bureauData.fileType;
			var objResult;
			var result = true;
			var objRuleValidation = {
				'ruleName': 'VerifyPreFinalOfferRejectionRule',
				'isEligible': false,
				'reasons': 'The Segment is having none of the Thick, Thin and NTC segments.'
			};
			var data = {};
			try {
				var data;
				switch (fileType.toLowerCase()) {
				case 'thick':
					data = ExecuteThickFileRules(input);
					break;
				}
				var errorData = [];
				for (var propertyName in data) {
					var el = data[propertyName];
					if (typeof(el) != 'undefined') {
						if (!el.success) {
							errorData.push(el.message);
						}
					}
				}
				var status = true;
				if (errorData.length > 0) {
					status = false;
				}
				objResult = {
					'name': 'verifyEligibilityCheck',
					'isEligible': status,
					'reasons': errorData
				};
				result = true;
			} catch (e) {
				result = false;
			}
			return {
				'data': objResult,
				'detail': errorData,
				'result': result,
				'rejectCode': '',
				'exception': null
			};
		}
		function ExecuteThickFileRules(input) {
			var data = {
			
			};
			return data;
		}
		function ValidateIncomeFromBankDataForThick(input) {
			var monthlyIncome = 0;
			if (input.perfiosReport.monthlyIncome != null) {
				monthlyIncome = parseFloat(input.perfiosReport.monthlyIncome);
			}
			if (monthlyIncome < 20000) {
				return {
					'success': false,
					'message': 'BankIncomeLowL4',
					'rejectCode': 'BankIncomeLowL4'
				};
			}
		}
		function ValidateIncomeFromBankDataForThin(input) {
			var monthlyIncome = 0;
			if (input.perfiosReport.monthlyIncome != null) {
				monthlyIncome = parseFloat(input.perfiosReport.monthlyIncome);
			}
			if (monthlyIncome < 25000) {
				return {
					'success': false,
					'message': 'BankIncomeLowL4',
					'rejectCode': 'BankIncomeLowL4'
				};
			}
		}
		function ValidateIncomeFromBankDataForNTC(input) {
			var monthlyIncome = 0;
			if (input.perfiosReport.monthlyIncome != null) {
				monthlyIncome = parseFloat(input.perfiosReport.monthlyIncome);
			}
			if (monthlyIncome < 30000) {
				return {
					'success': false,
					'message': 'BankIncomeLowL4',
					'rejectCode': 'BankIncomeLowL4'
				};
			}
		}
		function IsEligible(obj) {
			if (typeof(obj) != 'undefined') {
				if (obj.data.isEligible == false) {
					return false;
				}
			}
			return true;
		}
		var objValidateInput = validateRequiredInput(this, payload);
		var errorData = [];
		var rejectcodevalue = 'FINALCREDITREJECTION';
		if (objValidateInput.result == true) {
			var bureauData = objValidateInput.data;
			var requestedAmount = payload.application.requestedAmount;
			var IsManualVerificationRequired = ManualVerificationRequired(payload, bureauData);
			var preVerficationResult = calculatePreVerificationRules(payload, bureauData);
			if (IsManualVerificationRequired.IsManualReview) {
				for (var i = 0; i < IsManualVerificationRequired.ManualReviewReason.length; i++) {
					errorData.push(IsManualVerificationRequired.ManualReviewReason[i]);
				}
			} else {
				IsManualVerificationRequired.IsManualReview = true;
				errorData.push('DefaultManualReview');
			}
			if (IsEligible(preVerficationResult) == true) {
				var finalScore = GetScore(payload);
				
				var offered = CalculateFinalOffers(payload, IsManualVerificationRequired, bureauData);
				if (offered.data.offerData != null) {
				
					if (offered.data.offerData.length > 0) {
						
						                                return {
                                    'data': {
                                        'offerData': offered.data.offerData
                                    },
                                    'detail': errorData,
                                    'result': true,
                                    'rejectCode': '',
                                    'exception': null
                                };
                            
                       
                    }else {
						var message = 'No offer Data Found';
						errorData.push(message);
						return {
							'data': {
								'offerData': null
							},
							'detail': null,
							'result': false,
							'rejectCode': rejectcodevalue + ' ' + finalScore,
							'exception': errorData
						};
					}
				}
			} else {
				return {
					'data': {
						'bureauSourceType': bureauData.sourceType
					},
					'detail': preVerficationResult.detail,
					'result': false,
					'rejectCode': rejectcodevalue,
					'exception': null
				};
			}
		} else {
			return objValidateInput;
		}
	} catch (e) {
		return {
			'result': false,
			'detail': null,
			'data': null,
			'exception': [e.message],
			'rejectCode': ''
		};
	}
}
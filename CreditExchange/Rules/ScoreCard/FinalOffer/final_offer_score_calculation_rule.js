function final_offer_score_calculation_rule(payload) {
    try {

        function GetValidations(input, bureauData) {
            var returnObject = {
                'data': [],
                'detail': [],
                'result': ''
            };
            var applicationData = input.application;
            var companyCategoryReport = input.companyCategoryReport;
            var crifData = bureauData;
            var zipCodeData = input.GeoRiskEligibility.GeoRiskEligibility;
            var perfiosData = input.perfiosReport;
            if (typeof (perfiosData) == 'undefined') {
                perfiosData = input.yodleeCashFlowReport;
            }

            var finalScore = 0;
            var JSON_FileConfig;
            if (crifData.fileType.toLowerCase() == 'thick') {
                if (input.CalculateInitialOffer.CalculateInitialOffer.offerData[0].score != null) {
                    finalScore = finalScore + parseFloat(input.CalculateInitialOffer.CalculateInitialOffer.offerData[0].score);
                }
                JSON_FileConfig = GetThickJSON();
            }
            else if (crifData.fileType.toLowerCase() == 'thin') {               
                var objScoreData = getThinFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData);
                if (!isNaN(objScoreData.initialScore)) {
                    finalScore = objScoreData.initialScore;
                }
                JSON_FileConfig = GetThinJSON();
            }
            else if (crifData.fileType.toLowerCase() == 'ntc') {
                var objScoreData = getNTCFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData);
                if (!isNaN(objScoreData.initialScore)) {
                    finalScore = objScoreData.initialScore;
                }

                JSON_FileConfig = GetNTCJSON();
            }
            returnObject.data.push(JSON.parse('{' + GenerateScoreSheet(input, JSON_FileConfig, finalScore, crifData) + '}'));
            Evaluate(null, null);
            return {
                'data': {
                    'finalScore': returnObject.data
                },
                'detail': returnObject.detail,
                'result': true,
                'exception': null,
                'rejectCode': ''
            };
        }
        function getThinFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData) {
            var scoreWeight = getScoreWeight('Thin');
            var data = {
                bureauScore: getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport,perfiosData, scoreWeight.BureauScoreweight),
                openedTradeScore: getThinFileOpenedTradeScore(crifData, scoreWeight.OpenedTradeweight),
                bureauTenureScore: getThinFileBureauTenureScore(crifData, scoreWeight.BureauTenureweight),
                plInquiriesScore: getThinFilePLInquiriesScore(crifData, scoreWeight.PLInquiriesweight),
                noOfCreditCardScore: getThinFileNoOfCreditCardScore(crifData, scoreWeight.NoOfCreditCardweight),
                loanPurposeScore: getLoanPurposeScore(applicationData, scoreWeight.LoanPurposeweight),
                zipcodeScore: getZipCodeScore(zipCodeData, scoreWeight.Zipcodeweight),
                applicantAgeScore: getApplicantAgeScore(applicationData, scoreWeight.ApplicantAgeweight),
                applicantGenderScore: getApplicantGenderScore(applicationData, scoreWeight.ApplicantGenderweight),
                applicantMaritalStatusScore: getApplicantMaritalStatusScore(applicationData, scoreWeight.ApplicantMaritalStatusweight),
                applicantResidenceTypeScore: getApplicantResidenceTypeScore(applicationData, scoreWeight.ApplicantResidenceTypeweight)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));
            if (weightedScore < 0) {
                weightedScore = 0;
            }           
            return {
                'initialScore': weightedScore,
                'scoreData': data
            };
        }
        function getThinFilePLInquiriesScore(input, weightAssigned) {
            var noOfPLInquiries = input.noOfPLInquiries;
            if (noOfPLInquiries <= 2) {
                return (4 * weightAssigned);
            }
            else if (noOfPLInquiries <= 4) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }

        function getThinFileBureauTenureScore(input, weightAssigned) {
            var bureauTenure = input.bureauTenure;
            if (bureauTenure >= 4 && bureauTenure <= 6) {
                return (4 * weightAssigned);
            }
            else if (bureauTenure >= 0 && bureauTenure <= 3) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }

        function getThinFileOpenedTradeScore(input, weightAssigned) {
            var noOfOpenedTrade = input.noOfOpenedTrades;
            if (noOfOpenedTrade == 1) {
                return (4 * weightAssigned);
            }
            else if (noOfOpenedTrade == 2) {
                return (2 * weightAssigned);
            }
            else if (noOfOpenedTrade >= 3) {
                return 0;
            }
            else {
                return 0;
            }
        }

        function getThinFileNoOfCreditCardScore(input, weightAssigned) {
            var noOfCreditCard = input.noOfCreditCard;
            if (noOfCreditCard <= 1) {
                return (4 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        function getNTCFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData) {
            var scoreWeight = getScoreWeight('NTC');
            var data = {
                bureauScore: getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData, scoreWeight.BureauScoreweight),
                plInquiriesScore: getNTCPLInquiriesScore(crifData, scoreWeight.PLInquiriesweight),
                zipcodeScore: getZipCodeScore(zipCodeData, scoreWeight.Zipcodeweight),
                loanPurposeScore: getLoanPurposeScore(applicationData, scoreWeight.LoanPurposeweight),
                applicantAgeScore: getApplicantAgeScore(applicationData, scoreWeight.ApplicantAgeweight),
                applicantGenderScore: getApplicantGenderScore(applicationData, scoreWeight.ApplicantGenderweight),
                applicantMaritalStatusScore: getApplicantMaritalStatusScore(applicationData, scoreWeight.ApplicantMaritalStatusweight),
                applicantResidenceTypeScore: getApplicantResidenceTypeScore(applicationData, scoreWeight.ApplicantResidenceTypeweight)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));
            if (weightedScore < 0) {
                weightedScore = 0;
            }

            return {
                'initialScore': weightedScore,
                'scoreData': data
            };
        }
        ;
        function getApplicantResidenceTypeScore(input, weightAssigned) {
            var residenceType = input.residenceType;
            var highestRiskResidenceType = ['OwnedSelf', 'Owned-Family'];
            var mediumRiskResidenceType = ['Rented-Family', 'Company-Acco'];
            if (highestRiskResidenceType.indexOf(residenceType) > -1) {
                return (4 * weightAssigned);
            }
            else if (mediumRiskResidenceType.indexOf(residenceType) > -1) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        function getLoanPurposeScore(input, weightAssigned) {
            var lowestRiskLoanPurpose = ['loanrefinancing', 'vehiclepurchase'];
            var mediumRiskLoanPurpose = ['travel', 'homeimprovement', 'wedding', 'assetacquisition', 'business', 'agriculture', 'other'];
            var highestRiskLoanPurpose = ['education', 'medical'];
            var loanPurpose = input.purposeOfLoan;
            if (lowestRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (4 * weightAssigned);
            }
            else if (mediumRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (3 * weightAssigned);
            }
            else if (highestRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (1 * weightAssigned);
            }
            else {
                return (3 * weightAssigned);
            }
        }
        ;
        function getZipCodeScore(input, weightAssigned) {
            var zipcodeRisk = input.ZoneClassification;
            if (zipcodeRisk.toLowerCase() === 'low risk') {
                return (4 * weightAssigned);
            }
            else if (zipcodeRisk.toLowerCase() === 'medium risk') {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getApplicantAgeScore(input, weightAssigned) {
            var age = input.age;
            if (age >= 21 && age <= 30) {
                return (2 * weightAssigned);
            }
            else if (age >= 31 && age <= 45) {
                return (4 * weightAssigned);
            }
            else {
                return (1 * weightAssigned);
            }
        }
        ;
        function getApplicantGenderScore(input, weightAssigned) {
            var gender = input.gender;
            if (gender.toLowerCase() === 'male') {
                return (2 * weightAssigned);
            }
            else if (gender.toLowerCase() === 'female') {
                return (4 * weightAssigned);
            }
            else {
                return (3 * weightAssigned);
            }
        }
        ;
        function getApplicantMaritalStatusScore(input, weightAssigned) {
            var maritalStatus = input.maritalStatus;
            if (maritalStatus.toLowerCase() === 'married') {
                return (4 * weightAssigned);
            }
            else if (maritalStatus.toLowerCase() === 'single') {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getScoreWeight(fileType) {
            var scoreWeightFile = JSON.parse('{\"scoreweight\":[{\"FileType\":\"Thick\",\"AssignedWeight\":{\"BureauScoreweight\":0.80,\"CreditCardBalanceweight\":0.5,\"OpenedTradeweight\":1.00,\"Delinquencyweight\":1.00,\"BureauTenureweight\":1.00,\"HomeLoanweight\":1.00,\"PLInquiriesweight\":3.5,\"NoOfCreditCardweight\":1.00,\"UnsecuredLoanweight\":1.00,\"YoungestUnsecuredTradeweight\":1.00,\"RevolvingTradesweight\":1.00,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.00,\"ApplicantAgeweight\":0.15,\"ApplicantGenderweight\":0.15,\"ApplicantMaritalStatusweight\":0.20,\"ApplicantResidenceTypeweight\":1}},{\"FileType\":\"Thin\",\"AssignedWeight\":{\"BureauScoreweight\":4.00,\"OpenedTradeweight\":0.80,\"BureauTenureweight\":0.4,\"PLInquiriesweight\":1.25,\"NoOfCreditCardweight\":0.5,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.0,\"ApplicantAgeweight\":0.30,\"ApplicantGenderweight\":0.30,\"ApplicantMaritalStatusweight\":0.20,\"ApplicantResidenceTypeweight\":1.5}},{\"FileType\":\"NTC\",\"AssignedWeight\":{\"BureauScoreweight\":5.00,\"PLInquiriesweight\":0.5,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.25,\"ApplicantAgeweight\":0.40,\"ApplicantGenderweight\":0.40,\"ApplicantMaritalStatusweight\":0.40,\"ApplicantResidenceTypeweight\":1.5}}]}');
            var assignedWeight;
            var leng = scoreWeightFile.scoreweight.length;
            for (var i = 0; i < leng; i++) {
                if (scoreWeightFile.scoreweight[i].FileType == fileType) {
                    assignedWeight = scoreWeightFile.scoreweight[i].AssignedWeight;
                    break;
                }
            }
            return assignedWeight;
        };

        function getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData, weightAssigned) {
            var ntcScoreResponse = getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData);
            var NTCApplicationScore = 0;
            if (!isNaN(ntcScoreResponse.ntcScore)) {
                var ntcScore = parseInt(ntcScoreResponse.ntcScore);
                var fileCategory;
                if (ntcScore > 240) {
                    fileCategory = 'vl';
                }
                ;
                if (ntcScore >= 220 && ntcScore <= 240) {
                    fileCategory = 'l';
                }
                ;
                if (ntcScore >= 200 && ntcScore < 220) {
                    fileCategory = 'm';
                }
                ;
                if (ntcScore >= 180 && ntcScore < 200) {
                    fileCategory = 'h';
                }
                ;
                if (ntcScore < 180) {
                    fileCategory = 'vh';
                }
                ;
                if (crifData.fileType.toLowerCase() == 'thin') {
                    NTCApplicationScore = GetFileCategoryScoreforThin(fileCategory, weightAssigned);
                }
                else if (crifData.fileType.toLowerCase() == 'ntc') {
                    NTCApplicationScore = GetFileCategoryScoreforNTC(fileCategory, weightAssigned);
                }
            }
            return NTCApplicationScore;
        }

        function GetFileCategoryScoreforNTC(fileCategory, weightAssigned) {
            var NTCApplicationScore = 0;
            if (fileCategory === 'vl' || fileCategory === 'l') {
                NTCApplicationScore = 4 * weightAssigned;
            }
            else if (fileCategory === 'm') {
                NTCApplicationScore = 2 * weightAssigned;
            }
            else if (fileCategory === 'h' || fileCategory === 'vh') {
                NTCApplicationScore = 0;
            }
            return NTCApplicationScore;
        }

        function GetFileCategoryScoreforThin(fileCategory, weightAssigned) {
            var NTCApplicationScore = 0;
            if (fileCategory === 'vl') {
                NTCApplicationScore = 5 * weightAssigned;
            }
            else if (fileCategory === 'l') {
                NTCApplicationScore = 4 * weightAssigned;
            }
            else if (fileCategory === 'm') {
                NTCApplicationScore = 3 * weightAssigned;
            }
            else if (fileCategory === 'h') {
                NTCApplicationScore = 2 * weightAssigned;
            }
            else if (fileCategory === 'vh') {
                NTCApplicationScore = 1 * weightAssigned;
            }
            return NTCApplicationScore;
        }
         /*NTC Thin Score*/
        function getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport, perfiosData) {
            var bureauScore = getBureauScoreForNTC(crifData);
            var companyDBScore = getNTCCompanyCategoryScore(companyCategoryReport);
            var incomeScrore = get_NTC_IncomeScore(perfiosData, applicationData);
            var workingExpScore = getWorkingExperienceScore(applicationData);
            var ageScore = getAgeScore(applicationData);
            var qualitificationScore = getQualifificationScore(applicationData);
            var geoRiskScore = getGeoRiskRankingScore(zipCodeData);
            var loanTermScore = getLoanTermScore(applicationData);
            var ntcScore = bureauScore + companyDBScore + incomeScrore + workingExpScore + ageScore + qualitificationScore + geoRiskScore + loanTermScore;
            return {
                'ntcScore': ntcScore
            };
        }
        ;
        function getNTCCompanyCategoryScore(company) {
            var score = 0;
            if (company != null && typeof (company.experianCategory) != 'undefined') {
                if (company.experianCategory == 'A') {
                    score = 31;
                }
                else if (company.experianCategory == 'B' || company.experianCategory == 'C') {
                    score = 24;
                }
                else {
                    score = 24;
                }
            }
            return score;
        }
        function get_NTC_IncomeScore(perfios, applicationData) {
            var score = 0;
            var income = Math.min(applicationData.income, perfios.monthlyIncome);
            if (income < 22500) {
                score = 14;
            }
            ;
            if (income < 27500 && income >= 22500) {
                score = 22;
            }
            ;
            if (income < 42500 && income >= 27500) {
                score = 29;
            }
            ;
            if (income < 77500 && income >= 42500) {
                score = 46;
            }
            ;
            if (income >= 77500) {
                score = 70;
            }
            ;
            return score;
        }
        function getBureauScoreForNTC(crifData) {
            var NTCBureauScore = crifData.noOfPLInquiries;
            var score = 0;
            if (NTCBureauScore == 0) {
                score = 26;
            }
            ;
            if (NTCBureauScore == 1) {
                score = 20;
            }
            ;
            if (NTCBureauScore >= 2) {
                score = 8;
            }
            ;
            return score;
        }
        function getWorkingExperienceScore(application) {
            var score = 0;
            var experienceinMonth = application.totalWorkExp * 12;
            if (experienceinMonth == null) {
                score = 13;
            }

            if (experienceinMonth <= 12) {
                score = 32;
            }

            if (experienceinMonth >= 13 && experienceinMonth <= 46) {
                score = 37;
            }

            if (experienceinMonth >= 47) {
                score = 43;
            }

            return score;
        }
        function getAgeScore(application) {
            var score = 0;
            if (application.age < 26) {
                score = 21;
            }
            ;
            if (application.age <= 35 && application.age >= 26) {
                score = 25;
            }
            ;
            if (application.age >= 36 && application.age <= 45) {
                score = 26;
            }
            ;
            if (application.age >= 46 && application.age <= 55) {
                score = 27;
            }
            ;
            if (application.age >= 56) {
                score = 24;
            }
            ;
            return score;
        }
        function getQualifificationScore(application) {
            var score = 0;
            if (application.educationalQualification == 'highschool') {
                score = 14;
            }
            ;
            if (application.educationalQualification == 'bachelordegree') {
                score = 25;
            }
            ;
            if (application.educationalQualification == 'masterdegree' || application.educationalQualification == 'doctratedegree') {
                score = 28;
            }
            ;
            return score;
        }
        function getGeoRiskRankingScore(zipCodeData) {
            var score = 17;
            if (zipCodeData.geoRiskRanking >= 1 && zipCodeData.geoRiskRanking <= 9) {
                score = 28;
            }

            if (zipCodeData.geoRiskRanking >= 10 && zipCodeData.geoRiskRanking <= 21) {
                score = 21;
            }
            ;
            return score;
        }
        function getLoanTermScore(application) {
            var score = 0;
            var requestedTermValue = 1;
            if (requestedTermValue >= 1 && requestedTermValue <= 12) {
                score = 42;
            }
            ;
            if (requestedTermValue >= 13 && requestedTermValue <= 24) {
                score = 38;
            }
            ;
            if (requestedTermValue >= 25 && requestedTermValue <= 36) {
                score = 30;
            }
            ;
            if (requestedTermValue >= 37) {
                score = 23;
            }
            ;
            return score;
        }
        function getNTCPLInquiriesScore(input, weightAssigned) {
            var noOfPLInquiries = input.noOfPLInquiries;
            if (noOfPLInquiries <= 2) {
                return (4 * weightAssigned);
            }
            else if (noOfPLInquiries <= 4) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function Evaluate(scoreeventname, input, crifData) {
            if (scoreeventname != null) {
                switch (scoreeventname) {
                    case 'GetAvgMonthlyIncomeScore':
                        return GetAvgMonthlyIncomeScore(input, crifData);
                    case 'GetRecurringExpenseScore':
                        return GetRecurringExpenseScore(input, crifData);
                    case 'GetAvgInvestmentScore':
                        return GetAvgInvestmentScore(input, crifData);
                    case 'GetEMIandNMIRatio':
                        return GetEMIandNMIRatio(input, crifData);
                    case 'GetAvgDailyBalanceScore':
                        return GetAvgDailyBalanceScore(input, crifData);
                    case 'GetSocialScore':
                        return GetSocialScore(input, crifData);
                    case 'GetBehavioralScore':
                        return GetBehavioralScore(input, crifData);
                    case 'GetEmploymentScore':
                        return GetEmploymentScore(input, crifData);
                    case 'GetSocialScore_NTC':
                        return GetSocialScore_NTC(input, crifData);
                    default:
                        return 0;
                }
            }
        }
        function GenerateScoreSheet(input, arrScoresWeightage, finalScore, crifData) {
            var sReturnObject = '';
            var sreturn = [];
            if (arrScoresWeightage.ApplicantFinalScore.FunctionList.length > 0) {
                for (var k = 0; k <= arrScoresWeightage.ApplicantFinalScore.FunctionList.length - 1; k++) {
                    var eventName = arrScoresWeightage.ApplicantFinalScore.FunctionList[k].Name;
                    var weightage = arrScoresWeightage.ApplicantFinalScore.FunctionList[k].Weight;
                    var outputParamterName = arrScoresWeightage.ApplicantFinalScore.FunctionList[k].OutVariable;
                    var nScore = 0;
                    var res = eval(eventName + '(input,crifData)');
                    if (res != null) {
                        if (res.toString() != 'undefined') {
                            nScore = res * parseFloat(weightage);
                            sReturnObject = sReturnObject + String.fromCharCode(34) + outputParamterName + String.fromCharCode(34) + ': ' + nScore + ',';
                        }
                        else {
                            nScore = 0;
                            sReturnObject = sReturnObject + String.fromCharCode(34) + outputParamterName + String.fromCharCode(34) + ': ' + nScore + ',';
                        }
                        finalScore = finalScore + nScore;
                        sreturn[outputParamterName] = nScore;
                    }
                }
            }
            var employment_Flag = GetCategory(GetEmployerScore(input) + GetExperienceScore(input));
            var BehaviouralFlag = GetBehaviouralScore(input);

            sreturn['finalScore'] = parseFloat(finalScore.toFixed(2));
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'finalOfferScore' + String.fromCharCode(34) + ':' + finalScore + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'employerScore' + String.fromCharCode(34) + ':' + GetEmployerScore(input) + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'experienceScore' + String.fromCharCode(34) + ':' + GetExperienceScore(input) + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'empoyementFlag' + String.fromCharCode(34) + ':\"' + employment_Flag + '\",';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'behaviouralFlag1' + String.fromCharCode(34) + ':' + GetBehaviouralFlag1(input) + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'behaviouralFlag2' + String.fromCharCode(34) + ':' + GetBehaviouralFlag2(input) + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'behaviouralFlag' + String.fromCharCode(34) + ':\"' + BehaviouralFlag + '\"' + ',';
            sReturnObject = sReturnObject + String.fromCharCode(34) + 'bureauSourceType' + String.fromCharCode(34) + ':\"' + crifData.sourceType + '\"';
            return sReturnObject;
        }
        function GetAvgInvestmentScore(input, crifData) {
            if (input.perfiosReport.avgInvestmentExpensePerMonth != null) {
                if (parseFloat(input.perfiosReport.avgInvestmentExpensePerMonth) > 0) {
                    return 2;
                }
                else {
                    return 0;
                }
            }
        }
        function GetEMIandNMIRatio(input, crifData) {
            if (input.application.selfEmi != null && input.perfiosReport.monthlyEmi != null && input.application.income != null && input.perfiosReport.monthlyIncome != null) {
                var emi = Math.max(input.application.selfEmi, crifData.bureauEmi, input.perfiosReport.monthlyEmi);
                var income = Math.min(input.application.income, input.perfiosReport.monthlyIncome);
                var EMIandNMIRatio = (emi / income) * 100;
                if (EMIandNMIRatio <= 10) {
                    return 4;
                }
                else if (EMIandNMIRatio > 10 && EMIandNMIRatio <= 20) {
                    return 2;
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
        function GetAvgMonthlyIncomeScore(input, crifData) {
            if (input.perfiosReport.monthlyIncome != null && input.application.income != null) {
                var income = Math.min(input.application.income, input.perfiosReport.monthlyIncome);
                if (income >= 40000 && income < 50000) {
                    return 1;
                }
                else if (income >= 50000 && income < 75000) {
                    return 2;
                }
                else if (income >= 75000 && income < 100000) {
                    return 3;
                }
                else if (income >= 100000) {
                    return 4;
                }
                else {
                    return 0;
                }
            }
        }
        function GetRecurringExpenseScore(input, crifData) {
            if (input.perfiosReport.monthlyIncome != null && input.perfiosReport.monthlyExpense != null && input.application.income != null) {
                var income = Math.min(input.application.income, input.perfiosReport.monthlyIncome);
                var recurringExpenseandNMIRatio = (input.perfiosReport.monthlyExpense / income) * 100;
                if (recurringExpenseandNMIRatio <= 20) {
                    return 4;
                }
                else if (recurringExpenseandNMIRatio > 20 && recurringExpenseandNMIRatio <= 50) {
                    return 2;
                }
                else if (recurringExpenseandNMIRatio > 50 && recurringExpenseandNMIRatio <= 75) {
                    return 1;
                }
            }
            else {
                return 0;
            }
        }
        function GetAvgDailyBalanceScore(input, crifData) {
            if (input.perfiosReport.avgDailyBalance != null && input.perfiosReport.monthlyIncome != null && input.application.income != null) {
                var income = Math.min(input.application.income, input.perfiosReport.monthlyIncome);
                if ((parseFloat(input.perfiosReport.avgDailyBalance) / income) * 100 > 10 && (parseFloat(input.perfiosReport.avgDailyBalance) / income) * 100 <= 25) {
                    return 2;
                }
                else if ((parseFloat(input.perfiosReport.avgDailyBalance) / income) * 100 > 25) {
                    return 4;
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
        function GetSocialScore(input, crifData) {
            if (input != null && input.clientScoreCard != null && input.clientScoreCard.score != null) {
                if (input.clientScoreCard.score >= 750 && input.clientScoreCard.score <= 899) {
                    return 3;
                }
                if (input.clientScoreCard.score >= 900) {
                    return 4;
                }
            }
            else {
                return 0;
            }
        }
        function GetSocialScore_NTC(input, crifData) {
            if (input != null && input.clientScoreCard != null && input.clientScoreCard.score != null) {
                if (input.clientScoreCard.score >= 750 && input.clientScoreCard.score <= 899) {
                    return 2;
                }
                if (input.clientScoreCard.score >= 900) {
                    return 4;
                }
            }
            else {
                return 0;
            }
        }
        function GetEmploymentScore(input, crifData) {
            var employment_Flag = GetEmployerScore(input) + GetExperienceScore(input);
            var categry = GetCategory(employment_Flag);
            if (categry == 'A+') {
                return 4;
            }
            else if (categry == 'A') {
                return 3;
            }
            else if (categry == 'B') {
                return 2;
            }
            else if (categry == 'C') {
                return 1;
            }
            else {
                return 0;
            }
        }

        function GetCategory(employment_Flag) {
            if (typeof (employment_Flag) != 'undefined') {
                if (employment_Flag >= 13) {
                    return 'A+';
                }
                else if (employment_Flag >= 10) {
                    return 'A';
                }
                else if (employment_Flag >= 7) {
                    return 'B';
                }
                else if (employment_Flag >= 4) {
                    return 'C';
                }
                else {
                    return 'D';
                }
            }
            else {
                return 'D';
            }
        }

        function GetEmployerScore(input) {
            if (input.companyCategoryReport != null) {
                if (input.companyCategoryReport.companyCategory == 'A+') {
                    return 12;
                }
                else if (input.companyCategoryReport.companyCategory == 'A') {
                    return 9;
                }
                else if (input.companyCategoryReport.companyCategory == 'B') {
                    return 6;
                }
                else if (input.companyCategoryReport.companyCategory == 'C') {
                    return 3;
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
        function GetExperienceScore(input) {
            var experienceCalculated = input.application.age <= 22 ? 0 : input.application.age - 23;
            var experienceSelf = input.application.totalWorkExp != null ? input.application.totalWorkExp : experienceCalculated;


            /* if (experienceSelf > experienceCalculated) {
                 perc = (experienceSelf - experienceCalculated) * 100 / experienceSelf;
             }
             if (perc < 20) {
                 experience = experienceSelf;
             }
             else {
                 experience = experienceCalculated;
             }*/
            if (experienceSelf < 2) {
                return 0;
            }
            else if (experienceSelf < 4) {
                return 1;
            }
            else if (experienceSelf < 7) {
                return 2;
            }
            else {
                return 3;
            }
        }
        function GetBehavioralScore(input, crifData) {
            var behavioralScore = GetBehaviouralScore(input);
            if (behavioralScore != null) {
                if (behavioralScore == 'L') {
                    return 1;
                }
                if (behavioralScore == 'M') {
                    return 2;
                }
                if (behavioralScore == 'H') {
                    return 4;
                }
            }
            else {
                return 0;
            }
        }
        function GetBehaviouralFlag1(input) {
            if (input.application.applicationDate != null) {
                var dt = input.application.applicationDate.Time.toString();
                var hr = input.application.applicationDate.Hour.toString().replace(dt + '-', '');
                hr = hr.replace('-h', '');
            }
            else {
                return 0;
            }
            if (parseInt(hr) >= 1 && parseInt(hr) <= 5) {
                return 0;
            }
            else {
                return 1;
            }
        }
        function GetBehaviouralFlag2(input) {
            var dateDifference = 0;
            if (input.application.applicationDate != null && input.perfiosReport.salaryDate != null) {
                var applicationDt = input.application.applicationDate.Time.toString();
                var salaryDt = input.perfiosReport.salaryDate.toString();
                var date1 = new Date(applicationDt);
                var date2 = new Date(salaryDt);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                dateDifference = diffDays;
            }
            if (dateDifference <= 5) {
                return 0;
            }
            else {
                return 1;
            }
        }
        function GetBehaviouralScore(input) {
            var BehaviouralFlag1 = GetBehaviouralFlag1(input);
            var BehaviouralFlag2 = GetBehaviouralFlag2(input);
            var flag = BehaviouralFlag1 + BehaviouralFlag2;
            if (flag == 2) {
                return 'H';
            }
            else if (flag == 1) {
                return 'M';
            }
            else {
                return 'L';
            }
        }
        function GetThickJSON() {
            return {
                'ApplicantFinalScore': {
                    'FileType': 'Thick',
                    'FunctionList': [{
                        'Name': 'GetAvgMonthlyIncomeScore',
                        'Weight': 1.5,
                        'OutVariable': 'avgMonthlyIncomeScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetRecurringExpenseScore',
                        'Weight': 1.0,
                        'OutVariable': 'recurringExpenseScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgInvestmentScore',
                        'Weight': 1.0,
                        'OutVariable': 'avgInvestmentScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEMIandNMIRatio',
                        'Weight': 1.0,
                        'OutVariable': 'emiAndNmiRatioScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgDailyBalanceScore',
                        'Weight': 1.0,
                        'OutVariable': 'avgDailyBalanceScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetSocialScore',
                        'Weight': 1.0,
                        'OutVariable': 'score',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetBehavioralScore',
                        'Weight': 0,
                        'OutVariable': 'behavioralScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEmploymentScore',
                        'Weight': 1.5,
                        'OutVariable': 'employmentScore',
                        'InputVariable': ''
                    }]
                }
            };
        }
        function GetThinJSON() {
            return {
                'ApplicantFinalScore': {
                    'FileType': 'Thin',
                    'FunctionList': [{
                        'Name': 'GetAvgMonthlyIncomeScore',
                        'Weight': 2,
                        'OutVariable': 'avgMonthlyIncomeScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetRecurringExpenseScore',
                        'Weight': 0.5,
                        'OutVariable': 'recurringExpenseScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgInvestmentScore',
                        'Weight': 1.0,
                        'OutVariable': 'avgInvestmentScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEMIandNMIRatio',
                        'Weight': 0.5,
                        'OutVariable': 'emiAndNmiRatioScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgDailyBalanceScore',
                        'Weight': 1.25,
                        'OutVariable': 'avgDailyBalanceScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetSocialScore',
                        'Weight': 2.0,
                        'OutVariable': 'score',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetBehavioralScore',
                        'Weight': 0,
                        'OutVariable': 'behavioralScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEmploymentScore',
                        'Weight': 2.0,
                        'OutVariable': 'employmentScore',
                        'InputVariable': ''
                    }]
                }
            };
        }
        function GetNTCJSON() {
            return {
                'ApplicantFinalScore': {
                    'FileType': 'NTC',
                    'FunctionList': [{
                        'Name': 'GetAvgMonthlyIncomeScore',
                        'Weight': 2,
                        'OutVariable': 'avgMonthlyIncomeScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetRecurringExpenseScore',
                        'Weight': 0.7,
                        'OutVariable': 'recurringExpenseScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgInvestmentScore',
                        'Weight': 1.4,
                        'OutVariable': 'avgInvestmentScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEMIandNMIRatio',
                        'Weight': 0.75,
                        'OutVariable': 'emiandNMIRatioScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetAvgDailyBalanceScore',
                        'Weight': 1.40,
                        'OutVariable': 'avgDailyBalanceScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetSocialScore_NTC',
                        'Weight': 2.0,
                        'OutVariable': 'score',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetBehavioralScore',
                        'Weight': 0,
                        'OutVariable': 'behavioralScore',
                        'InputVariable': ''
                    }, {
                        'Name': 'GetEmploymentScore',
                        'Weight': 3.0,
                        'OutVariable': 'employmentScore',
                        'InputVariable': ''
                    }]
                }
            };
        }
        function validateRequiredInput(context, input) {
            var requiredPerfiosProperty = ['monthlyEmi', 'avgInvestmentExpensePerMonth', 'monthlyIncome', 'monthlyExpense', 'avgDailyBalance', 'salaryDate'];
            var requiredApplicationProperty = ['selfEmi', 'applicationDate', 'income', 'totalWorkExp', 'age'];
            var requiredcrifCreditReportProperty = ['fileType', 'inquiryScore', 'bureauEmi', 'noOfPLInquiries'];
            var requiredcompanyCategoryReportProperty = ['companyCategory'];
            var requiredCalculateInitialOfferProperty = ['score'];
          
            var errorData = [];
            var bureauData;
            var errorMessage;
            if (typeof (input) != 'undefined') {
                var companyCategoryReport = input.companyCategoryReport;
                if (typeof (companyCategoryReport) != 'undefined') {
                    for (var i = 0; i < requiredcompanyCategoryReportProperty.length; i++) {
                        var el = companyCategoryReport[requiredcompanyCategoryReportProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredcompanyCategoryReportProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    ;
                }
                else {
                    errorMessage = 'Company Category Report Property data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                var CalculateInitialOffer = input.CalculateInitialOffer.CalculateInitialOffer.offerData;
                if (typeof (CalculateInitialOffer) != 'undefined') {
                    for (var i = 0; i < requiredCalculateInitialOfferProperty.length; i++) {
                        var el = CalculateInitialOffer[0][requiredCalculateInitialOfferProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredCalculateInitialOfferProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                        else { }
                    }
                    ;
                }
                else {
                    errorMessage = 'CalculateInitialOffer.CalculateInitialOffer Score Property data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                if (typeof (input.crifCreditReport) == 'undefined' && typeof (input.cibilReport) == 'undefined') {
                    errorMessage = 'bureau data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'rejectCode': '',
                        'data': null,
                        'exception': errorData
                    };
                }
                else {
                    bureauData = context.call('data_attribute_getbureauData', [input.crifCreditReport, input.cibilReport]);
                    if (typeof (bureauData) != 'undefined') {
                        for (var i = 0; i < requiredcrifCreditReportProperty.length; i++) {
                            var el = bureauData[requiredcrifCreditReportProperty[i]];
                            if (typeof (el) === 'undefined') {
                                errorMessage = 'Input property : ' + requiredcrifCreditReportProperty[i] + ' not found.';
                                errorData.push(errorMessage);
                            }
                            ;
                        }
                        ;
                    }
                    else {
                        errorMessage = 'credit report data not found';
                        errorData.push(errorMessage);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'rejectCode': '',
                            'exception': errorData
                        };
                    }
                }

              

                var companyCategoryReport = input.companyCategoryReport;
                if (typeof (companyCategoryReport) != 'undefined') {
                    for (var i = 0; i < requiredcompanyCategoryReportProperty.length; i++) {
                        var el = companyCategoryReport[requiredcompanyCategoryReportProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredcompanyCategoryReportProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    ;
                }
                else {
                    errorMessage = 'Company Category Report Property data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                var application = input.application;
                if (typeof (application) != 'undefined') {
                    for (var i = 0; i < requiredApplicationProperty.length; i++) {
                        var el = application[requiredApplicationProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredApplicationProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    ;
                }
                else {
                    errorMessage = 'application data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                var bankData;
                if (typeof (input.bankPreferenceData) != 'undefined' && input.bankPreferenceData.BankPreferenceSource != null) {
                    if (input.bankPreferenceData.BankPreferenceSource.toLowerCase() == 'manual') {
                        bankData = typeof (input.manualCashFlowReport) != 'undefined' ? input.manualCashFlowReport : undefined;
                    }
                    else if (input.bankPreferenceData.BankPreferenceSource.toLowerCase() == 'yodlee') {
                        bankData = typeof (input.yodleeCashFlowReport) != 'undefined' ? input.yodleeCashFlowReport : undefined;
                    }
                    else {
                        bankData = typeof (input.perfiosReport) != 'undefined' ? input.perfiosReport : undefined;
                    }
                } else {
                    var perfios = input.perfiosReport;
                    var yodlee = input.yodleeCashFlowReport;
                    var manualReport = input.manualCashFlowReport;
                    if (typeof (yodlee) != 'undefined') {
                        bankData = yodlee;
                    }
                    else if (typeof (perfios) != 'undefined') {
                        bankData = perfios;
                    }
                    else if (typeof (manualReport) != 'undefined') {
                        bankData = manualReport;
                    } else {
                        errorMessage = 'Bank data not found';
                        errorData.push(errorMessage);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'rejectCode': '',
                            'exception': errorData
                        };
                    }
                }

                if (typeof (bankData) != 'undefined') {
                    for (var i = 0; i < requiredPerfiosProperty.length; i++) {
                        var el = bankData[requiredPerfiosProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredPerfiosProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    input.perfiosReport = bankData;
                } else {
                    errorMessage = 'Bank data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'rejectCode': '',
                        'exception': errorData
                    };
                }


                if (errorData.length > 0) {
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                else {
                    return {
                        'result': true,
                        'detail': null,
                        'data': bureauData,
                        'rejectCode': ''
                    };
                }
            }
            else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
        }
        ;
        function validateRequiredClientscorecardInput(input) {
            var requiredclientScoreCardProperty = ['score'];
            var geoRiskRequiredProperty = ['ZoneClassification', 'geoRiskRanking'];
            var errorData = [];
            var errorMessage;
            if (typeof (input) != 'undefined') {
                var clientScoreCard = input.clientScoreCard;
                if (typeof (clientScoreCard) != 'undefined') {
                    for (var i = 0; i < requiredclientScoreCardProperty.length; i++) {
                        var el = clientScoreCard[requiredclientScoreCardProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + requiredclientScoreCardProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    ;
                }
                else {
                    errorMessage = 'Client Score data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }

                var geoRiskPinCode = input.GeoRiskEligibility.GeoRiskEligibility;
                if (typeof (geoRiskPinCode) != 'undefined') {
                    for (var i = 0; i < geoRiskRequiredProperty.length; i++) {
                        var el = geoRiskPinCode[geoRiskRequiredProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + geoRiskRequiredProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                        ;
                    }
                    ;
                }
                else {
                    errorMessage = 'GeoRisk Classification data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'rejectcode': '',
                        'exception': errorData
                    };
                }


                if (errorData.length > 0) {
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                else {
                    return {
                        'result': true,
                        'detail': errorData,
                        'data': null,
                        'rejectCode': ''
                    };
                }
            }
            else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
        }
        

        var objValidateInput = validateRequiredInput(this, payload);
        if (objValidateInput.result) {
            var bureauData = objValidateInput.data;
            if (bureauData.fileType.toLowerCase() == 'thin' || bureauData.fileType.toLowerCase() == 'ntc') {
                var objValidateInputforClientScore = validateRequiredClientscorecardInput(payload);
                if (!objValidateInputforClientScore.result) {
                    return objValidateInputforClientScore;
                }
            }
            return GetValidations(payload, bureauData);
        }
        else {
            return objValidateInput;
        }
    }
    catch (e) {
        return {
            'result': false,
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };
    }
}


function initialoffer_compute(payload) {
    try {
        function validateRequiredInput(context, input) {
            var apprequiredProperty = ['age', 'purposeOfLoan', 'gender', 'maritalStatus', 'residenceType', 'selfEmi', 'requestedAmount', 'monthlyExpense', 'monthlyRent', 'income'];
            var crifRequiredProperty = ['bureauScore', 'creditCardBalance', 'creditCardLimit', 'bureauEmi', 'noOfOpenedTrades', 'delinquencyFlag', 'bureauTenure', 'homeLoan', 'noOfPLInquiries', 'noOfCreditCard', 'unsecuredLoanBalance', 'unsecuredLoanLimit', 'youngestUnsecuredTrageAgeInMonths', 'fileType', 'revolvingTrades'];
            var geoRiskRequiredProperty = ['ZoneClassification'];
            var errorData = [];
            var errorMessage;
            var bureauData;
            if (typeof (input) != 'undefined') {
                var application = input.application;
                if (typeof (application) != 'undefined') {
                    for (var i = 0; i < apprequiredProperty.length; i++) {
                        var el = application[apprequiredProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + apprequiredProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                }
                else {
                    errorMessage = 'application data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'rejectcode': '',
                        'data': null,
                        'exception': errorData
                    };
                }
                var geoRiskPinCode = input.GeoRiskEligibility.GeoRiskEligibility;
                if (typeof (geoRiskPinCode) != 'undefined') {
                    for (var i = 0; i < geoRiskRequiredProperty.length; i++) {
                        var el = geoRiskPinCode[geoRiskRequiredProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + geoRiskRequiredProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                        ;
                    }
                    ;
                }
                else {
                    errorMessage = 'GeoRisk Classification data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'rejectcode': '',
                        'exception': errorData
                    };
                }
                if (typeof (input.crifCreditReport) == 'undefined' && typeof (input.cibilReport) == 'undefined') {
                    errorMessage = 'bureau data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'rejectcode': '',
                        'data': null,
                        'exception': errorData
                    };
                }
                else {
                    bureauData = context.call('data_attribute_getbureauData', [input.crifCreditReport, input.cibilReport]);
                    if (typeof (bureauData) != 'undefined') {
                        for (var i = 0; i < crifRequiredProperty.length; i++) {
                            var el = bureauData[crifRequiredProperty[i]];
                            if (typeof (el) === 'undefined') {
                                errorMessage = 'Input property : ' + crifRequiredProperty[i] + ' not found.';
                                errorData.push(errorMessage);
                            }
                            ;
                        }
                        ;
                    }
                    else {
                        errorMessage = 'credit report data not found';
                        errorData.push(errorMessage);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'rejectcode': '',
                            'exception': errorData
                        };
                    }
                }
                if (errorData.length > 0) {
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'rejectcode': '',
                        'exception': errorData
                    };
                }
                else {
                    return {
                        'result': true,
                        'detail': errorData,
                        'data': bureauData,
                        'rejectcode': ''
                    };
                }
            }
            else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'rejectcode': '',
                    'detail': null,
                    'data': null,
                    'exception': errorData
                };
            }
        }

        function validateRequiredExperianInput(input) {

            var geoRiskRequiredProperty = ['geoRiskRanking'];
            var errorData = [];
            var errorMessage;
            if (typeof (input) != 'undefined') {

                var geoRiskPinCode = input.GeoRiskEligibility.GeoRiskEligibility;
                if (typeof (geoRiskPinCode) != 'undefined') {
                    for (var i = 0; i < geoRiskRequiredProperty.length; i++) {
                        var el = geoRiskPinCode[geoRiskRequiredProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + geoRiskRequiredProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                        ;
                    }
                    ;
                }
                else {
                    errorMessage = 'GeoRisk Classification data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'rejectcode': '',
                        'exception': errorData
                    };
                }


                if (errorData.length > 0) {
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData,
                        'rejectCode': ''
                    };
                }
                else {
                    return {
                        'result': true,
                        'detail': errorData,
                        'data': null,
                        'rejectCode': ''
                    };
                }
            }
            else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'exception': errorData,
                    'rejectCode': ''
                };
            }
        }
        function getMonthlyExpense(fileType, applicationData, minIncome) {
         
         
            if (fileType.toLowerCase() == 'thick') {
                return applicationData.income * 0.2;
            }
            else if (fileType.toLowerCase() == 'thin') {
                return applicationData.income * 0.25;
            }
            else if (fileType.toLowerCase() == 'ntc') {
                return applicationData.income * 0.3;
            }
        
        }

        function getMaxpermittedEMI(filetype, income, maxEMINMIRatioless50k, maxEMINMIRatiogreater50k, currentEMINMI, netMonthlyIncome, minIncome, monthlyExpense, monthlyRent) {
            var netMonthlyratio = 0;
            if (filetype.toLowerCase() == 'thick') {
                netMonthlyratio = 0.65;
            }
            else if (filetype.toLowerCase() == 'thin') {
                netMonthlyratio = 0.5;
            }
            else if (filetype.toLowerCase() == 'ntc') {
                netMonthlyratio = 0.4;
            }
            var maxPermittedEMI = 0.0;
            if (income < minIncome) {
                maxPermittedEMI = Math.min((income * (maxEMINMIRatioless50k - currentEMINMI)), (netMonthlyratio * (netMonthlyIncome)));
            }
            else {
                maxPermittedEMI = Math.min((income * (maxEMINMIRatiogreater50k - currentEMINMI)), (0.80 * income - monthlyExpense));
            }
            return maxPermittedEMI;
        }

        function calculateOffer(applicationData, bureauData, initialScore, fileType, loanTenure) {

            var maxOfferAmountgreater50k = 750000;
            var minIncome = 50000;
            var finalOfferAmount = 0;
            var maxPermittedEMI = 0;
            var maxEMINMIRatioless50k = 0.55;
            var maxEMINMIRatiogreater50k = 0.6;
            var monthlyExpense = 0;
            var netMonthlyIncome = 0;
            var currentEMINMI = 0;
            var maxPermittedEMI = 0;
            var offerAmount = 0.0;
            var finalOfferAmount = 0;
            var selfEmi = Math.max(applicationData.selfEmi, bureauData.bureauEmi);
            var emiDeviation = 0;
            emiDeviation = (bureauData.bureauEmi - applicationData.selfEmi) * 100 / bureauData.bureauEmi;

            var interestRate = getInterestRate('application', fileType, initialScore, loanTenure);
            if (interestRate > 0) {
                monthlyExpense = getMonthlyExpense(fileType, applicationData, minIncome);
                monthlyExpense = applicationData.income>=minIncome ? (monthlyExpense + selfEmi) : monthlyExpense; 
                netMonthlyIncome = applicationData.income - monthlyExpense - selfEmi;
                var expenseNMIRatio = (monthlyExpense / applicationData.income) * 100;
                var currentEMINMI = (selfEmi / applicationData.income);
                currentEMINMI = parseFloat(currentEMINMI.toFixed(4));

                maxPermittedEMI = getMaxpermittedEMI(fileType, applicationData.income, maxEMINMIRatioless50k, maxEMINMIRatiogreater50k, currentEMINMI, netMonthlyIncome, minIncome, monthlyExpense, applicationData.monthlyRent);
                var rate = (interestRate / 100) / 12;
                var featurevalue = 0;
                offerAmount = -pv(rate, loanTenure, maxPermittedEMI, featurevalue);
                if (applicationData.income < minIncome) {
                    finalOfferAmount = Math.min(offerAmount, maxOfferAmountgreater50k, applicationData.requestedAmount);
                }
                else {
                    finalOfferAmount = Math.min(offerAmount, maxOfferAmountgreater50k, applicationData.requestedAmount);
                }
                finalOfferAmount = finalOfferAmount / 1000;
                finalOfferAmount = parseInt(finalOfferAmount.toString()) * 1000;
                var offerEligibilityCheck = offerAmountEligibilityRule(applicationData, finalOfferAmount);
                if (offerEligibilityCheck.isEligible) {
                    var finalData = {
                        'fileType': fileType,
                        'score': initialScore,
                        'loantenure': loanTenure,
                        'interestRate': interestRate,
                        'monthlyExpense': parseFloat(monthlyExpense.toFixed(2)),
                        'netMonthlyIncome': netMonthlyIncome,
                        'currentEMINMI': (currentEMINMI * 100),
                        'maxPermittedEMI': maxPermittedEMI,
                        'offerAmount': offerAmount,
                        'consideredEMI': selfEmi,
                        'emiDeviation': parseFloat(emiDeviation.toFixed(2)),
                        'finalOfferAmount': finalOfferAmount,
                        'expenseNMIRatio': parseFloat(expenseNMIRatio.toFixed(2)),
                        'consideredNMI': applicationData.income
                    };
                    return {
                        'name': 'calculateOffer',
                        'isEligible': true,
                        'rejectreason': '',
                        'reason': null,
                        'data': finalData,
                        'exception': ''
                    };
                }
                else {
                    var finalData = {
                        'fileType': fileType,
                        'loantenure': loanTenure,
                        'score': initialScore,
                        'interestRate': interestRate,
                        'monthlyExpense': parseFloat(monthlyExpense.toFixed(2)),
                        'netMonthlyIncome': netMonthlyIncome,
                        'currentEMINMI': currentEMINMI * 100,
                        'maxPermittedEMI': maxPermittedEMI,
                        'consideredEMI': selfEmi,
                        'emiDeviation': parseFloat(emiDeviation.toFixed(2)),
                        'offerAmount': offerAmount,
                        'finalOfferAmount': finalOfferAmount,
                        'expenseNMIRatio': parseFloat(expenseNMIRatio.toFixed(2)),
                        'consideredNMI': applicationData.income
                    };
                    offerEligibilityCheck.data = finalData;
                    return offerEligibilityCheck;
                }
                
            }
            else {
                var message = 'Interest rate not found for score : ' + initialScore + ' ,for loanTenure : ' + loanTenure;
                var finalData = {
                    'fileType': fileType,
                    'interestRate': interestRate,
                    'loantenure': loanTenure,
                    'score': initialScore,
                    'monthlyExpense': parseFloat(monthlyExpense.toFixed(2)),
                    'netMonthlyIncome': netMonthlyIncome,
                    'currentEMINMI': currentEMINMI * 100,
                    'maxPermittedEMI': maxPermittedEMI,
                    'consideredEMI': selfEmi,
                    'emiDeviation': parseFloat(emiDeviation.toFixed(2)),
                    'offerAmount': offerAmount,
                    'finalOfferAmount': finalOfferAmount,
                    'expenseNMIRatio': 0,
                    'consideredNMI': applicationData.income
                };
                return {
                    'name': 'calculateOffer',
                    'isEligible': false,
                    'rejectreason': '',
                    'reason': null,
                    'data': finalData,
                    'exception': message
                };
            }
        }
        ;
        function conv_number(expr, decplaces) {
            var str = '' + Math.round(eval(expr) * Math.pow(10, decplaces));
            while (str.length <= decplaces) {
                str = '0' + str;
            }
            var decpoint = str.length - decplaces;
            return (str.substring(0, decpoint) + '.' + str.substring(decpoint, str.length));
        }
        ;
        function pv(rate, nper, pmt, fv) {
            var pv_value;
            var x;
            var y;
            rate = parseFloat(rate);
            nper = parseFloat(nper);
            pmt = parseFloat(pmt);
            fv = parseFloat(fv);
            if (rate == 0) {
                pv_value = -(fv + (pmt * nper));
            }
            else {
                x = Math.pow(1 + rate, -nper);
                y = Math.pow(1 + rate, nper);
                pv_value = -(x * (fv * rate - pmt + y * pmt)) / rate;
            }
            pv_value = conv_number(pv_value, 2);
            return (pv_value);
        }
        ;
        function getInterestRate(entityType, fileType, initialScore, year) {
            var interestRateFile = JSON.parse('{\"EntityType\":\"application\",\"ProductId\":\"abc\",\"OfferType\":\"InitialOffer\",\"interestRateLookUp\":[{\"FileType\":\"Thick\",\"interestYears\":[{\"Year\":12,\"intrestScoreRange\":[{\"ScoreFrom\":65,\"ScoreTo\":70,\"Rate\":12},{\"ScoreFrom\":60,\"ScoreTo\":65,\"Rate\":13},{\"ScoreFrom\":56,\"ScoreTo\":60,\"Rate\":14},{\"ScoreFrom\":52,\"ScoreTo\":56,\"Rate\":15},{\"ScoreFrom\":49,\"ScoreTo\":52,\"Rate\":16},{\"ScoreFrom\":46,\"ScoreTo\":49,\"Rate\":17},{\"ScoreFrom\":43,\"ScoreTo\":46,\"Rate\":18},{\"ScoreFrom\":40,\"ScoreTo\":43,\"Rate\":19},{\"ScoreFrom\":37,\"ScoreTo\":40,\"Rate\":20},{\"ScoreFrom\":34,\"ScoreTo\":37,\"Rate\":21},{\"ScoreFrom\":0,\"ScoreTo\":34,\"Rate\":22}]},{\"Year\":36,\"intrestScoreRange\":[{\"ScoreFrom\":69,\"ScoreTo\":70,\"Rate\":14},{\"ScoreFrom\":66,\"ScoreTo\":68,\"Rate\":14.5},{\"ScoreFrom\":63,\"ScoreTo\":65,\"Rate\":15},{\"ScoreFrom\":61,\"ScoreTo\":62,\"Rate\":15.5},{\"ScoreFrom\":59,\"ScoreTo\":60,\"Rate\":16},{\"ScoreFrom\":57,\"ScoreTo\":58,\"Rate\":16.5},{\"ScoreFrom\":55,\"ScoreTo\":56,\"Rate\":17},{\"ScoreFrom\":53,\"ScoreTo\":54,\"Rate\":17.5},{\"ScoreFrom\":51,\"ScoreTo\":52,\"Rate\":18},{\"ScoreFrom\":50,\"ScoreTo\":50,\"Rate\":18.5},{\"ScoreFrom\":49,\"ScoreTo\":49,\"Rate\":19},{\"ScoreFrom\":48,\"ScoreTo\":48,\"Rate\":19.5},{\"ScoreFrom\":47,\"ScoreTo\":47,\"Rate\":20},{\"ScoreFrom\":46,\"ScoreTo\":46,\"Rate\":20.5},{\"ScoreFrom\":45,\"ScoreTo\":45,\"Rate\":21},{\"ScoreFrom\":44,\"ScoreTo\":44,\"Rate\":21.5},{\"ScoreFrom\":43,\"ScoreTo\":43,\"Rate\":22},{\"ScoreFrom\":42,\"ScoreTo\":42,\"Rate\":22.5},{\"ScoreFrom\":41,\"ScoreTo\":41,\"Rate\":23},{\"ScoreFrom\":40,\"ScoreTo\":40,\"Rate\":23.5},{\"ScoreFrom\":39,\"ScoreTo\":39,\"Rate\":24},{\"ScoreFrom\":38,\"ScoreTo\":38,\"Rate\":24.5},{\"ScoreFrom\":37,\"ScoreTo\":37,\"Rate\":25},{\"ScoreFrom\":36,\"ScoreTo\":36,\"Rate\":25.5},{\"ScoreFrom\":35,\"ScoreTo\":35,\"Rate\":26},{\"ScoreFrom\":34,\"ScoreTo\":34,\"Rate\":26.5},{\"ScoreFrom\":33,\"ScoreTo\":33,\"Rate\":27},{\"ScoreFrom\":32,\"ScoreTo\":32,\"Rate\":27.5},{\"ScoreFrom\":0,\"ScoreTo\":31,\"Rate\":28}]}]},{\"FileType\":\"Thin\",\"interestYears\":[{\"Year\":12,\"intrestScoreRange\":[{\"ScoreFrom\":38,\"ScoreTo\":40,\"Rate\":14},{\"ScoreFrom\":36,\"ScoreTo\":38,\"Rate\":15},{\"ScoreFrom\":34,\"ScoreTo\":36,\"Rate\":16},{\"ScoreFrom\":32,\"ScoreTo\":34,\"Rate\":17},{\"ScoreFrom\":30,\"ScoreTo\":32,\"Rate\":18},{\"ScoreFrom\":28,\"ScoreTo\":30,\"Rate\":19},{\"ScoreFrom\":26,\"ScoreTo\":28,\"Rate\":20},{\"ScoreFrom\":24,\"ScoreTo\":26,\"Rate\":21},{\"ScoreFrom\":22,\"ScoreTo\":24,\"Rate\":22},{\"ScoreFrom\":20,\"ScoreTo\":22,\"Rate\":23},{\"ScoreFrom\":0,\"ScoreTo\":20,\"Rate\":24}]},{\"Year\":36,\"intrestScoreRange\":[{\"ScoreFrom\":39,\"ScoreTo\":50,\"Rate\":15.5},{\"ScoreFrom\":37,\"ScoreTo\":38,\"Rate\":16},{\"ScoreFrom\":35,\"ScoreTo\":36,\"Rate\":16.5},{\"ScoreFrom\":33,\"ScoreTo\":34,\"Rate\":17},{\"ScoreFrom\":31,\"ScoreTo\":32,\"Rate\":17.5},{\"ScoreFrom\":29,\"ScoreTo\":30,\"Rate\":18},{\"ScoreFrom\":27,\"ScoreTo\":28,\"Rate\":18.5},{\"ScoreFrom\":25,\"ScoreTo\":26,\"Rate\":19},{\"ScoreFrom\":23,\"ScoreTo\":24,\"Rate\":19.5},{\"ScoreFrom\":21,\"ScoreTo\":22,\"Rate\":20},{\"ScoreFrom\":0,\"ScoreTo\":20,\"Rate\":20.5}]}]},{\"FileType\":\"NTC\",\"interestYears\":[{\"Year\":12,\"intrestScoreRange\":[{\"ScoreFrom\":34,\"ScoreTo\":36,\"Rate\":14},{\"ScoreFrom\":32,\"ScoreTo\":34,\"Rate\":15},{\"ScoreFrom\":30,\"ScoreTo\":32,\"Rate\":16},{\"ScoreFrom\":28,\"ScoreTo\":30,\"Rate\":17},{\"ScoreFrom\":26,\"ScoreTo\":28,\"Rate\":18},{\"ScoreFrom\":24,\"ScoreTo\":26,\"Rate\":19},{\"ScoreFrom\":22,\"ScoreTo\":24,\"Rate\":20},{\"ScoreFrom\":20,\"ScoreTo\":22,\"Rate\":21},{\"ScoreFrom\":18,\"ScoreTo\":20,\"Rate\":22},{\"ScoreFrom\":16,\"ScoreTo\":18,\"Rate\":23},{\"ScoreFrom\":0,\"ScoreTo\":16,\"Rate\":24}]},{\"Year\":36,\"intrestScoreRange\":[{\"ScoreFrom\":35,\"ScoreTo\":38,\"Rate\":15.5},{\"ScoreFrom\":33,\"ScoreTo\":34,\"Rate\":16},{\"ScoreFrom\":31,\"ScoreTo\":32,\"Rate\":16.5},{\"ScoreFrom\":29,\"ScoreTo\":30,\"Rate\":17},{\"ScoreFrom\":27,\"ScoreTo\":28,\"Rate\":17.5},{\"ScoreFrom\":25,\"ScoreTo\":26,\"Rate\":18},{\"ScoreFrom\":23,\"ScoreTo\":24,\"Rate\":18.5},{\"ScoreFrom\":21,\"ScoreTo\":22,\"Rate\":19},{\"ScoreFrom\":19,\"ScoreTo\":20,\"Rate\":19.5},{\"ScoreFrom\":0,\"ScoreTo\":18,\"Rate\":20}]}]}]}');
            var interestYear;
            var interestScoreRange;
            var interestRate = 0;
            if (interestRateFile.EntityType == entityType) {
                for (var i = 0; i < interestRateFile.interestRateLookUp.length; i++) {
                    if (interestRateFile.interestRateLookUp[i].FileType.toLowerCase() == fileType.toLowerCase()) {
                        interestYear = interestRateFile.interestRateLookUp[i].interestYears;
                        break;
                    }
                }
                if (typeof (interestYear) != 'undefined') {
                    for (var k = 0; k < interestYear.length; k++) {
                        if (interestYear[k].Year == year) {
                            interestScoreRange = interestYear[k].intrestScoreRange;
                            break;
                        }
                    }
                    for (var j = 0; j < interestScoreRange.length; j++) {
                        if (Math.ceil(initialScore) >= interestScoreRange[j].ScoreFrom && Math.ceil(initialScore) <= interestScoreRange[j].ScoreTo) {
                            interestRate = interestScoreRange[j].Rate;
                            break;
                        }
                    }
                }
                else {
                    interestRate = 0;
                }
            }
            return interestRate;
        }
        ;
        function offerAmountEligibilityRule(input, finalOfferAmount) {
            var requestedAmount = input.requestedAmount;
            var offerAmount = finalOfferAmount;
            if (offerAmount < 25000 && input.purposeOfLoan!='loanrefinancing') {
                var message = 'LOWELIGIBILITY';
                return {
                    'name': 'OfferAmountEligibilityRule',
                    'isEligible': false,
                    'rejectreason': 'Low Eligibility',
                    'reason': message,
                    'data': offerAmount,
                    'exception': ''
                };
            }
            else {
                return {
                    'name': 'OfferAmountEligibilityRule',
                    'isEligible': true,
                    'rejectreason': '',
                    'reason': null,
                    'data': offerAmount,
                    'exception': ''
                };
            }
        }
        ;
        function getScoreWeight(fileType) {
            var scoreWeightFile = JSON.parse('{\"scoreweight\":[{\"FileType\":\"Thick\",\"AssignedWeight\":{\"BureauScoreweight\":0.80,\"CreditCardBalanceweight\":0.5,\"OpenedTradeweight\":1.00,\"Delinquencyweight\":1.00,\"BureauTenureweight\":1.00,\"HomeLoanweight\":1.00,\"PLInquiriesweight\":3.5,\"NoOfCreditCardweight\":1.00,\"UnsecuredLoanweight\":1.00,\"YoungestUnsecuredTradeweight\":1.00,\"RevolvingTradesweight\":1.00,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.00,\"ApplicantAgeweight\":0.15,\"ApplicantGenderweight\":0.15,\"ApplicantMaritalStatusweight\":0.20,\"ApplicantResidenceTypeweight\":1}},{\"FileType\":\"Thin\",\"AssignedWeight\":{\"BureauScoreweight\":4.00,\"OpenedTradeweight\":0.80,\"BureauTenureweight\":0.4,\"PLInquiriesweight\":1.25,\"NoOfCreditCardweight\":0.5,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.0,\"ApplicantAgeweight\":0.30,\"ApplicantGenderweight\":0.30,\"ApplicantMaritalStatusweight\":0.20,\"ApplicantResidenceTypeweight\":1.5}},{\"FileType\":\"NTC\",\"AssignedWeight\":{\"BureauScoreweight\":5.00,\"PLInquiriesweight\":0.5,\"LoanPurposeweight\":0,\"Zipcodeweight\":1.25,\"ApplicantAgeweight\":0.40,\"ApplicantGenderweight\":0.40,\"ApplicantMaritalStatusweight\":0.40,\"ApplicantResidenceTypeweight\":1.5}}]}');
            var assignedWeight;
            var leng = scoreWeightFile.scoreweight.length;
            for (var i = 0; i < leng; i++) {
                if (scoreWeightFile.scoreweight[i].FileType == fileType) {
                    assignedWeight = scoreWeightFile.scoreweight[i].AssignedWeight;
                    break;
                }
            }
            return assignedWeight;
        }
        ; /* Thick File Score */
        function getThickFileInitialBureauScore(applicationData, crifData, zipCodeData) {
            var scoreWeight = getScoreWeight('Thick');
            var data = {
                bureauScore: getBureauScore(crifData, scoreWeight.BureauScoreweight),
                creditCardScore: getCreditCardBalanceScore(crifData, scoreWeight.CreditCardBalanceweight),
                openedTradeScore: getOpenedTradeScore(crifData, scoreWeight.OpenedTradeweight),
                delinquencyScore: getDelinquencyScore(crifData, scoreWeight.Delinquencyweight),
                bureauTenureScore: getBureauTenureScore(crifData, scoreWeight.BureauTenureweight),
                homeLoanScore: getHomeLoanScore(crifData, scoreWeight.HomeLoanweight),
                plInquiriesScore: getPLInquiriesScore(crifData, scoreWeight.PLInquiriesweight),
                noOfCreditCardScore: getNoOfCreditCardScore(crifData, scoreWeight.NoOfCreditCardweight),
                unsecuredLoanScore: getUnsecuredLoanScore(crifData, scoreWeight.UnsecuredLoanweight),
                youngestUnsecuredTradeScore: getYoungestUnsecuredTradeScore(crifData, scoreWeight.YoungestUnsecuredTradeweight),
                revolvingTradesScore: getRevolvingTradesScore(crifData, applicationData, scoreWeight.RevolvingTradesweight),
                loanPurposeScore: getLoanPurposeScore(applicationData, scoreWeight.LoanPurposeweight),
                zipcodeScore: getZipCodeScore(zipCodeData, scoreWeight.Zipcodeweight),
                applicantAgeScore: getApplicantAgeScore(applicationData, scoreWeight.ApplicantAgeweight),
                applicantGenderScore: getApplicantGenderScore(applicationData, scoreWeight.ApplicantGenderweight),
                applicantMaritalStatusScore: getApplicantMaritalStatusScore(applicationData, scoreWeight.ApplicantMaritalStatusweight),
                applicantResidenceTypeScore: getApplicantResidenceTypeScore(applicationData, scoreWeight.ApplicantResidenceTypeweight)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));

            if (weightedScore < 0) {
                weightedScore = 0;
            }

            var revolvingFlag = crifData.revolvingTrades == 0 ? 'NA' : ((crifData.creditCardBalance / applicationData.income >= 4) ? 'Yes' : 'No');
            data['revolvingIndex'] = revolvingFlag;
            return {
                'initialScore': weightedScore,
                'scoreData': data
            };
        }

        function getBureauScore(input, weightAssigned) {
            var bureauScore = input.bureauScore;
            if (bureauScore > 740) {
                bureauScore = 740;
            }
            ;
            var finalscore = ((bureauScore - 625) / 3.29) * weightAssigned;
            if (finalscore < 0) {
                finalscore = 0;
            }
            ;
            return finalscore;
        }
        ;
        function getCreditCardBalanceScore(input, weightAssigned) {
            var creditCardBalance = input.creditCardBalance;
            var highCreditOrSanctionedAmount = input.highCreditOrSanctionedAmount;
            var creditCardLimit = input.creditCardLimit != null ? input.creditCardLimit : highCreditOrSanctionedAmount;
            var creditCardScore = 0;
            if (creditCardLimit != null && creditCardLimit > 0) {
                var utilizationBand = (creditCardBalance / creditCardLimit) * 100;
                if (utilizationBand <= 10) {
                    creditCardScore = 4;
                }
                else if (utilizationBand > 10 && utilizationBand <= 20) {
                    creditCardScore = 2;
                }
                else if (utilizationBand > 20 && utilizationBand <= 50) {
                    creditCardScore = 1;
                }
                else if (utilizationBand > 50) {
                    creditCardScore = 0;
                }
            }
            else {
                creditCardScore = 2;
            }
            return (creditCardScore * weightAssigned);
        }

        function getOpenedTradeScore(input, weightAssigned) {
            var noOfOpenedTrade = input.noOfOpenedTrades;
            if (noOfOpenedTrade >= 0 && noOfOpenedTrade <= 2) {
                return (4 * weightAssigned);
            }
            else if (noOfOpenedTrade >= 3 && noOfOpenedTrade <= 4) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }

        function getDelinquencyScore(input, weightAssigned) {
            var delinquencyFlag = input.delinquencyFlag;
            if (delinquencyFlag) {
                return 0;
            }
            else if (!delinquencyFlag) {
                return (5 * weightAssigned);
            }
        }

        function getBureauTenureScore(input, weightAssigned) {
            var bureauTenure = input.bureauTenure;
            if (bureauTenure > 24) {
                return (4 * weightAssigned);
            }
            else if (bureauTenure > 12 && bureauTenure <= 24) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }

        function getHomeLoanScore(input, weightAssigned) {
            var homeloanflag = input.homeLoan;
            if (homeloanflag) {
                return (2 * weightAssigned);
            }
            else {
                return (1 * weightAssigned);
            }
        }
        ;
        function getPLInquiriesScore(input, weightAssigned) {
            var noOfPLInquiries = input.noOfPLInquiries;
            if (noOfPLInquiries <= 2) {
                return (2 * weightAssigned);
            }
            else if (noOfPLInquiries <= 4) {
                return (1 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getNoOfCreditCardScore(input, weightAssigned) {
            var noOfCreditCard = input.noOfCreditCard;
            if (noOfCreditCard <= 1) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getUnsecuredLoanScore(input, weightAssigned) {
            var unsecuredLoanBalance = input.unsecuredLoanBalance;
            var unsecuredLoanLimit = input.unsecuredLoanLimit;
            var unsecuredLoanScore = 2;
            if (unsecuredLoanBalance > 0 && unsecuredLoanLimit > 0) {
                var utilizationBand = (unsecuredLoanBalance / unsecuredLoanLimit) * 100;
                if (utilizationBand >= 75) {
                    unsecuredLoanScore = 0;
                }
                ;
            }
            return (unsecuredLoanScore * weightAssigned);
        }
        ;
        function getYoungestUnsecuredTradeScore(input, weightAssigned) {
            var youngestUnsecuredTrageAgeInMonths = input.youngestUnsecuredTrageAgeInMonths;
            var unsecuredTradeScore = 2;
            if (youngestUnsecuredTrageAgeInMonths > 0) {
                if (youngestUnsecuredTrageAgeInMonths <= 3) {
                    unsecuredTradeScore = 0;
                }
                else if (youngestUnsecuredTrageAgeInMonths > 3) {
                    unsecuredTradeScore = 2;
                }
            }
            return (unsecuredTradeScore * weightAssigned);
        }
        ;
        function getRevolvingTradesScore(input, application, weightAssigned) {
            var creditCardBalance = input.creditCardBalance;
            var revolvingTrades = input.revolvingTrades;
            var income = application.income;
            if (revolvingTrades == 0) {
                return (1 * weightAssigned);
            }
            if (creditCardBalance > 0 && income > 0) {
                if (creditCardBalance / income > 4) {
                    return 0;
                }
                else {
                    return (2 * weightAssigned);
                }
            } else {
                return (2 * weightAssigned);
            }

        }
        ;
        function getLoanPurposeScore(input, weightAssigned) {
            var lowestRiskLoanPurpose = ['loanrefinancing', 'vehiclepurchase'];
            var mediumRiskLoanPurpose = ['travel', 'homeimprovement', 'wedding', 'assetacquisition', 'business', 'agriculture', 'other'];
            var highestRiskLoanPurpose = ['education', 'medical'];
            var loanPurpose = input.purposeOfLoan;
            if (lowestRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (4 * weightAssigned);
            }
            else if (mediumRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (3 * weightAssigned);
            }
            else if (highestRiskLoanPurpose.indexOf(loanPurpose.toLowerCase()) > -1) {
                return (1 * weightAssigned);
            }
            else {
                return (3 * weightAssigned);
            }
        }
        ;
        function getZipCodeScore(input, weightAssigned) {
            var zipcodeRisk = input.ZoneClassification;
            if (zipcodeRisk.toLowerCase() === 'low risk') {
                return (4 * weightAssigned);
            }
            else if (zipcodeRisk.toLowerCase() === 'medium risk') {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getApplicantAgeScore(input, weightAssigned) {
            var age = input.age;
            if (age >= 23 && age <= 30) {
                return (2 * weightAssigned);
            }
            else if (age >= 31 && age <= 45) {
                return (4 * weightAssigned);
            }
            else {
                return (1 * weightAssigned);
            }
        }
        ;
        function getApplicantGenderScore(input, weightAssigned) {
            var gender = input.gender;
            if (gender.toLowerCase() === 'male') {
                return (2 * weightAssigned);
            }
            else if (gender.toLowerCase() === 'female') {
                return (4 * weightAssigned);
            }
            else {
                return (3 * weightAssigned);
            }
        }
        ;
        function getApplicantMaritalStatusScore(input, weightAssigned) {
            var maritalStatus = input.maritalStatus;
            if (maritalStatus.toLowerCase() === 'married') {
                return (4 * weightAssigned);
            }
            else if (maritalStatus.toLowerCase() === 'single') {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getApplicantResidenceTypeScore(input, weightAssigned) {
            var residenceType = input.residenceType;
            var highestRiskResidenceType = ['OwnedSelf', 'Owned-Family'];
            var mediumRiskResidenceType = ['Rented-Family', 'Company-Acco'];
            if (highestRiskResidenceType.indexOf(residenceType) > -1) {
                return (4 * weightAssigned);
            }
            else if (mediumRiskResidenceType.indexOf(residenceType) > -1) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ; /* NTC File Functions*/
        function getNTCFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport) {
            var scoreWeight = getScoreWeight('NTC');
            var data = {
                bureauScore: getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, scoreWeight.BureauScoreweight),
                plInquiriesScore: getNTCPLInquiriesScore(crifData, scoreWeight.PLInquiriesweight),
                zipcodeScore: getZipCodeScore(zipCodeData, scoreWeight.Zipcodeweight),
                loanPurposeScore: getLoanPurposeScore(applicationData, scoreWeight.LoanPurposeweight),
                applicantAgeScore: getApplicantAgeScore(applicationData, scoreWeight.ApplicantAgeweight),
                applicantGenderScore: getApplicantGenderScore(applicationData, scoreWeight.ApplicantGenderweight),
                applicantMaritalStatusScore: getApplicantMaritalStatusScore(applicationData, scoreWeight.ApplicantMaritalStatusweight),
                applicantResidenceTypeScore: getApplicantResidenceTypeScore(applicationData, scoreWeight.ApplicantResidenceTypeweight)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));
            if (weightedScore < 0) {
                weightedScore = 0;
            }
            var ntcBureauData = getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport);

            for (var propertyName in ntcBureauData.ExpeianBureauData) {
                data[propertyName] = ntcBureauData.ExpeianBureauData[propertyName]
            }

            return {
                'initialScore': weightedScore,
                'scoreData': data
            };
        }

        function getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, weightAssigned) {
            var ntcScoreResponse = getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport);
            var NTCApplicationScore = 0;
            if (!isNaN(ntcScoreResponse.ntcScore)) {
                var ntcScore = parseInt(ntcScoreResponse.ntcScore);
                var fileCategory;
                if (ntcScore > 240) {
                    fileCategory = 'vl';
                }
                ;
                if (ntcScore >= 220 && ntcScore <= 240) {
                    fileCategory = 'l';
                }
                ;
                if (ntcScore >= 200 && ntcScore < 220) {
                    fileCategory = 'm';
                }
                ;
                if (ntcScore >= 180 && ntcScore < 200) {
                    fileCategory = 'h';
                }
                ;
                if (ntcScore < 180) {
                    fileCategory = 'vh';
                }
                ;
                if (crifData.fileType.toLowerCase() == 'thin') {
                    NTCApplicationScore = GetFileCategoryScoreforThin(fileCategory, weightAssigned);
                }
                else if (crifData.fileType.toLowerCase() == 'ntc') {
                    NTCApplicationScore = GetFileCategoryScoreforNTC(fileCategory, weightAssigned);
                }

            }
            return NTCApplicationScore;
        }

        function GetFileCategoryScoreforNTC(fileCategory, weightAssigned) {
            var NTCApplicationScore = 0;
            if (fileCategory === 'vl' || fileCategory === 'l') {
                NTCApplicationScore = 4 * weightAssigned;
            }
            else if (fileCategory === 'm') {
                NTCApplicationScore = 2 * weightAssigned;
            }
            else if (fileCategory === 'h' || fileCategory === 'vh') {
                NTCApplicationScore = 0;
            }
            return NTCApplicationScore;
        }

        function GetFileCategoryScoreforThin(fileCategory, weightAssigned) {
            var NTCApplicationScore = 0;
            if (fileCategory === 'vl') {
                NTCApplicationScore = 5 * weightAssigned;
            }
            else if (fileCategory === 'l') {
                NTCApplicationScore = 4 * weightAssigned;
            }
            else if (fileCategory === 'm') {
                NTCApplicationScore = 3 * weightAssigned;
            }
            else if (fileCategory === 'h') {
                NTCApplicationScore = 2 * weightAssigned;
            }
            else if (fileCategory === 'vh') {
                NTCApplicationScore = 1 * weightAssigned;
            }
            return NTCApplicationScore;
        }

        /*NTC Score*/
        function getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport) {
            var data = {
                ExperianBureauScore: getBureauScoreForNTC(crifData),
                ExperianCompanyDBScore: getNTCCompanyCategoryScore(companyCategoryReport),
                ExperianIncomeScrore: get_NTC_IncomeScore(applicationData),
                ExperianWorkingExpScore: getWorkingExperienceScore(applicationData),
                ExperianAgeScore: getAgeScore(applicationData),
                ExperianQualitificationScore: getQualifificationScore(applicationData),
                ExperianGeoRiskScore: getGeoRiskRankingScore(zipCodeData),
                ExperianLoanTermScore: getLoanTermScore(applicationData)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));
            if (weightedScore < 0) {
                weightedScore = 0;
            }
            return {
                'ntcScore': weightedScore,
                'ExpeianBureauData': data
            };
        }

        function getNTCCompanyCategoryScore(company) {
            var score = 0;
            if (company != null && typeof (company.experianCategory) != 'undefined') {
                if (company.experianCategory == 'A') {
                    score = 31;
                }
                else if (company.experianCategory == 'B' || company.experianCategory == 'C') {
                    score = 24;
                }
                else {
                    score = 24;
                }
            }
            return score;
        }
        function get_NTC_IncomeScore(applicationData) {
            var score = 0;
            if (applicationData.income < 22500) {
                score = 14;
            }
            ;
            if (applicationData.income < 27500 && applicationData.income >= 22500) {
                score = 22;
            }
            ;
            if (applicationData.income < 42500 && applicationData.income >= 27500) {
                score = 29;
            }
            ;
            if (applicationData.income < 77500 && applicationData.income >= 42500) {
                score = 46;
            }
            ;
            if (applicationData.income >= 77500) {
                score = 70;
            }
            ;
            return score;
        }
        function getBureauScoreForNTC(crifData) {
            var NTCBureauScore = crifData.noOfPLInquiries;
            var score = 0;
            if (NTCBureauScore == 0) {
                score = 26;
            }
            ;
            if (NTCBureauScore == 1) {
                score = 20;
            }
            ;
            if (NTCBureauScore >= 2) {
                score = 8;
            }
            ;
            return score;
        }
        function getWorkingExperienceScore(application) {
            var score = 0;
            var experienceinMonth = application.totalWorkExp * 12;
            if (experienceinMonth == null) {
                score = 13;
            }
            ;
            if (experienceinMonth <= 12) {
                score = 32;
            }
            ;
            if (experienceinMonth >= 13 && experienceinMonth <= 46) {
                score = 37;
            }
            ;
            if (experienceinMonth >= 47) {
                score = 43;
            }
            ;
            return score;
        }
        function getAgeScore(application) {
            var score = 0;
            if (application.age < 26) {
                score = 21;
            }
            ;
            if (application.age <= 35 && application.age >= 26) {
                score = 25;
            }
            ;
            if (application.age >= 36 && application.age <= 45) {
                score = 26;
            }
            ;
            if (application.age >= 46 && application.age <= 55) {
                score = 27;
            }
            ;
            if (application.age >= 56) {
                score = 24;
            }
            ;
            return score;
        }
        function getQualifificationScore(application) {
            var score = 25;
            if (application.educationalQualification != null) {
                if (application.educationalQualification == 'highschool') {
                    score = 14;
                }
                if (application.educationalQualification == 'bachelordegree') {
                    score = 25;
                }
                if (application.educationalQualification == 'masterdegree' || application.educationalQualification == 'doctratedegree') {
                    score = 28;
                }
            }
            return score;
        }
        function getGeoRiskRankingScore(zipCodeData) {
            var score = 17;
            if (zipCodeData.geoRiskRanking >= 1 && zipCodeData.geoRiskRanking <= 9) {
                score = 28;
            }

            if (zipCodeData.geoRiskRanking >= 10 && zipCodeData.geoRiskRanking <= 21) {
                score = 21;
            }

            return score;
        }
        function getLoanTermScore(application) {
            var score = 0;
            var requestedTermValue = 1;
            if (requestedTermValue >= 1 && requestedTermValue <= 12) {
                score = 42;
            }
            ;
            if (requestedTermValue >= 13 && requestedTermValue <= 24) {
                score = 38;
            }
            ;
            if (requestedTermValue >= 25 && requestedTermValue <= 36) {
                score = 30;
            }
            ;
            if (requestedTermValue >= 37) {
                score = 23;
            }
            ;
            return score;
        }
        function getNTCPLInquiriesScore(input, weightAssigned) {
            var noOfPLInquiries = input.noOfPLInquiries;
            if (noOfPLInquiries <= 2) {
                return (4 * weightAssigned);
            }
            else if (noOfPLInquiries <= 4) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ; /* Thin File Functions*/
        function getThinFileInitialBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport) {
            var scoreWeight = getScoreWeight('Thin');
            var data = {
                bureauScore: getExperianBureauScore(applicationData, crifData, zipCodeData, companyCategoryReport, scoreWeight.BureauScoreweight),
                openedTradeScore: getThinFileOpenedTradeScore(crifData, scoreWeight.OpenedTradeweight),
                bureauTenureScore: getThinFileBureauTenureScore(crifData, scoreWeight.BureauTenureweight),
                plInquiriesScore: getThinFilePLInquiriesScore(crifData, scoreWeight.PLInquiriesweight),
                noOfCreditCardScore: getThinFileNoOfCreditCardScore(crifData, scoreWeight.NoOfCreditCardweight),
                loanPurposeScore: getLoanPurposeScore(applicationData, scoreWeight.LoanPurposeweight),
                zipcodeScore: getZipCodeScore(zipCodeData, scoreWeight.Zipcodeweight),
                applicantAgeScore: getApplicantAgeScore(applicationData, scoreWeight.ApplicantAgeweight),
                applicantGenderScore: getApplicantGenderScore(applicationData, scoreWeight.ApplicantGenderweight),
                applicantMaritalStatusScore: getApplicantMaritalStatusScore(applicationData, scoreWeight.ApplicantMaritalStatusweight),
                applicantResidenceTypeScore: getApplicantResidenceTypeScore(applicationData, scoreWeight.ApplicantResidenceTypeweight)
            };
            var weightedScore = 0;
            for (var propertyName in data) {
                weightedScore += data[propertyName];
            }
            weightedScore = parseFloat(weightedScore.toFixed(2));
            if (weightedScore < 0) {
                weightedScore = 0;
            }
            var thinBureauData = getExperianScore(applicationData, crifData, zipCodeData, companyCategoryReport);

            for (var propertyName in thinBureauData.ExpeianBureauData) {
                data[propertyName] = thinBureauData.ExpeianBureauData[propertyName]
            }

            return {
                'initialScore': weightedScore,
                'scoreData': data
            };
        }

        function getThinFileOpenedTradeScore(input, weightAssigned) {
            var noOfOpenedTrade = input.noOfOpenedTrades;
            if (noOfOpenedTrade == 1) {
                return (4 * weightAssigned);
            }
            else if (noOfOpenedTrade == 2) {
                return (2 * weightAssigned);
            }
            else if (noOfOpenedTrade >= 3) {
                return 0;
            }
            else {
                return 0;
            }
        }
        ;
        function getThinFileBureauTenureScore(input, weightAssigned) {
            var bureauTenure = input.bureauTenure;
            if (bureauTenure >= 4) {
                return (4 * weightAssigned);
            }
            else if (bureauTenure >= 1 && bureauTenure <= 3) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getThinFilePLInquiriesScore(input, weightAssigned) {
            var noOfPLInquiries = input.noOfPLInquiries;
            if (noOfPLInquiries <= 2) {
                return (4 * weightAssigned);
            }
            else if (noOfPLInquiries <= 4) {
                return (2 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        function getThinFileNoOfCreditCardScore(input, weightAssigned) {
            var noOfCreditCard = input.noOfCreditCard;
            if (noOfCreditCard <= 1) {
                return (4 * weightAssigned);
            }
            else {
                return 0;
            }
        }
        ;
        var objValidateInput = validateRequiredInput(this, payload);
        if (objValidateInput.result) {
            var bureauData = objValidateInput.data;

            if (bureauData.fileType.toLowerCase() == 'thin' || bureauData.fileType.toLowerCase() == 'ntc') {
                var objValidateInputforClientScore = validateRequiredExperianInput(payload);
                if (!objValidateInputforClientScore.result) {
                    return objValidateInputforClientScore;
                }
            }
            var applicationData = payload.application;
            var companyCategoryReport = payload.companyCategoryReport;
            var zipCodeData = payload.GeoRiskEligibility.GeoRiskEligibility;
            var errorData = [];
            var calculatedOffer = [];
            var fileType;
            var objScoreData = {
                'initialScore': 0,
                'scoreData': null
            };
            fileType = bureauData.fileType;
            if (fileType.toLowerCase() == 'thick') {
                objScoreData = getThickFileInitialBureauScore(applicationData, bureauData, zipCodeData);
            }
            else if (fileType.toLowerCase() == 'thin') {
                objScoreData = getThinFileInitialBureauScore(applicationData, bureauData, zipCodeData, companyCategoryReport);
            }
            else if (fileType.toLowerCase() == 'ntc') {
                objScoreData = getNTCFileInitialBureauScore(applicationData, bureauData, zipCodeData, companyCategoryReport);
            }
            var status = false;
            var initialScore = 0;
            if (!isNaN(objScoreData.initialScore)) {
                initialScore = objScoreData.initialScore;
            };
            if (objScoreData.scoreData != null) {
                calculatedOffer.push(objScoreData.scoreData);
                calculatedOffer[0]['bureauSourceType'] = bureauData.sourceType;
            };


            if (initialScore > 0) {
                
                var loantenure = [36];
                for (var j = 0; j < loantenure.length; j++) {
                    var finalData = calculateOffer(applicationData, bureauData, initialScore, fileType, loantenure[j]);
                    if (finalData.isEligible) {
                        status = true;
                    };
                    if (finalData.data != null) {
                        var dataObj = finalData.data;
                        var keys = Object.keys(dataObj);
                        if (keys.length > 0) {
                            for (var i = 0; i < keys.length; i++) {
                                calculatedOffer[0][keys[i]] = dataObj[keys[i]];
                            }
                        }
                    };
                    if (finalData.reason != null) {
                        errorData.push(finalData.reason);
                    }
                    var exceptionDetail = [];

                    if (finalData.exception != null && finalData.exception != '') {
                        exceptionDetail.push(finalData.exception);
                    }

                }
                var objOfferData = {};
                objOfferData['offerData'] = calculatedOffer;
                if (!status) {
                    return {
                        'result': status,
                        'detail': errorData,
                        'data': {
                            'bureauSourceType': bureauData.sourceType
                        },
                        'rejectcode': 'LOWELIGIBILITY',
                        'exception': exceptionDetail
                    };
                }
                else {
                    return {
                        'result': status,
                        'detail': null,
                        'data': objOfferData,
                        'rejectcode': '',
                        'exception': exceptionDetail
                    };
                }
            }
            else {
                var message = 'InitialScore :  ' + initialScore + ' ,must be greater then 0.';
                errorData.push(message);
                return {
                    'result': false,
                    'rejectcode': '',
                    'detail': null,
                    'data': {
                        'bureauSourceType': bureauData.sourceType
                    },
                    'exception': errorData
                };
            }
        }
        else {
            return objValidateInput;
        }
    }
    catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'rejectcode': '',
            'detail': null,
            'data': null,
            'exception': errorData
        };
    }
}


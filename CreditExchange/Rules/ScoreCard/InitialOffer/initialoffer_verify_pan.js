function initialoffer_verify_pan(payload) {
    var errorData = [];
    var rejectcode = 'PANNOTVERIFIED';
    try {
		
 function validateRequiredInput(input) {
    var requiredProperty = ['score'];
    var errorData = [];
    var errorMessage;
    if (typeof (input) != 'undefined') {
        var crifPanData = input.crifPanReport;
        if (typeof (crifPanData) != 'undefined') {
            for (var i = 0; i < requiredProperty.length; i++) {
                var el = crifPanData[requiredProperty[i]];
                if (typeof (el) === 'undefined') {
                    errorMessage = 'input property : ' + requiredProperty[i] + ' not found.';
                    errorData.push(errorMessage);
                }
            }
            ;
            if (errorData.length > 0) {
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': errorData
                };
            }
            else {
                return {
                    'result': true,
                    'detail': errorData,
                    'data': null,
                    'rejectcode': ''
                };
            }
        }
        else {
            errorMessage = 'crif pan data not found';
            errorData.push(errorMessage);
            return {
                'result': false,
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': errorData
            };
        }
    }
    else {
        errorMessage = 'input data not found.';
        errorData.push(errorMessage);
        return {
            'result': false,
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': errorData
        };
    }
}


        var objValidateInput = validateRequiredInput(payload);
        if (objValidateInput.result) {
            var input = payload.crifPanReport;
            if (!isNaN(parseFloat(input.score))) {
                if (parseFloat(input.score) < 40) {
                    var message = 'PANNotVerfiied';
                    errorData.push(message);
                    return {
                        'result': false,
                        'rejectcode': rejectcode,
                        'detail': errorData,
                        'data': null
                    };
                }
                else {
                    return {
                        'result': true,
                        'rejectcode': rejectcode,
                        'detail': errorData,
                        'data': null
                    };
                }
            }
            else {
                var message = 'PANNotVerfiied';
                errorData.push(message);
                return {
                    'result': false,
                    'rejectcode': rejectcode,
                    'detail': errorData,
                    'data': null
                };
            }
        }
        else {
            return objValidateInput;
        }
    }
    catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'rejectcode': '',
            'detail': null,
            'data': null,
            'exception': errorData
        };
    }
}
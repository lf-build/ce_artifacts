function initialoffer_credit_eligibility(payload) {
	var errorData = [];
	var rejectcodevalue = 'CREDITREJECTION';
	try {
		function validateRequiredInput(context, input) {
			var apprequiredProperty = ['selfEmi', 'income'];
			var crifRequiredProperty = ['bureauScore', 'noOfPLInquiries', 'isDerogatoryStatus', 'creditCardBalance', 'isHighDPDTrade', 'bureauEmi', 'fileType'];
			var errorData = [];
			var errorMessage;
			if (typeof (input) != 'undefined') {
				var application = input.application;
				if (typeof (application) != 'undefined') {
					for (var i = 0; i < apprequiredProperty.length; i++) {
						var el = application[apprequiredProperty[i]];
						if (typeof (el) === 'undefined') {
							errorMessage = 'Input property : ' + apprequiredProperty[i] + ' not found.';
							errorData.push(errorMessage);
						}
					};
				} else {
					errorMessage = 'application data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'exception': errorData
					};
				}
				if (typeof (input.crifCreditReport) == 'undefined' && typeof (input.cibilReport) == 'undefined') {
					errorMessage = 'bureau data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'exception': errorData
					};
				} else {
					var bureauData = context.call('data_attribute_getbureauData', [input.crifCreditReport, input.cibilReport]);
					if (typeof (bureauData) != 'undefined') {
						for (var i = 0; i < crifRequiredProperty.length; i++) {
							var el = bureauData[crifRequiredProperty[i]];
							if (typeof (el) === 'undefined') {
								errorMessage = 'Input property : ' + crifRequiredProperty[i] + ' not found.';
								errorData.push(errorMessage);
							}
						};
					} else {
						errorMessage = 'credit report data not found';
						errorData.push(errorMessage);
						return {
							'result': false,
							'detail': null,
							'data': null,
							'exception': errorData
						};
					}
					if (errorData.length > 0) {
						return {
							'result': false,
							'detail': null,
							'data': null,
							'exception': errorData
						};
					} else {
						return {
							'result': true,
							'detail': errorData,
							'data': bureauData
						};
					}
				}
			} else {
				errorMessage = 'input data not found.';
				errorData.push(errorMessage);
				return {
					'result': false,
					'detail': null,
					'data': null,
					'exception': errorData
				};
			}
		}
		function getRBLDTIThreshold(segment, income, cibilscore) {
			var dtithresold = 0;
			if (segment == 'thick') {
				if ((income >= 18000 && income < 35000) && cibilscore < 700) {
					dtithresold = 40;
				} else if ((income >= 18000 && income < 35000) && cibilscore >= 700) {
					dtithresold = 40;
				} else if ((income >= 35000 && income < 50000) && cibilscore < 700) {
					dtithresold = 40;
				} else if ((income >= 35000 && income < 50000) && cibilscore >= 700) {
					dtithresold = 50;
				} else if ((income >= 50000 && income < 75000) && cibilscore < 680) {
					dtithresold = 45;
				} else if ((income >= 50000 && income < 75000) && cibilscore >= 680) {
					dtithresold = 55;
				} else if ((income >= 75000) && cibilscore < 680) {
					dtithresold = 55;
				} else if ((income >= 75000) && cibilscore >= 680) {
					dtithresold = 60;
				}
			}
			return dtithresold;
		}
		function getIIFLDTIThreshold(segment, income, cibilscore) {
			var dtithresold = 0;
			if (segment == 'thick') {
				if ((income >= 18000 && income < 35000) && cibilscore < 675) {
					dtithresold = 35;
				} else if ((income >= 18000 && income < 35000) && cibilscore >= 675) {
					dtithresold = 45;
				} else if ((income >= 35000 && income < 50000) && cibilscore < 675) {
					dtithresold = 40;
				} else if ((income >= 35000 && income < 50000) && cibilscore >= 675) {
					dtithresold = 55;
				} else if ((income >= 50000 && income < 75000) && cibilscore < 675) {
					dtithresold = 45;
				} else if ((income >= 50000 && income < 75000) && cibilscore >= 675) {
					dtithresold = 65;
				} else if ((income >= 75000) && cibilscore < 675) {
					dtithresold = 50;
				} else if ((income >= 75000) && cibilscore >= 675) {
					dtithresold = 75;
				}
			}
			return dtithresold;
		}
		function getIBLDTIThreshold(segment, income, cibilscore) {
			var dtithresold = 0;
			if (segment == 'thick') {
				if ((income >= 18000 && income < 35000) && cibilscore < 680) {
					dtithresold = 40;
				} else if ((income >= 18000 && income < 35000) && cibilscore >= 680) {
					dtithresold = 40;
				} else if ((income >= 35000 && income < 50000) && cibilscore < 680) {
					dtithresold = 45;
				} else if ((income >= 35000 && income < 50000) && cibilscore >= 680) {
					dtithresold = 50;
				} else if ((income >= 50000 && income < 65000) && cibilscore < 680) {
					dtithresold = 55;
				} else if ((income >= 50000 && income < 65000) && cibilscore >= 680) {
					dtithresold = 60;
				} else if ((income >= 65000) && cibilscore < 680) {
					dtithresold = 60;
				} else if ((income >= 65000) && cibilscore >= 680) {
					dtithresold = 65;
				}
			}
			return dtithresold;
		}
		function thickfileEligibilityCheck(input) {
			var rejectcodevalue = 'CREDITREJECTION';
			var minIncome = 75000;
			var data = {
				verifybureauScore: verifybureauScore(input),
				verifyloanstatus: verifyloanstatus(input),
				verifyCreditCardBalance: verifyCreditCardBalance(input, 35000),
				verifyisRBLLive: verifyisRBLLive(input),
				verifyisRBLDefaultLoan: verifyisRBLDefaultLoan(input),
				verifyisRBLDPDRule3: verifyisRBLDPDRule3(input),
				verifyHighDPDTrade: verifyHighDPDTrade(input),
				verifyHighDPDTrade1: verifyHighDPDTrade1(input),
				verifyDTIThresold: verifyDTIThresold('thick', input)
			};
			var errorData = [];
			for (var propertyName in data) {
				var el = data[propertyName];
				if (!el.success) {
					if (typeof (input) != 'undefined') {
						errorData.push(el.message);
					}
				}
			};
			var status = true;
			var rejectcode = '';
			if (errorData.length > 0) {
				status = false;
				rejectcode = rejectcodevalue;
			}
			return {
				'result': status,
				'rejectcode': rejectcode,
				'detail': errorData,
				'data': {
					'bureauSourceType': input.bureauSourceType
				}
			};
		}
		function thinfileEligibilityCheck(input) {
			var thinFileMinSelfIncome = 20000;
			var rejectcodevalue = 'CREDITREJECTION';
			var minIncome = 75000;
			var data = {
				verifyselfreportedincome: checkSelfReportedIncome(input, thinFileMinSelfIncome, 'thin', input.additionalMatch),
				verifyloanstatus: verifyloanstatus(input),
				verifyCreditCardBalance: verifyCreditCardBalance(input, minIncome),
				verifydelinquencyFlag: verifydelinquencyFlag(input, minIncome),
				verifyHighDPDTrade: verifyHighDPDTrade(input),
				verifyHighDPDTrade1: verifyHighDPDTrade1(input),
				verifyisRBLLive: verifyisRBLLive(input),
				verifyisRBLDefaultLoan: verifyisRBLDefaultLoan(input),
				verifyisRBLDPDRule1: verifyisRBLDPDRule1(input),
				verifyisRBLOverdue: verifyisRBLOverdue(input),
				verifyisRBLDPDRule3: verifyisRBLDPDRule3(input)
			};
			var errorData = [];
			for (var propertyName in data) {
				var el = data[propertyName];
				if (!el.success) {
					errorData.push(el.message);
				}
			};
			var status = true;
			var rejectcode = '';
			if (errorData.length > 0) {
				status = false;
				rejectcode = rejectcodevalue;
			}
			return {
				'result': status,
				'rejectcode': rejectcode,
				'detail': errorData,
				'data': {
					'bureauSourceType': input.bureauSourceType
				}
			};
		}

		function checkSelfReportedIncome(input, minSelfIncome, fileType, additionalMatch) {
			if (additionalMatch == true) {
				var msg = '';
				if (fileType == 'ntc') {
					msg = 'IncomeLowL3';
				} else if (fileType == 'thin') {
					msg = 'IncomeLowL2';
				}
				if (input.income < minSelfIncome) {
					return {
						success: false,
						message: msg
					};
				}
			}
			return {
				success: true
			};
		}


		function ntcfileEligibilityCheck(input) {
			var NTCFileMinSelfIncome = 20000;
			var rejectcodevalue = 'CREDITREJECTION';
			var data = {
				verifyselfreportedincome: checkSelfReportedIncome(input, NTCFileMinSelfIncome, 'ntc', input.additionalMatch)
			};
			var errorData = [];
			for (var propertyName in data) {
				var el = data[propertyName];
				if (!el.success) {
					errorData.push(el.message);
				}
			};
			var status = true;
			var rejectcode = '';
			if (errorData.length > 0) {
				status = false;
				rejectcode = rejectcodevalue;
			}
			return {
				'result': status,
				'rejectcode': rejectcode,
				'detail': errorData,
				'data': {
					'bureauSourceType': input.bureauSourceType
				}
			};
		}
		function verifybureauScore(input) {
			var bureauScore = input.bureauScore;
			if (bureauScore < 600) {
				return {
					success: false,
					message: 'BureauScoreLow'
				};
			};
			return {
				success: true
			};
		}
		function verifyDTIThresold(segment, input) {
			var rbldtithresold = getRBLDTIThreshold(segment, input.income, input.bureauScore);
			var ibldtithresold = getIBLDTIThreshold(segment, input.income, input.bureauScore);
			var iifldtithresold = getIIFLDTIThreshold(segment, input.income, input.bureauScore);
			var dtithresold = Math.max(rbldtithresold, ibldtithresold, iifldtithresold);
			var imputedDTI = (input.bureauEmi / input.income) * 100;
			if (input.loanpurpose == 'loanrefinancing') {
				if (imputedDTI > (dtithresold + 20)) {
					return {
						success: false,
						message: 'HighDTIThresholds'
					};
				}
			} else {
				if (imputedDTI > (dtithresold + 6.5)) {
					return {
						success: false,
						message: 'HighDTIThresholds'
					};
				}
			}
			return {
				success: true
			};
		}
		function verifytotalInquiriesInLast6Months(input) {
			var totalInquiriesInLast6Months = input.noOfPLInquiries;
			if (totalInquiriesInLast6Months >= 5) {
				return {
					success: false,
					message: 'HighInquiries'
				};
			};
			return {
				success: true
			};
		}
		function verifyloanstatus(input) {
			var loanstatus = input.isDerogatoryStatus;
			if (loanstatus) {
				return {
					success: false,
					message: 'BureauLoanDerog'
				};
			};
			return {
				success: true
			};
		}
		function verifyCreditCardBalance(input, minIncome) {
			var creditCardBalanceFromBureau = input.creditCardBalance;
			var selfReportedIncome = input.income;
			if (creditCardBalanceFromBureau / selfReportedIncome >= 6 && selfReportedIncome < minIncome) {
				return {
					success: false,
					message: 'CCBalToIncomeHigh'
				};
			};
			return {
				success: true
			};
		}
		function verifydelinquencyFlag(input, minIncome) {
			var delinquencyFlag = input.delinquencyFlag;
			if (delinquencyFlag && input.income < minIncome) {
				return {
					success: false,
					message: 'HighDPDTrade1'
				};
			};
			return {
				success: true
			};
		}
		function verifyHighDPDTrade1(input) {
			var HighDPDTrade1 = input.isHighDPDTrade1;
			if (HighDPDTrade1) {
				return {
					success: false,
					message: 'HighDPDTrade1'
				};
			};
			return {
				success: true
			};
		}
		function verifyHighDPDTrade(input) {
			var HighDPDTrade = input.delinquencyFlag;
			if (HighDPDTrade) {
				return {
					success: false,
					message: 'HighDPDTrade'
				};
			};
			return {
				success: true
			};
		}
		function verifyisRBLLive(input) {
			var isRBLLive = input.isRBLLive;
			if (isRBLLive) {
				return {
					success: false,
					message: 'RBLLiveLoan'
				};
			};
			return {
				success: true
			};
		}
		function verifyisRBLDefaultLoan(input) {
			var isRBLDefaultLoan = input.isRBLDefaultLoan;
			if (isRBLDefaultLoan) {
				return {
					success: false,
					message: 'RBLDefaultLoan'
				};
			};
			return {
				success: true
			};
		}
		function verifyisRBLPLInquiry(input) {
			var isRBLPLInquiry = input.isRBLPLInquiry;
			if (isRBLPLInquiry) {
				return {
					success: false,
					message: 'RBLInquiryRule'
				};
			};
			return {
				success: true
			};
		}
		function verifyisRBLDPDRule1(input) {
			var isRBLDPDRule1 = input.isRBLDPDRule1;
			if (isRBLDPDRule1) {
				return {
					success: false,
					message: 'RBLDPDRule1'
				};
			};
			return {
				success: true
			};
		};
		function verifyisRBLOverdue(input) {
			var isRBLOverdue = input.isRBLOverdue;
			if (isRBLOverdue) {
				return {
					success: false,
					message: 'RBLOverdueRule'
				};
			};
			return {
				success: true
			};
		};
		function verifyisRBLDPDRule2(input) {
			var isRBLDPDRule2 = input.isRBLDPDRule2;
			if (isRBLDPDRule2) {
				return {
					success: false,
					message: 'RBLDPDRule2'
				};
			};
			return {
				success: true
			};
		};
		function verifyisRBLDPDRule3(input) {
			var isRBLDPDRule3 = input.isRBLDPDRule3;
			if (isRBLDPDRule3) {
				return {
					success: false,
					message: 'RBLDPDRule3'
				};
			};
			return {
				success: true
			};
		}
		var objValidateInput = validateRequiredInput(this, payload);
		if (objValidateInput.result) {
			var bureauData = objValidateInput.data;
			var input = {
				'bureauScore': bureauData.bureauScore,
				'noOfPLInquiries': bureauData.noOfPLInquiries,
				'isDerogatoryStatus': bureauData.isDerogatoryStatus,
				'creditCardBalance': bureauData.creditCardBalance,
				'delinquencyFlag': bureauData.isHighDPDTrade,
				'isHighDPDTrade1': bureauData.isHighDPDTrade1,
				'income': payload.application.income,
				'selfEmi': payload.application.selfEmi,
				'bureauEmi': bureauData.bureauEmi,
				'isRBLLive': bureauData.isRBLLive,
				'isRBLDefaultLoan': bureauData.isRBLDefaultLoan,
				'isRBLPLInquiry': bureauData.isRBLPLInquiry,
				'isRBLDPDRule1': bureauData.isRBLDPDRule1,
				'isRBLOverdue': bureauData.isRBLOverdue,
				'isRBLDPDRule2': bureauData.isRBLDPDRule2,
				'isRBLDPDRule3': bureauData.isRBLDPDRule3,
				'bureauSourceType': bureauData.sourceType,
				'additionalMatch': bureauData.IsSecondaryReportExist,
				'loanpurpose': payload.application.loanpurpose,
			};
			var fileType = bureauData.fileType;
			if (fileType != null) {
				if (fileType.toLowerCase() == 'thick') {
					return thickfileEligibilityCheck(input);
				} else if (fileType.toLowerCase() == 'thin') {
					return thinfileEligibilityCheck(input);
				} else if (fileType.toLowerCase() == 'ntc') {
					return ntcfileEligibilityCheck(input);
				}
			}
			return {
				'result': true,
				'rejectcode': '',
				'detail': errorData,
				'data': {
					'bureauSourceType': input.bureauSourceType
				}
			};
		} else {
			return objValidateInput;
		}
	} catch (e) {
		var msg = 'Error occured :' + e.message;
		errorData.push(msg);
		return {
			'result': false,
			'rejectcode': rejectcodevalue,
			'detail': null,
			'data': null,
			'exception': errorData
		};
	}
}
function initialoffer_execute_eligibility_rules(payload) {
	var errorData = [];
	var rejectcodevalue = 'PRESCREEN';
	try {
		function validateRequiredInput(input) {
			var requiredProperty = ['age', 'city', 'purposeOfLoan', 'cinNumber', 'income', 'requestedAmount', 'employeeType', 'pan', 'firstName', 'lastName', 'selfEmi', 'creditCardBalance'];
			var errorData = [];
			var errorMessage;
			if (typeof(input) != 'undefined') {
				var applicationData = input.application;
				if (typeof(applicationData) != 'undefined') {
					for (var i = 0; i < requiredProperty.length; i++) {
						var el = applicationData[requiredProperty[i]];
						if (typeof(el) === 'undefined') {
							errorMessage = requiredProperty[i] + ' not found.';
							errorData.push(errorMessage);
						}
					}
					if (errorData.length > 0) {
						return {
							'result': false,
							'detail': null,
							'data': null,
							'exception': errorData
						};
					} else {
						return {
							'result': true,
							'detail': errorData,
							'data': null
						};
					}
				} else {
					errorMessage = 'application data not found';
					errorData.push(errorMessage);
					return {
						'result': false,
						'detail': null,
						'data': null,
						'exception': errorData
					};
				}
			} else {
				errorMessage = 'input data not found.';
				errorData.push(errorMessage);
				return {
					'result': false,
					'detail': null,
					'data': null,
					'exception': errorData
				};
			}
		};
		function verifyAge(input) {
			var age = input.age;
			if (age < 23 || age > 57) {
				return {
					success: false,
					message: 'AgeMismatch'
				};
			};
			return {
				success: true
			};
		};
		function verifyCity(input) {
			var pinCode = input.zipCode;
			var pinCodeSupported = ['11', '56', '60', '122', '201', '121', '40', '42', '41', '50','38','302','303'];
			var unsupportedPincode = ['414001'];
			if (pinCode != null) {
				for (var i = 0; i < pinCodeSupported.length; i++) {
					if (pinCode.startsWith(pinCodeSupported[i]) && unsupportedPincode.indexOf(pinCode) == -1 && !pinCode.startsWith('403')) {
						return {
							success: true
						};
					}
				}
			}
			return {
				success: false,
				message: 'CitySupported1'
			};
		};
		function verifyPurposeofLoan(input) {
			var purposeofloan = input.purposeOfLoan;
			if (purposeofloan.toLowerCase() == 'business' || purposeofloan.toLowerCase() == 'agriculture') {
				return {
					success: false,
					message: 'LoanPurposeCheck'
				};
			};
			return {
				success: true
			};
		};
		function verifycinNumber(input) {
			var cinnumber = input.cinNumber;
			if (cinnumber != null) {
				return {
					success: true
				};
			};
			return {
				success: false,
				message: 'EmpNotExist'
			};
		};
		function verifyIncome(input) {
			var income = input.income;
			if (income <= 0) {
				throw 'Income should not be zero';
			}
			if (income >= 18000 && income <= 1000000) {
				return {
					success: true
				};
			};
			return {
				success: false,
				message: 'IncomeOutOfRange'
			};
		};
		function verifyLoanAmount(input) {
			var loanamount = input.requestedAmount;
			if (loanamount >= 25000 && loanamount <= 1000000) {
				return {
					success: true
				};
			};
			return {
				success: false,
				message: 'LoanAmtMismatch'
			};
		};
		function verifyEmployeeType(input) {
			var employeetype = input.employeeType;
			if (employeetype != null) {
				if (employeetype.toLowerCase() === 'salaried') {
					return {
						success: true
					};
				};
			}
			return {
				success: false,
				message: 'EmpTypeNotSupported'
			};
		};
		function verifyPan4thLetter(input) {
			var pan = input.pan;
			if (pan != null) {
				if (pan[3].toLowerCase() == 'p') {
					return {
						success: true
					};
				};
			}
			return {
				success: false,
				message: 'InvalidPANNumber'
			};
		};
		function verifySelfReportedIncome(input) {
			var selfEmi = input.selfEmi;
			var selfIncome = input.income;
			if (selfEmi == null) {
				selfEmi = 0;
			}
			if (selfIncome == null) {
				selfIncome = 0;
			}
			if (selfIncome > 0) {
				var emitoincome = (selfEmi / selfIncome) * 100;
				if (emitoincome > 70 && selfIncome < 75000) {
					return {
						success: false,
						message: 'HighDTILowIncomeL1'
					};
				}
				return {
					success: true
				};
			}
			return {
				success: false,
				message: 'HighDTILowIncomeL1'
			};
		};
		function verifySelfCreditCardBalance(input) {
			var creditcardbalance = input.creditCardBalance;
			var selfIncome = input.income;
			if (creditcardbalance == null) {
				creditcardbalance = 0;
			}
			if (selfIncome == null) {
				selfIncome = 0;
			}
			if (selfIncome > 0) {
				var emitoincome = (creditcardbalance / selfIncome);
				if (emitoincome >= 6 && selfIncome < 35000) {
					return {
						success: false,
						message: 'CCBalanceRatioHigh'
					};
				}
				return {
					success: true
				};
			}
			return {
				success: false,
				message: 'CCBalanceRatioHigh'
			};
		};
		var objValidateInput = validateRequiredInput(payload);
		if (objValidateInput.result) {
			var input = payload.application;
			var data = {
				verifyAge: verifyAge(input),
				verifyCity: verifyCity(input),
				verifycinNumber: verifycinNumber(input),
				verifyIncome: verifyIncome(input),
				verifyLoanAmount: verifyLoanAmount(input),
				verifyEmployeeType: verifyEmployeeType(input),
				verifyPan4thLetter: verifyPan4thLetter(input)
			};
			for (var propertyName in data) {
				var el = data[propertyName];
				if (!el.success) {
					errorData.push(el.message);
				}
			};
			var status = true;
			var rejectcode = '';
			if (errorData.length > 0) {
				status = false;
				rejectcode = rejectcodevalue;
			}
			var result = {
				'result': status,
				'rejectcode': rejectcode,
				'detail': errorData,
				'data': null
			};
			return result;
		} else {
			return objValidateInput;
		}
	} catch (e) {
		var msg = 'Error occured :' + e;
		errorData.push(msg);
		return {
			'result': false,
			'rejectcode': rejectcodevalue,
			'detail': null,
			'data': null,
			'exception': errorData
		};
	}
};
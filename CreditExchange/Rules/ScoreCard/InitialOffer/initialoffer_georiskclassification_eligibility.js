function initialoffer_georiskclassification_eligibility(payload) {
    var errorData = [];
    try {
        function validateRequiredInput(context, input) {
            var apprequiredProperty = ['age', 'income'];
            var crifRequiredProperty = ['bureauScore', 'noOfPLInquiries', 'avg_PL_Limit', 'bureauTenure', 'fileType'];
            var geoRiskRequiredProperty = ['zone'];
            var errorData = [];
            var errorMessage;
            if (typeof (input) != 'undefined') {
                var application = input.application;
                if (typeof (application) != 'undefined') {
                    for (var i = 0; i < apprequiredProperty.length; i++) {
                        var el = application[apprequiredProperty[i]];
                        if (typeof (el) === 'undefined') {
                            errorMessage = 'Input property : ' + apprequiredProperty[i] + ' not found.';
                            errorData.push(errorMessage);
                        }
                    }
                    ;
                }
                else {
                    errorMessage = 'application data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData
                    };
                }
                if (typeof (input.crifCreditReport) == 'undefined' && typeof (input.cibilReport) == 'undefined') {
                    errorMessage = 'bureau data not found';
                    errorData.push(errorMessage);
                    return {
                        'result': false,
                        'detail': null,
                        'data': null,
                        'exception': errorData
                    };
                }
                else {
                    var bureauData = context.call('data_attribute_getbureauData', [input.crifCreditReport, input.cibilReport]);
                    if (typeof (bureauData) != 'undefined') {
                        for (var i = 0; i < crifRequiredProperty.length; i++) {
                            var el = bureauData[crifRequiredProperty[i]];
                            if (typeof (el) === 'undefined') {
                                errorMessage = 'Input property : ' + crifRequiredProperty[i] + ' not found.';
                                errorData.push(errorMessage);
                            }
                        }
                        ;
                    }
                    else {
                        errorMessage = 'crif credit report data not found';
                        errorData.push(errorMessage);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'exception': errorData
                        };
                    }
                    var geoRiskPinCode = input.geoRiskPinCode;
                    if (typeof (geoRiskPinCode) != 'undefined') {
                        for (var i = 0; i < geoRiskRequiredProperty.length; i++) {
                            var el = geoRiskPinCode[geoRiskRequiredProperty[i]];
                            if (typeof (el) === 'undefined') {
                                errorMessage = 'Input property : ' + geoRiskRequiredProperty[i] + ' not found.';
                                errorData.push(errorMessage);
                            }
                        }
                        ;
                    }
                    else {
                        errorMessage = 'GeoRiskPinCode data not found';
                        errorData.push(errorMessage);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'exception': errorData
                        };
                    }
                    if (errorData.length > 0) {
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'exception': errorData
                        };
                    }
                    else {
                        return {
                            'result': true,
                            'detail': errorData,
                            'data': bureauData
                        };
                    }
                }
            }
            else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'exception': errorData
                };
            }
        }



        function checkFileZone(input) {
            var errorData = [];
            switch (input.zipcodeZone) {
                case 'Zone-1':
                    {
                        return CheckZone1Risk(input);
                    }
                case 'Zone-2':
                    {
                        var objZone2 = checkZoneRisk(input.bureauScore, input.income, input.age, input.noOfPLInquiries, input.avg_PL_Limit, input.fileType);
                        return checkEligibility_RiskZone2(objZone2, input.bureauSourceType, input.geoRiskRanking);
                    }
                case 'Zone-3':
                    {
                        var objZone3 = checkZoneRisk(input.bureauScore, input.income, input.age, input.noOfPLInquiries, input.avg_PL_Limit, input.fileType);
                        return checkEligibility_RiskZone3(objZone3, input.bureauTenure, input.bureauSourceType, input.geoRiskRanking);
                    }
                case 'Zone-4':
                    {
                        return CheckZone4Risk(input.bureauSourceType, input.geoRiskRanking);
                    }
                case 'Not Found':
				case 'Others':
                    {
                        var objdata = {};
                        objdata['ZoneClassification'] = 'Low Risk';
                        objdata['geoRiskRanking'] = input.geoRiskRanking;
                        objdata['bureauSourceType'] = input.bureauSourceType;
                        return {
                            'result': true,
                            'detail': null,
                            'data': objdata
                        };
                    }
                default:
                    {
                        var message = 'No configured zone found';
                        errorData.push(message);
                        return {
                            'result': false,
                            'detail': null,
                            'data': null,
                            'exception': errorData
                        };
                    }
            }
        }
        function checkEligibility_RiskZone2(objData, bureauSourceType, geoRiskRanking) {
            if (objData.result) {
                var objdata = {};
                objdata['ZoneClassification'] = 'High Risk';
                objdata['geoRiskRanking'] = geoRiskRanking;
                objdata['bureauSourceType'] = bureauSourceType;
                return {
                    'result': true,
                    'detail': errorData,
                    'data': objdata
                };
            }
            else {
                var objdata = {};
                objdata['bureauSourceType'] = bureauSourceType;
                objData['data'] = objdata;
                return objData;
            }
        }
        function checkEligibility_RiskZone3(objData, bureauTenure, bureauSourceType, geoRiskRanking) {
            if (objData.result) {
                if (bureauTenure <= 24) {
                    var objdata = {};
                    objdata['ZoneClassification'] = 'High Risk';
                    objdata['geoRiskRanking'] = geoRiskRanking;
                    objdata['bureauSourceType'] = bureauSourceType;
                    return {
                        'result': true,
                        'detail': null,
                        'data': objdata
                    };
                }
                else {
                    var objdata = {};
                    objdata['ZoneClassification'] = 'Medium Risk';
                    objdata['geoRiskRanking'] = geoRiskRanking;
                    objdata['bureauSourceType'] = bureauSourceType;
                    return {
                        'result': true,
                        'detail': null,
                        'data': objdata
                    };
                }
            }
            else {
                var objdata = {};
                objdata['bureauSourceType'] = input.bureauSourceType;
                objData['data'] = objdata;
                return objData;
            }
        }
        function checkZoneRisk(bureauScore, income, age, inquiries, avg_PL_Limit, fileType) {
            var errorData = [];
            var annualincome = income * 12;
           if (bureauScore < 700 && age > 45 && income < 75000) {
                var message = 'HighRiskZipRule4';
                errorData.push(message);
                return {
                    'result': false,
                    'detail': errorData,
                    'data': null
                };
            }  else {
                return {
                    'result': true,
                    'detail': errorData,
                    'data': null
                };
            }
        }

        function CheckZone1Risk(input) {
            if (input.income < 75000) {
             var objdata = {};
                objdata['ZoneClassification'] = 'High Risk';
                objdata['geoRiskRanking'] = input.geoRiskRanking;
                objdata['bureauSourceType'] = input.bureauSourceType;
                return {
                    'result': true,
                    'detail': errorData,
                    'data': objdata
                };
            } else {
                var objdata = {};
                objdata['ZoneClassification'] = 'Low Risk';
                objdata['geoRiskRanking'] = input.geoRiskRanking;
                objdata['bureauSourceType'] = input.bureauSourceType;
                return {
                    'result': true,
                    'detail': null,
                    'data': objdata
                };
            }

        }
        function CheckZone4Risk(bureauSourceType, geoRiskRanking) {
            var objdata = {};
            objdata['ZoneClassification'] = 'Medium Risk';
            objdata['geoRiskRanking'] = geoRiskRanking;
            objdata['bureauSourceType'] = bureauSourceType;
            return {
                'result': true,
                'detail': null,
                'data': objdata
            };
        }

        var objValidateInput = validateRequiredInput(this, payload);
        if (objValidateInput.result) {
            if (typeof (objValidateInput.data) == 'undefined') {
                var errorMessage = 'objValidateInput.data. not exist';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': null,
                    'data': null,
                    'exception': errorData
                };
            }
            var bureauData = objValidateInput.data;
            var application = payload.application;
            var zipZone = payload.geoRiskPinCode;
            var input = {
                'zipcodeZone': zipZone.zone,
                'geoRiskRanking' : zipZone.geoRiskRanking,
                'age': application.age,
                'income': application.income,
                'bureauScore': bureauData.bureauScore,
                'noOfPLInquiries': bureauData.noOfPLInquiries,
                'avg_PL_Limit': bureauData.avg_PL_Limit,
                'bureauTenure': bureauData.bureauTenure,
                'fileType':bureauData.fileType,
                'bureauSourceType': bureauData.sourceType
            };
            return checkFileZone(input);
        }
        else {
            return objValidateInput;
        }
    }
    catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'detail': null,
            'data': null,
            'exception': errorData
        };
    }
}


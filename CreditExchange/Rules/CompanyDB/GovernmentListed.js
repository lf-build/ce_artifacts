function GovernmentListed(payload) {
    try {
        if (payload == null || payload.DirectorAffiliations == null || payload.DirectorAffiliations.length == 0) {
            return {
                'status': false,
                'result': 'input parameter not found for GovernmentListed'
            }
        }
        else {
            var directorAffiliation = [];
            for (var k = 0; k < payload.DirectorAffiliations.length; k++) {

                var DirectorAffiliationscore = 4.6875 *
                    ((parseFloat(payload.DirectorAffiliations[k]['Age']) < 2.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 1.0) ? 0.5 :
                        (parseFloat(payload.DirectorAffiliations[k]['Age']) < 5.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 2.0) ? 1.0 :
                            (parseFloat(payload.DirectorAffiliations[k]['Age']) < 8.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 5.0) ? 4.0 :
                                (parseFloat(payload.DirectorAffiliations[k]['Age']) < 11.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 8.0) ? 2.0 :
                                    (parseFloat(payload.DirectorAffiliations[k]['Age']) < 16.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 11.0) ? 2.5 :
                                        (parseFloat(payload.DirectorAffiliations[k]['Age']) < 25.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 16.0) ? 3.0 :
                                            (parseFloat(payload.DirectorAffiliations[k]['Age']) < 35.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 25.0) ? 3.5 :
                                                parseFloat(payload.DirectorAffiliations[k]['Age']) >= 35.0 ? 4.0 : 0.0)
                    +
                    7.8125 *
                    ((parseFloat(payload.DirectorAffiliations[k]['Capital']) < 100000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 0.0) ? 0.0 :
                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 500000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 100000.0) ? 0.5 :
                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 1000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 500000.0) ? 1.0 :
                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 2500000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 1000000.0) ? 1.25 :
                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 5000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 2500000.0) ? 1.5 :
                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 10000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 5000000.0) ? 1.75 :
                                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 20000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 10000000.0) ? 0.5 :
                                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 50000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 20000000.0) ? 2.25 :
                                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 100000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 50000000.0) ? 2.5 :
                                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 250000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 100000000.0) ? 2.75 :
                                                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 1000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 250000000.0) ? 3.0 :
                                                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 5000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 1000000000.0) ? 3.25 :
                                                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 10000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 5000000000.0) ? 3.75 :
                                                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 10000000000.0) ? 4.0 : 0.0);


                directorAffiliation.push(DirectorAffiliationscore);
            };
            var sum = 0;
            for (var i = 0; i < directorAffiliation.length; i++) {
                sum += parseFloat(directorAffiliation[i]);
            }

            var dirAffValue = sum / directorAffiliation.length;

            var dirAffValueScore = 5.0 *
                (
                    dirAffValue >= 0.0 && dirAffValue < 5.0 ? 0.0 :
                        dirAffValue >= 5.0 && dirAffValue < 12.5 ? 1.0 :
                            dirAffValue >= 12.50 && dirAffValue < 25.0 ? 2.0 :
                                dirAffValue >= 25.0 && dirAffValue < 37.5 ? 3.0 :
                                    dirAffValue >= 37.5 && dirAffValue <= 50.0 ? 4.0 : 0.0
                );
            var AgeOfCompanyScore = 1.875 *
                ((parseFloat(payload.AgeOfCompany) < 20.0 && parseFloat(payload.AgeOfCompany) >= 1.0) ? 1.0 :
                    (parseFloat(payload.AgeOfCompany) < 30.0 && parseFloat(payload.AgeOfCompany) >= 21.0) ? 3.0 :
                        (parseFloat(payload.AgeOfCompany) >= 30.0) ? 4.0 : 0.0);

            var PaidUpCapitalScore = 3.125 *
                (
                    (parseFloat(payload.PaidUpCapital) < 50000000.0 && parseFloat(payload.PaidUpCapital) >= 0.0) ? 1.0 :
                        (parseFloat(payload.PaidUpCapital) < 100000000.0 && parseFloat(payload.PaidUpCapital) >= 50000000.0) ? 2.0 :
                            (parseFloat(payload.PaidUpCapital) < 200000000.0 && parseFloat(payload.PaidUpCapital) >= 100000000.0) ? 3.0 :
                                (parseFloat(payload.PaidUpCapital) >= 200000000.0) ? 4.0 : 0.0
                );
            var RevenueScore = 6.875 *
                (
                    (parseFloat(payload.Revenue) < 1000000.0 ? 0.4 :
                        (parseFloat(payload.Revenue) >= 1000000.0 && parseFloat(payload.Revenue) < 20000000.0) ? 1.0 :
                            (parseFloat(payload.Revenue) >= 2000000.0 && parseFloat(payload.Revenue) < 500000000.0) ? 2.0 :
                                (parseFloat(payload.Revenue) >= 500000000.0 && parseFloat(payload.Revenue) < 3000000000.0) ? 3.0 :
                                    (parseFloat(payload.Revenue) >= 3000000000.0) ? 4.0 : 0.0)
                );
            var ProfitScore = 6.875 *
                (
                    (parseFloat(payload.Profit) <= 0.0 ? 0.0 :
                        (parseFloat(payload.Profit) > 0.0 && parseFloat(payload.Profit) <= 5000000.0) ? 1.0 :
                            (parseFloat(payload.Profit) > 5000000.0 && parseFloat(payload.Profit) <= 100000000.0) ? 2.0 :
                                (parseFloat(payload.Profit) > 100000000.0) ? 4.0 : 0.0)
                );

            var StateScore = 1.25 *
                (
                    payload.IsState == true ? 3.0 : 4.0
                );

            var finalScore = dirAffValueScore
                +
                AgeOfCompanyScore
                +
                PaidUpCapitalScore
                +
                RevenueScore
                +
                ProfitScore
                +
                StateScore;


            var companyCategory = finalScore >= 70.0 ? 'A+' :
                finalScore >= 60.0 ? 'A' :
                    finalScore >= 50.0 ? 'B' :
                        finalScore >= 30.0 ? 'C' : 'D';

            return {
                'status': true,
                'result': companyCategory,
                'data': {
                    'dirAffValueScore': dirAffValueScore,
                    'AgeOfCompanyScore': AgeOfCompanyScore,
                    'PaidUpCapitalScore': PaidUpCapitalScore,
                    'RevenueScore': RevenueScore,
                    'ProfitScore': ProfitScore,
                    'StateScore': StateScore,
                    'finalScore': finalScore
                }
            }

        }
    } catch (e) {

        return {
            'status': false,
            'result': 'Unable to verify',
            'exception': e.message
        }
    }
}
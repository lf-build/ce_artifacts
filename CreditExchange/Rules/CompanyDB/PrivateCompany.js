function PrivateCompany(payload) {
    try {
        if (payload == null || payload.DirectorAffiliations == null || payload.DirectorAffiliations.length == 0) {
            return {
                'status': false,
                'result': 'input parameter not found for PrivateCompany'
            }
        }
        else {
            var directorAffiliation = [];
            for (var k = 0; k < payload.DirectorAffiliations.length; k++) {

                var DirectorAffiliationscore = 3.675 *
                    ((parseFloat(payload.DirectorAffiliations[k]['Age']) < 2.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 1.0) ? 0.5 :
                        (parseFloat(payload.DirectorAffiliations[k]['Age']) < 5.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 2.0) ? 1.0 :
                            (parseFloat(payload.DirectorAffiliations[k]['Age']) < 8.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 5.0) ? 4.0 :
                                (parseFloat(payload.DirectorAffiliations[k]['Age']) < 11.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 8.0) ? 2.0 :
                                    (parseFloat(payload.DirectorAffiliations[k]['Age']) < 16.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 11.0) ? 2.5 :
                                        (parseFloat(payload.DirectorAffiliations[k]['Age']) < 25.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 16.0) ? 3.0 :
                                            (parseFloat(payload.DirectorAffiliations[k]['Age']) < 35.0 && parseFloat(payload.DirectorAffiliations[k]['Age']) >= 25.0) ? 3.5 :
                                                parseFloat(payload.DirectorAffiliations[k]['Age']) >= 35.0 ? 4.0 : 0.0)
                    +
                    8.825 *
                    ((parseFloat(payload.DirectorAffiliations[k]['Capital']) < 100000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 0.0) ? 0.0 :
                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 500000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 100000.0) ? 0.5 :
                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 1000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 500000.0) ? 1.0 :
                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 2500000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 1000000.0) ? 1.25 :
                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 5000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 2500000.0) ? 1.5 :
                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 10000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 5000000.0) ? 1.75 :
                                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 20000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 10000000.0) ? 0.5 :
                                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 50000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 20000000.0) ? 2.25 :
                                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 100000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 50000000.0) ? 2.5 :
                                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 250000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 100000000.0) ? 2.75 :
                                                            (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 1000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 250000000.0) ? 3.0 :
                                                                (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 5000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 1000000000.0) ? 3.25 :
                                                                    (parseFloat(payload.DirectorAffiliations[k]['Capital']) < 10000000000.0 && parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 5000000000.0) ? 3.75 :
                                                                        (parseFloat(payload.DirectorAffiliations[k]['Capital']) >= 10000000000.0) ? 4.0 : 0.0);


                directorAffiliation.push(DirectorAffiliationscore);
            };
            var sum = 0;
            for (var i = 0; i < directorAffiliation.length; i++) {
                sum += parseFloat(directorAffiliation[i]);
            }

            var dirAffValue = sum / directorAffiliation.length;



            var dirAffValueScore = 3.75 *
                (
                    dirAffValue >= 0.0 && dirAffValue < 5.0 ? 0.0 :
                        dirAffValue >= 5.0 && dirAffValue < 12.5 ? 1.0 :
                            dirAffValue >= 12.50 && dirAffValue < 25.0 ? 2.0 :
                                dirAffValue >= 25.0 && dirAffValue < 37.5 ? 3.0 :
                                    dirAffValue >= 37.5 && dirAffValue <= 50.0 ? 4.0 : 0.0
                );

            var AgeOfCompanyScore = 6.25 *
                ((parseFloat(payload.AgeOfCompany) < 2.0 && parseFloat(payload.AgeOfCompany) >= 1.0) ? 0.5 :
                    (parseFloat(payload.AgeOfCompany) < 5.0 && parseFloat(payload.AgeOfCompany) >= 2.0) ? 1.0 :
                        (parseFloat(payload.AgeOfCompany) < 8.0 && parseFloat(payload.AgeOfCompany) >= 5.0) ? 1.5 :
                            (parseFloat(payload.AgeOfCompany) < 11.0 && parseFloat(payload.AgeOfCompany) >= 8.0) ? 2.0 :
                                (parseFloat(payload.AgeOfCompany) < 16.0 && parseFloat(payload.AgeOfCompany) >= 11.0) ? 2.5 :
                                    (parseFloat(payload.AgeOfCompany) < 25.0 && parseFloat(payload.AgeOfCompany) >= 16.0) ? 3.0 :
                                        (parseFloat(payload.AgeOfCompany) < 35.0 && parseFloat(payload.AgeOfCompany) >= 25.0) ? 3.5 :
                                            parseFloat(payload.AgeOfCompany) >= 35.0 ? 4.0 : 0.0);

            var PaidUpCapitalScore = 9.0 *
                (
                    (parseFloat(payload.PaidUpCapital) < 100000.0 && parseFloat(payload.PaidUpCapital) >= 0.0) ? 0.0 :
                        (parseFloat(payload.PaidUpCapital) < 500000.0 && parseFloat(payload.PaidUpCapital) >= 100000.0) ? 0.5 :
                            (parseFloat(payload.PaidUpCapital) < 1000000.0 && parseFloat(payload.PaidUpCapital) >= 500000.0) ? 1.0 :
                                (parseFloat(payload.PaidUpCapital) < 2500000.0 && parseFloat(payload.PaidUpCapital) >= 1000000.0) ? 1.25 :
                                    (parseFloat(payload.PaidUpCapital) < 5000000.0 && parseFloat(payload.PaidUpCapital) >= 2500000.0) ? 1.5 :
                                        (parseFloat(payload.PaidUpCapital) < 10000000.0 && parseFloat(payload.PaidUpCapital) >= 5000000.0) ? 1.75 :
                                            (parseFloat(payload.PaidUpCapital) < 20000000.0 && parseFloat(payload.PaidUpCapital) >= 10000000.0) ? 0.5 :
                                                (parseFloat(payload.PaidUpCapital) < 50000000.0 && parseFloat(payload.PaidUpCapital) >= 20000000.0) ? 2.25 :
                                                    (parseFloat(payload.PaidUpCapital) < 100000000.0 && parseFloat(payload.PaidUpCapital) >= 50000000.0) ? 2.5 :
                                                        (parseFloat(payload.PaidUpCapital) < 250000000.0 && parseFloat(payload.PaidUpCapital) >= 100000000.0) ? 2.75 :
                                                            (parseFloat(payload.PaidUpCapital) < 1000000000.0 && parseFloat(payload.PaidUpCapital) >= 250000000.0) ? 3.0 :
                                                                (parseFloat(payload.PaidUpCapital) < 5000000000.0 && parseFloat(payload.PaidUpCapital) >= 1000000000.0) ? 3.25 :
                                                                    (parseFloat(payload.PaidUpCapital) < 10000000000.0 && parseFloat(payload.PaidUpCapital) >= 5000000000.0) ? 3.75 :
                                                                        (parseFloat(payload.PaidUpCapital) >= 10000000000.0) ? 4.0 : 0.0

                );

            var AuthCapitalScore = 6.0 *
                (
                    (parseFloat(payload.AuthCapital) < 100000.0 && parseFloat(payload.AuthCapital) >= 0.0) ? 0.0 :
                        (parseFloat(payload.AuthCapital) < 500000.0 && parseFloat(payload.AuthCapital) >= 100000.0) ? 0.0 :
                            (parseFloat(payload.AuthCapital) < 1000000.0 && parseFloat(payload.AuthCapital) >= 500000.0) ? 0.0 :
                                (parseFloat(payload.AuthCapital) < 2500000.0 && parseFloat(payload.AuthCapital) >= 1000000.0) ? 0.5 :
                                    (parseFloat(payload.AuthCapital) < 5000000.0 && parseFloat(payload.AuthCapital) >= 2500000.0) ? 0.75 :
                                        (parseFloat(payload.AuthCapital) < 10000000.0 && parseFloat(payload.AuthCapital) >= 5000000.0) ? 1.0 :
                                            (parseFloat(payload.AuthCapital) < 20000000.0 && parseFloat(payload.AuthCapital) >= 10000000.0) ? 1.25 :
                                                (parseFloat(payload.AuthCapital) < 50000000.0 && parseFloat(payload.AuthCapital) >= 20000000.0) ? 1.5 :
                                                    (parseFloat(payload.AuthCapital) < 100000000.0 && parseFloat(payload.AuthCapital) >= 50000000.0) ? 1.75 :
                                                        (parseFloat(payload.AuthCapital) < 250000000.0 && parseFloat(payload.AuthCapital) >= 100000000.0) ? 2.0 :
                                                            (parseFloat(payload.AuthCapital) < 1000000000.0 && parseFloat(payload.AuthCapital) >= 250000000.0) ? 2.5 :
                                                                (parseFloat(payload.AuthCapital) < 5000000000.0 && parseFloat(payload.AuthCapital) >= 1000000000.0) ? 3.00 :
                                                                    (parseFloat(payload.AuthCapital) < 10000000000.0 && parseFloat(payload.AuthCapital) >= 5000000000.0) ? 3.50 :
                                                                        (parseFloat(payload.AuthCapital) >= 10000000000.0) ? 4.0 : 0.0);
               
            var finalScore = dirAffValueScore
                +
                AgeOfCompanyScore
                +
                PaidUpCapitalScore
                +
                AuthCapitalScore;
                    


            var companyCategory = ((parseFloat(payload.PaidUpCapital) >= 1000000000.0 ||
                (payload.AuthCapital != null || parseFloat(payload.AuthCapital) >= 1000000000.0))
                && finalScore >= 70.0) ? 'A+' :
                ((parseFloat(payload.PaidUpCapital) >= 500000000.0 ||
                    (payload.AuthCapital != null || parseFloat(payload.AuthCapital) >= 500000000.0))
                    && finalScore >= 60.0) ? 'A' :
                    ((parseFloat(payload.PaidUpCapital) >= 50000000.0 ||
                        (payload.AuthCapital != null || parseFloat(payload.AuthCapital) >= 50000000.0))
                        && finalScore >= 50.0) ? 'B' :
                        finalScore >= 30.0 ? 'C' : 'D';

            return {
                'status': true,
                'result': companyCategory,
                'data': {
                    'dirAffValueScore': dirAffValueScore,
                    'AgeOfCompanyScore': AgeOfCompanyScore,
                    'PaidUpCapitalScore': PaidUpCapitalScore,
                    'AuthCapitalScore': AuthCapitalScore,
                    'finalScore': finalScore
                }
            }

        }
    } catch (e) {

        return {
            'status': false,
            'result': 'Unable to verify',
            'exception': e.message
        }
    }
}
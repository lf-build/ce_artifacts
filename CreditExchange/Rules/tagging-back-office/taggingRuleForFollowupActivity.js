function taggingRuleForFollowupActivity(payload) {
	var applyTags = [];
	var removeTags = [];
	if (typeof(payload) != 'undefined') {
		if (typeof(payload.Data) != 'undefined') {
			var eventData = payload.Data;
			if (typeof(eventData.Title) != 'undefined') {
				if (eventData.Title == 'Need followup') {
					applyTags.push('RBL Followup');
				}else if(eventData.Title == 'Resend to Bank'){
					removeTags.push('RBL Followup');
				}
			}
		}
	}
	return {
		ApplyTags: applyTags,
		RemoveTags: removeTags
	};
}

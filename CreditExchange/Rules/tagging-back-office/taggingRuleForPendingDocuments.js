function taggingRuleForPendingDocuments(payload) {
	var applyTags = [];
	var removeTags = [];
	if (typeof (payload) != 'undefined') {
		if (typeof (payload.Data) != 'undefined') {
			var eventData = payload.Data;
			if (typeof (eventData.PendingDocumentcount) != 'undefined') {
				if (eventData.PendingDocumentcount == '0') {
					removeTags.push('Pending Documents');
				}
			}
		}
	}
	return {
		ApplyTags: applyTags,
		RemoveTags: removeTags
	};
}

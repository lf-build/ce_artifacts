function taggingRuleForNotCategorized(payload) {
	var applyTags = [];
	var removeTags = [];
	if (typeof(payload) != 'undefined') {
		if (typeof(payload.Data) != 'undefined') {
			var eventData = payload.Data;
			if (typeof(eventData.Response.Result) != 'undefined') {
				if (eventData.Response.Result.toUpperCase() == 'UNCLASSIFIED') {
					applyTags.push('Company Unclassified');
				}
			}
		}
	}
	return {
		ApplyTags: applyTags,
		RemoveTags: removeTags
	};
}
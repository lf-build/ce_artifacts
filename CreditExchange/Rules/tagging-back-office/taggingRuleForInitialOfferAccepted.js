function taggingRuleForInitialOfferAccepted(payload) {
    var applyTags = [];
    var removeTags = [];
    if (typeof (payload) != 'undefined') {
        applyTags.push('Bank Pending');
        applyTags.push('Employment Pending');
		if(typeof(payload.DataAttributes) != 'undefined' && typeof(payload.DataAttributes.cibilReport) != 'undefined')
		{
			var fileType = payload.DataAttributes.cibilReport.fileType;
			if (typeof (fileType) != 'undefined' && fileType != null) {
			fileType = fileType.toUpperCase();
			if(fileType == 'THIN' || fileType == 'NTC'){
				applyTags.push('Social Pending');
				}            
			}
		}
    }
	
    return { ApplyTags: applyTags, RemoveTags: removeTags };
}
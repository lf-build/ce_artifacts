function taggingRuleForExportsOnStatus(payload) {
	var applyTags = [];
	var removeTags = [];
	if (typeof (payload) != 'undefined') {
		if (typeof (payload.ApplicationStatus) != 'undefined') {
			if (payload.ApplicationStatus.Code == '200.08') {
				applyTags.push('Ready for HunterExport');
			}

			if (payload.ApplicationStatus.Code == '200.11') {
				applyTags.push('Ready for FinacleExport');
				applyTags.push('Ready for BankQA');
			}

			if (payload.ApplicationStatus.Code == '200.16') {
				removeTags.push('RBL Followup');
			}

			if (payload.ApplicationStatus.Code == '200.15' ||
				payload.ApplicationStatus.Code == '200.14') {
				removeTags.push('ALL');
			}
		}
	}
	return { ApplyTags: applyTags, RemoveTags: removeTags };
}
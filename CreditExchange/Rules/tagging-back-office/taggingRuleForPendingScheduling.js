function taggingRuleForPendingScheduling(payload) {
    var applyTags = [];
    var removeTags = [];

    if (typeof (payload.DataAttributes) != 'undefined' && typeof (payload.DataAttributes.application) != 'undefined' && typeof (payload.DataAttributes.application.preferredAddress) != 'undefined') {
        removeTags.push('Pending Scheduling');
    }
    return {
        RemoveTags: removeTags
    };
}

function taggingRuleForFactVerified(payload) {
	function getVerificationFactDetails(input, factName) {
    if (typeof (input) != 'undefined') {
        for (var i = 0; i < input.length; i++) {
            if (input[i]['FactName'] == factName) {
                return input[i];
            }
        }
    }
}

    var applyTags = [];
    var removeTags = [];
    if (typeof (payload) != 'undefined') {
        if (typeof (payload.VerificationDetails) != 'undefined') {
            var bankVerification = getVerificationFactDetails(payload.VerificationDetails, 'BankVerification');
            if (typeof (bankVerification) != 'undefined') {
                if (bankVerification.CurrentStatus == 3) {
                    removeTags.push('Bank Pending');
                }
            }
            var socialVerification = getVerificationFactDetails(payload.VerificationDetails, 'SocialVerification');
            if (typeof (socialVerification) != 'undefined') {
                if (socialVerification.CurrentStatus == 3) {
                    removeTags.push('Social Pending');
                }
            }
            var employmentVerification = getVerificationFactDetails(payload.VerificationDetails, 'EmploymentVerification');
            var workEmailVerification = getVerificationFactDetails(payload.VerificationDetails, 'WorkEmailVerification');
            if (typeof (employmentVerification) != 'undefined') {
                if (employmentVerification.CurrentStatus == 3) {
                    removeTags.push('Employment Pending');
                }
            }
            else if (typeof (workEmailVerification) != 'undefined') {
                if (workEmailVerification.CurrentStatus == 3) {
                    removeTags.push('Employment Pending');
                }
            }
            var residenceVerification = getVerificationFactDetails(payload.VerificationDetails, 'LocationVerification');
            if (typeof (residenceVerification) != 'undefined') {
                if (residenceVerification.CurrentStatus == 3) {
                    removeTags.push('Residence Pending');
                }
            }
        }
    }
    return {
        ApplyTags: applyTags,
        RemoveTags: removeTags
    };
}

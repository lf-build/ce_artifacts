function taggingRuleForFCUSampling(payload){
    var applyTags = [];
	var removeTags = [];
    if(typeof(payload) != 'undefined'){
		if(typeof(payload.Data.OldStatus) != 'undefined'){
            if(payload.Data.OldStatus == '200.09' && payload.Data.NewStatus == '200.10'){
				removeTags.push('FCU Sampled');
			}
        }
    }
    return { ApplyTags: applyTags, RemoveTags: removeTags };
}

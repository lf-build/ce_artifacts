function taggingRuleForFinalOfferAccepted(payload) {

	function getVerificationFactDetails(input, factName) {
		if (typeof (input) != 'undefined') {
			for (var i = 0; i < input.length; i++) {
				if (input[i]['FactName'] == factName) {
					return input[i];
				}
			}
		}
		return undefined;
	}
	var applyTags = [];
	var removeTags = [];
	if (typeof (payload) != 'undefined') {
		if (typeof (payload.VerificationDetails) != 'undefined') {
			var residenceVerification = getVerificationFactDetails(payload.VerificationDetails, 'LocationVerification');
			if (typeof (residenceVerification) != 'undefined') {
				if (residenceVerification.CurrentStatus != 3) {
					applyTags.push('Residence Pending');
				}
			}
		}
	}

	if (typeof (payload.DataAttributes.application.preferredAddress) == 'undefined' || payload.DataAttributes.application.preferredAddress == null) {
		applyTags.push('Pending Scheduling');
	}

	return {
		ApplyTags: applyTags,
		RemoveTags: removeTags
	};
}

function GetLeadSquaredApplicationCreatedEmailData(input) {
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (input.DataAttributes.application.source.SystemChannel == 'Leadsquared' &&
		input.EventData.InitialOffer.Status == 'Completed' &&
		input.DataAttributes.application.source.SourceReferenceId != 'UpdateApplication') {
		if (typeof (input) != 'undefined') {
			if (typeof (input.DataAttributes.application) != 'undefined') {
				var firstname = jsUcfirst(input.DataAttributes.application.firstName);
				var Email = input.DataAttributes.application.personalEmail;
				var sourcedata = input.DataAttributes.application.source.TrackingCode;

				return {
					Name: firstname,
					Email: Email,
					qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png',
					sourcedata: sourcedata
				}
			}
		}
	} else {
		return {
			Email: ''
		};
	}
}

function GetFinalOfferAcceptedNotificationData(input) {
	function ConvertToCurrencyFormat(input) {
		input = input.toString();
		var afterPoint = '';
		if (input.indexOf('.') > 0) {
			afterPoint = input.substring(input.indexOf('.'), input.length);
		}

		input = Math.floor(input);
		input = input.toString();
		var lastThree = input.substring(input.length - 3);
		var otherNumbers = input.substring(0, input.length - 3);
		if (otherNumbers != '') {
			lastThree = ',' + lastThree;
		}
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
		return res;
	}
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function GetTaxAmount(input) {
		var taxAmount = 0;
		if (typeof (input) != 'undefined') {
			var processingFee = input.ProcessingFee;
			taxAmount = Number(processingFee) * 0.18;
		}
		return Math.round(taxAmount);
	}

	if (typeof (input) != 'undefined') {
		if (typeof (input.DataAttributes.application) != 'undefined' && typeof (input.EventData.SelectedFinalOffer) != 'undefined') {
			var firstname = jsUcfirst(input.DataAttributes.application.firstName);
			var amount = ConvertToCurrencyFormat(input.EventData.SelectedFinalOffer.FinalOfferAmount);
			var EMIAmount = ConvertToCurrencyFormat(input.EventData.SelectedFinalOffer.Emi);
			var PeriodInYear = input.EventData.SelectedFinalOffer.Loantenure;
			var ProcessingFees = ConvertToCurrencyFormat(input.EventData.SelectedFinalOffer.ProcessingFee);
			var TaxAmount = ConvertToCurrencyFormat(GetTaxAmount(input.EventData.SelectedFinalOffer));
			var date = input.DataAttributes.application.expiryDate.Time;
			var Email = input.DataAttributes.application.personalEmail;
			var applicationnumber = input.EventData.EntityId;
			var qberalink = 'http://apply.qbera.com/';

			return {
				Name: firstname,
				amount: amount,
				EMIAmount: EMIAmount,
				PeriodInYear: PeriodInYear,
				ProcessingFees: ProcessingFees,
				TaxAmount: TaxAmount,
				date: date,
				Email: Email,
				applicationnumber: applicationnumber,
				qberaurl: qberalink,
				qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png'
			}
		}
	}
}

function GetApplicationInActiveNotificationData(input) {

	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (typeof (input) != 'undefined') {
		if (typeof (input.DataAttributes.application) != 'undefined' && typeof (input.EventData) != 'undefined') {
			return {
				Name: jsUcfirst(input.DataAttributes.application.firstName),
				Email: input.DataAttributes.application.personalEmail,
				applicationnumber: input.EventData.InActiveApplication.ApplicationNumber,
				qberalink: 'http://apply.qbera.com',
				qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png'
			}
		}
	}
}

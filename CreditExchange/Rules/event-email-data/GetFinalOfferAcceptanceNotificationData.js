function GetFinalOfferAcceptanceNotificationData(input) {
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (typeof (input) != 'undefined') {
		if (typeof (input.DataAttributes.application) != 'undefined') {
			var firstname = jsUcfirst(input.DataAttributes.application.firstName);
			var Email = input.DataAttributes.application.personalEmail;
			var applicationnumber = input.EventData.FinalOffer.EntityId;
			var qberalink = 'http://apply.qbera.com/';
			var dateParts = input.DataAttributes.application.expiryDate.Time.split('-');
			var expiryDate = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];


			return {
				Name: firstname,
				Email: Email,
				applicationnumber: applicationnumber,
				qberaurl: qberalink,
				qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png',
				date: expiryDate
			}
		}
	}
}

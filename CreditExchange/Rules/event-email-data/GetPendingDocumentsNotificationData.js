function GetPendingDocumentsNotificationData(input) {


	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (typeof (input) != 'undefined') {
		if (typeof (input.EventData.PendingDocuments) != 'undefined' && input.EventData.PendingDocuments.length > 0) {
			var returnVal = '';
			var documentList = [];
			for (var i = 0; i < input.EventData.PendingDocuments.length; i++) {
				var factDocuments = input.EventData.PendingDocuments[i].DocumentName;
				documentList = documentList.concat(factDocuments);
			}

			for (var i = 0; i < documentList.length; i++) {
				var j = i + 1;
				returnVal = returnVal + j + '. ' + documentList[i] + '<br>';
			}
			returnVal = returnVal + '<br>';

			return {
				documentlist: returnVal,
				Name: jsUcfirst(input.DataAttributes.application.firstName),
				Email: input.DataAttributes.application.personalEmail,
				applicationnumber: input.EventData.PendingDocApplication.ApplicationNumber,
				qberalink: 'http://apply.qbera.com/',
				qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png'
			}
		}
	}
}

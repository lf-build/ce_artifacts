function GetApplicationRejectionNotificationData(input) {

	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (typeof (input) != 'undefined') {
		if (typeof (input.EventData) != 'undefined' && typeof (input.DataAttributes) != 'undefined') {
			if (typeof (input.EventData.Reason) != 'undefined') {
				if (input.EventData.Reason.length > 0) {
					return {
						Name: jsUcfirst(input.DataAttributes.application.firstName),
						reason: input.EventData.Reason[0],
						Email: input.DataAttributes.application.personalEmail,
						applicationnumber: input.EventData.EntityId,
						qberalogo: 'https://crex-cdn.s3.amazonaws.com/templates/logo.png'
					}
				}
			}
		}
	}
}

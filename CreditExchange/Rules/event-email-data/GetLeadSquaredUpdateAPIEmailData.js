function GetLeadSquaredUpdateAPIEmailData(input) {
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	if (input.DataAttributes.application.source.SystemChannel == 'Leadsquared' &&
		input.DataAttributes.application.source.SourceReferenceId == 'UpdateApplication') {
		if (typeof (input) != 'undefined') {
			if (typeof (input.DataAttributes.application) != 'undefined') {
				var firstname = jsUcfirst(input.DataAttributes.application.firstName);
				var Email = input.DataAttributes.application.personalEmail;

				return {
					Name: firstname,
					Email: Email
				}
			}
		}
	} else {
		return {
			Email: ''
		};
	}
}

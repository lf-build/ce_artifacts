function GetUsersFromStatusFilterRoles(input) {
	var self = this;
	var configurationService = self.call('configuration');
	var identityService = self.call('securityService');
	var roles = null;
	var users = null;
	var roleData = null;
	var emails = [];
	var ToAddress = [];
	var duplicate = [];
	var applicationnumber = input.EventData.EntityId;
	var applicantFullname = '';
	var applicantCity = jsUcfirst(input.DataAttributes.application.city);
	var newStatus = input.EventData.NewStatus;


	if (typeof (input.DataAttributes.application) != 'undefined') {
		if (typeof (input.DataAttributes.application.middleName) != 'undefined' && input.DataAttributes.application.middleName != null) {
			applicantFullname = jsUcfirst(input.DataAttributes.application.firstName) + ' ' + input.DataAttributes.application.middleName + ' ' + input.DataAttributes.application.lastName;
		}
		else {
			applicantFullname = jsUcfirst(input.DataAttributes.application.firstName) + ' ' + input.DataAttributes.application.lastName;
		}
	}

	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function checkduplicate(email) {
		var temp = true;
		for (var i = 0; i < duplicate.length; i++) {
			if (duplicate[i] == email) {
				temp = false;
				return false;
			}
		}
		if (temp) {
			return true;
		}
	}
	function getUsersfromRoles(input) {
		return identityService.get(input).then(function (data) {
			for (var k = 0; k < data.length; k++) {
				if(data[k].isActive){				
				var ToAddress = {
					'Email': data[k].email,
					'Name': data[k].name
				};
				if (k > 0) {
					if (checkduplicate(data[k].email)) {
						emails.push(ToAddress);
					}
				}
				else {
					emails.push(ToAddress);
				}
				duplicate.push(data[k].email);
			}
			}
			return {
				'ToAddress': emails,
				'firstname': 'name',
				'applicationNumber': applicationnumber,
				'applicantname': applicantFullname,
				'applicantcity': applicantCity
			};
		});
	}

	function GetConfiguration() {
		return configurationService.get('status-tag-roles').then(function (response) {
			for (var i = 0; i < response.statuses.length; i++) {
				if (response.statuses[i].code == newStatus && response.statuses[i].isCheck == true) {
					roles = response.statuses[i].Roles;
				}
			}
			if (roles == null) {
				return;
			} else {
				for (var j = 0; j < roles.length; j++) {
					if (j == 0) {
						roleData = roles[j];
					} else {
						roleData = roleData + '/' + roles[j];
					}
				}
			}
			return getUsersfromRoles(roleData);
		});
	}
	return GetConfiguration();
}

function GetUsersFromUnTagFilterRoles(input) {
	var self = this;
	var configurationService = self.call('configuration');
	var identityService = self.call('securityService');
	var statusmanagementSerivce = self.call('statusmanagementSerivce');
	var applicationFilterSerivce = self.call('applicationfilterstagservice');
	var tagRemoved = input.EventData.TagName;
	var roles = null;
	var users = null;
	var roleData = null;
	var emails = [];
	var duplicate = [];
	var applicationnumber = input.EventData.ApplicationNumber;
	var applicantFullname = '';
	var applicationStatus = null;
	var hasBankQA = null;
	var eventSource = null;
	var applicantCity = jsUcfirst(input.DataAttributes.application.city);

	if (typeof (input.EventData.EventSource) != 'undefined' && typeof (input.EventData.EventSource) != null) {
		eventSource = input.EventData.EventSource;
	}

	if (typeof (input.DataAttributes.application) != 'undefined') {
		if (typeof (input.DataAttributes.application.middleName) != 'undefined' && input.DataAttributes.application.middleName != null) {
			applicantFullname = jsUcfirst(input.DataAttributes.application.firstName) + ' ' + input.DataAttributes.application.middleName + ' ' + input.DataAttributes.application.lastName;
		}
		else {
			applicantFullname = jsUcfirst(input.DataAttributes.application.firstName) + ' ' + input.DataAttributes.application.lastName;
		}
	}


	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function checkduplicate(email) {
		var temp = true;
		for (var i = 0; i < duplicate.length; i++) {
			if (duplicate[i] == email) {
				temp = false;
				return false;
			}
		}
		if (temp) {
			return true;
		}
	}

	function getUsersfromRoles(input) {
		return identityService.get(input).then(function (data) {
			for (var k = 0; k < data.length; k++) {
				if(data[k].isActive){				
				var ToAddress = {
					'Email': data[k].email,
					'Name': data[k].name
				};
				if (k > 0) {
					if (checkduplicate(data[k].email)) {
						emails.push(ToAddress);
					}
				}
				else {
					emails.push(ToAddress);
				}
				duplicate.push(data[k].email);
			}
			}
			return {
				'ToAddress': emails,
				'firstname': 'name',
				'applicationNumber': applicationnumber,
				'applicantname': applicantFullname,
				'applicantcity': applicantCity
			};
		});
	}

	function GetConfiguration() {
		return configurationService.get('status-tag-roles').then(function (response) {
			if (applicationStatus == '200.11') {
				if (hasBankQA != null) {
					for (var i = 0; i < response.Addtags.length; i++) {
						if (hasBankQA == response.Addtags[i].tag) {
							roles = response.Addtags[i].Roles;
						}
					}
				}
				else {
					for (var i = 0; i < response.Removetags.length; i++) {
						if (response.Removetags[i].tag == 'Ready for BankQA') {
							roles = response.Removetags[i].Roles;
						}
					}
				}
			}
			else {
				if (applicationStatus != null) {
					for (var i = 0; i < response.statuses.length; i++) {
						if (response.statuses[i].code == applicationStatus) {
							roles = response.statuses[i].Roles;
						}
					}
				} else {
					for (var i = 0; i < response.Removetags.length; i++) {
						if (tagRemoved == response.Removetags[i].tag) {
							roles = response.Removetags[i].Roles;
						}
					}
				}
			}
			if (roles == null) {
				return;
			} else {
				for (var j = 0; j < roles.length; j++) {
					if (j == 0) {
						roleData = roles[j];
					} else {
						roleData = roleData + '/' + roles[j];
					}
				}
			}
			return getUsersfromRoles(roleData);
		});
	}

	function getApplicationTags() {
		return applicationFilterSerivce.get(applicationnumber).then(function (response) {
			for (var i = 0; i < response.tags.length; i++) {
				if (response.tags[i].tagName == 'Ready for BankQA') {
					hasBankQA = response.tags[i].tagName;
				}
			}
			return GetConfiguration();
		})
	}

	function getStatusOfApplication() {
		if (eventSource == null && tagRemoved == 'Ready for BankQA') {
			return;
		}
		else if (tagRemoved == 'RBL Followup') {
			return statusmanagementSerivce.get(applicationnumber).then(function (response) {
				applicationStatus = response.code;
				if (applicationStatus == '200.11') {
					return getApplicationTags();
				} else {
					return GetConfiguration();
				}
			})
		} else {
			return GetConfiguration();
		}
	}
	return getStatusOfApplication();
}

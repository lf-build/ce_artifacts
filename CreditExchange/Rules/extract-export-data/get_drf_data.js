function get_drf_data(input) {
	var data;
	function getBeneName(input) {
		var beneName = '';
		if (typeof (input) != 'undefined') {
			if (input.application.firstName != null) {
				beneName = beneName + input.application.firstName;
			}
			
			if (input.application.middleName != null) {
				beneName = beneName + ' ' + input.application.middleName;
			}
			
			if (input.application.lastName != null) {
				beneName = beneName + ' ' +  input.application.lastName;
			}
		}
		return beneName;
	}

	function mergeBankAddressFields(input) {
		var result = '';
		if (typeof (input) != 'undefined') {
			result += (input.AddressLine1 != null ? input.AddressLine1.replace(/[^A-Z0-9-_.&@]+/ig, " ") : '') + ', ' + (input.AddressLine2 != null ? input.AddressLine2.replace(/[^A-Z0-9-_.&@]+/ig, " ") : '') + ', ';
			result += input.AddressLine3 != null ? input.AddressLine3.replace(/[^A-Z0-9-_.&@]+/ig, " ") + ', ' : '';
			result += input.AddressLine4 != null ? input.AddressLine4.replace(/[^A-Z0-9-_.&@]+/ig, " ") + ', ' : '';
			result += input.City != null ? input.City + ', ' : '';
			result += input.State != null ? input.State : '';
			result += input.PinCode != null ? ' ' + input.PinCode : '';
		}
		return result;
	}

	function getProcessingFee(input) {
		if (typeof (input) != 'undefined') {
			var loanAmount = input.SelectedFinalOffer.FinalOfferAmount;
			var processingFee = loanAmount * (input.SelectedFinalOffer.ProcessingFeePercentage / 100);
			if (processingFee < 1500) {
				processingFee = 1500;
			}

			if (processingFee > 10000) {
				processingFee = 10000;
			}

			var serviceTax = processingFee * 0.18;
			var sbCess = 0;
			var kkCess = 0;

			return processingFee + serviceTax + sbCess + kkCess;
		}
	}

	try {
		data = {
			ApplicationNumber: input.ApplicationNumber,
			LoanProcessingFees: getProcessingFee(input),
			BorrowerName: getBeneName(input.DataAttributes),
			BorrowerAccNo: input.DataAttributes.application.bankInformation[0].AccountNumber,
			BorrowerBankName: input.DataAttributes.application.bankInformation[0].BankName,
			BorrowerBankAddress: mergeBankAddressFields(input.DataAttributes.application.bankInformation[0].BankAddresses),
			ResidenceCity: input.DataAttributes.application.currentAddress.City,
			BorrowerBankIFSC: input.DataAttributes.application.bankInformation[0].IfscCode
		};
	} catch (e) {
		return null;
	}
	return data;
};
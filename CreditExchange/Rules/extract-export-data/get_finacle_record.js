function get_finacle_record(input) {    
	function formatDate(date) {
    if (typeof (date) != 'undefined') {
        var dateParts = date.split('-');                
        var returnValue = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
        return returnValue;
    }
    else {
        return '';
    }
}

function getDocumentDate(documentDate){
	if(typeof(documentDate) != 'undefined'){
		var dateObject = new Date(documentDate);
		return ( '0' + dateObject.getDate()).slice(-2)  + '-' + ( '0' + (dateObject.getMonth() + 1)).slice(-2) + '-' + dateObject.getFullYear();
	}
	else{
		return '';
	}
}

function toUpperCase(input) {
    return input.toUpperCase();
}

function getGenderCode(input) {
    var returnValue = '';
    if (typeof (input) != 'undefined') {
        switch (input.toUpperCase()) {
            case 'MALE': returnValue = 'M';
                break;
            case 'FEMALE': returnValue = 'F';
        }
    }
    return returnValue;
}

function getMaritalStatusCode(input) {
    var returnValue = '';
    if (typeof (input) != 'undefined') {
        switch (input.toUpperCase()) {
            case 'MARRIED': returnValue = 'MARR';
                break;
            case 'SINGLE': returnValue = 'UNMAR';
        }
    }
    return returnValue;
}

function getEduQualificationCode(input) {
    var returnValue = '';
    if (typeof (input) != 'undefined') {
        switch (input.toUpperCase()) {
            case 'HIGHSCHOOL': returnValue = 'OTH';
                break;
            case 'BACHELORDEGREE': returnValue = 'GRADUATE';			      
                break;
            case 'MASTERDEGREE': returnValue = 'POST GRADUATE';			      
                break;
            case 'DOCTRATEDEGREE': returnValue = 'PROFESSIONAL';			     
        }
    }
    return returnValue;
}

function getCasteCode(input) {
    var returnVal = input;
    switch (input.toUpperCase()) {
        case 'GENERAL': returnVal = 'OTH';
            break;
        case 'OTHER': returnVal = 'NA';
    }
    return returnVal.toUpperCase();
}

function getCommunityCode(input) {
    var returnVal = '';
    switch (input.toUpperCase()) {
        case 'HINDU': returnVal = 'HINDU';
            break;
        case 'MUSLIM': returnVal = 'MUSLI';
            break;
        case 'CHRISTIAN': returnVal = 'CHRIS';
            break;
        case 'SIKH': returnVal = 'SIKHS';
            break;
        case 'ZOROASTRIAN': returnVal = 'ZORAS';
            break;
        case 'JAIN': returnVal = 'JAIN';
            break;
		case 'BUDDHIST' : returnVal = 'BUDHI';
			break;
		case 'JEW' : returnVal = 'JEW';
			break;
        default: returnVal = 'OTHER';
            break;
    }
    return returnVal;
}

function extractAddressDetails(input) {
    var address;
    if (typeof (input) != 'undefined') {
        address = {
            AddressType: '',
            AddressLine1: (input.AddressLine1 != null ? toUpperCase(input.AddressLine1.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : '') + ' ' + (input.AddressLine2 != null ? toUpperCase(input.AddressLine2.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : ''),
            AddressLine2: input.AddressLine3 != null ? toUpperCase(input.AddressLine3.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : '',
            AddressLine3: input.AddressLine4 != null ? toUpperCase(input.AddressLine4.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : '',
            City: toUpperCase(input.City),
            State: toUpperCase(input.State),
            Pincode: input.PinCode
        }
    }
    return address;
}

function getGrossIncome(input){
	var grossIncome = 0;
	var returnVal = '';
	if(typeof(input) != 'undefined'){
		grossIncome = parseInt(input) * 12;
		switch(true){
			case (grossIncome <= 60000) :
							returnVal = 'AI01';
							break;
			case (grossIncome > 60000 && grossIncome <= 120000) :
							returnVal = 'AI02';
							break;
			case (grossIncome > 120000 && grossIncome <= 200000) :
							returnVal = 'AI03';
							break;
			case (grossIncome > 200000 && grossIncome <= 295000) :
							returnVal = 'AI04';
							break;
			case (grossIncome > 295000 && grossIncome <= 475000) :
							returnVal = 'AI05';
							break;
			case (grossIncome > 475000 && grossIncome <= 875000) :
							returnVal = 'AI06';
							break;
			case (grossIncome > 875000 && grossIncome <= 3675000) :
							returnVal = 'AI07';
							break;
			case (grossIncome > 3675000) :
							returnVal = 'AI08';
							break;
		}
	}
	return returnVal;
}

function getCompanyCode(input){
	var companyCode = '';
	if(typeof(input) != 'undefined'){
		if(input.typeofcompany == 'Private'){
			companyCode = 'F001';
		}else if(input.typeofcompany == 'Public'){
			companyCode = 'F004';
		}
		
		if(input.subCategory == 'Union' || input.subCategory == 'State'){
			companyCode = 'F005';
		}
	}
	return companyCode;
}

function mergeBankAddressFields(input){
	var result = '';
	if(typeof(input) != 'undefined')
	{
		 result += (input.AddressLine1 != null ? toUpperCase(input.AddressLine1.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : '') + ', ' + (input.AddressLine2 != null ? toUpperCase(input.AddressLine2.replace(/[^A-Z0-9-_.&@]+/ig, " ")) : '') + ', ';
         result += input.AddressLine3 != null ? toUpperCase(input.AddressLine3.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + ', ' : '' ;
         result += input.AddressLine4 != null ? toUpperCase(input.AddressLine4.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + ', ' : '' ;
         result += input.City != null ? toUpperCase(input.City) + ', ' : '';
         result += input.State != null ? toUpperCase(input.State) : '';
         result +=  input.PinCode != null ?  ' '  + input.PinCode : '';
	}
	return result;
}

function getBeneName(input){
	if(typeof(input) != 'undefined'){
		if(typeof(input.DataAttributes.perfiosReport) != 'undefined'){
			return input.DataAttributes.perfiosReport.name;
		}
		
		if(typeof(input.DataAttributes.yodleeCashFlowReport) != 'undefined'){
			return input.DataAttributes.yodleeCashFlowReport.name;
		}

		return '';
	}
}

function getTaxesAndCharges(input){
	if(typeof(input) != 'undefined'){
		var loanAmount = input.DataAttributes.selectedFinalOffer.FinalOfferAmount;
		var processingFee = loanAmount * (input.DataAttributes.selectedFinalOffer.ProcessingFeePercentage / 100);
		if(processingFee < 1500){
			processingFee = 1500;
		}
		
		if(processingFee > 10000){
			processingFee = 10000;
		}
		
		var serviceTax = processingFee * 0.18;
		var sbCess =0;
		var kkCess = 0;
		
		return {
			ProcessingFee : processingFee,
			ServiceTax : serviceTax,
			SbCess : sbCess,
			KkCess : kkCess
		}
	}
}


try {
        var errorData = [];
        var data = {
            TypeOfCust: 'M',
            TitleId: toUpperCase(input.DataAttributes.application.salutation) + '.',
            FirstName: toUpperCase(input.DataAttributes.application.firstName),
            MiddleName: toUpperCase(input.DataAttributes.application.middleName != null ? input.DataAttributes.application.middleName :  ''),
            LastName: toUpperCase(input.DataAttributes.application.lastName),
            MotherMaidenName: toUpperCase(input.DataAttributes.ancillaryData.MothersMaidenName),
            DateOfBirth: formatDate(input.DataAttributes.application.dateOfBirth),
            Gender: getGenderCode(input.DataAttributes.application.gender),
            MaritalStatus: getMaritalStatusCode(input.DataAttributes.application.maritalStatus),
            Qualification: getEduQualificationCode(input.DataAttributes.application.educationalQualification),
            EmploymentStatus: 'SALARIED',
            TypeOfCompany: getCompanyCode(input.DataAttributes.companyCategoryReport),
            Occupation: '29',
            SourceOfIncome: 'SALARY',
            GrossIncome: getGrossIncome(input.DataAttributes.application.income),
            Caste: getCasteCode(input.DataAttributes.ancillaryData.Category),
            Community: getCommunityCode(input.DataAttributes.ancillaryData.Religion),
            PanNumber: toUpperCase(input.DataAttributes.application.pan),
            DocumentCode1: 'PAN',
            UniqueId1: toUpperCase(input.DataAttributes.application.pan),
            DocumentExpiryDate1: '',
            DocumentCode2: 'UID',
            UniqueId2: toUpperCase(input.DataAttributes.application.aadharNumber != null ? input.DataAttributes.application.aadharNumber : ''),
            DocumentExpiryDate2: '',
            AddressType1: '',
            AddressLine11: '',
            AddressLine21: '',
            AddressLine31: '',
            City1: '',
            State1: '',
            Pincode1: '',
            AddressType2: '',
            AddressLine12: '',
            AddressLine22: '',
            AddressLine32: '',
            City2: '',
            State2: '',
            Pincode2: '',
            PhoneType1: 'CELLPH',
            PhoneNo1: input.DataAttributes.application.mobileNo,
            PhoneType2: '',
            PhoneNo2: '',
            EmailType: 'HOMEEML',
            EmailId: toUpperCase(input.DataAttributes.application.personalEmail),
            LoanAmount: input.DataAttributes.selectedFinalOffer.FinalOfferAmount.toFixed(2),
            RateOfInterest: input.DataAttributes.selectedFinalOffer.InterestRate.toFixed(2),
            Tenor: input.DataAttributes.selectedFinalOffer.LoanTenure,
            EMIAmount: input.DataAttributes.selectedFinalOffer.Emi.toFixed(2),
            AccountOpeningDate: '',
            EMIStartDate: '',
            LoanSanctionDate: input.LoanSanctionDate,
            SanctionRefNo: input.ApplicationNumber,
            DocumentDate: getDocumentDate(input.DataAttributes.ancillaryData.DocumentDate),
            RepaymentMethod: 'D',
            OperativeAccountNo: '',
            BrokenPeriodInterest: '',
            BrokenPeriodIntDueDate: '',
            ProcessingFee: '',
            PfServiceTax: '',
            PfSbSt: '',
            PfKkc: '',
            NetDisbursementAmt: '',
            IfscCode: toUpperCase(input.DataAttributes.application.bankInformation[0].IfscCode),
            BeneAccNo: toUpperCase(input.DataAttributes.application.bankInformation[0].AccountNumber),
            BeneName: toUpperCase(getBeneName(input)),
            BeneAddress1: mergeBankAddressFields(input.DataAttributes.application.bankInformation[0].BankAddresses),
        };

        var mailingAddress = extractAddressDetails(input.DataAttributes.application.currentAddress);
        data.AddressType1 = 'MAILING';
        data.AddressLine11 = mailingAddress.AddressLine1;
        data.AddressLine21 = mailingAddress.AddressLine2;
        data.AddressLine31 = mailingAddress.City;
        data.City1 = mailingAddress.City;
        data.State1 = mailingAddress.State;
        data.Pincode1 = mailingAddress.Pincode;

        var permanentAddress = extractAddressDetails(input.DataAttributes.application.permanentAddress);
        data.AddressType2 = 'PERMANENT';
        data.AddressLine12 = permanentAddress.AddressLine1;
        data.AddressLine22 = permanentAddress.AddressLine2;
        data.AddressLine32 = permanentAddress.City;
        data.City2 = permanentAddress.City;
        data.State2 = permanentAddress.State;
        data.Pincode2 = permanentAddress.Pincode;
		
		var taxesNCharges = getTaxesAndCharges(input);
		data.ProcessingFee = taxesNCharges.ProcessingFee.toFixed(2);
		data.PfServiceTax = taxesNCharges.ServiceTax.toFixed(2);
		data.PfSbSt = taxesNCharges.SbCess.toFixed(2);
		data.PfKkc = taxesNCharges.KkCess.toFixed(2);
		
    } catch (e) {
        return null;
    }
	return data;
};

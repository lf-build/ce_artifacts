function get_hunter_record(input) {    
        function formatDate(date) {
            if (typeof date != undefined) {
				var dateParts = date.split('-');                
                var returnValue = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
                return returnValue;
            }
            else {
                return '';
            }
        }

        function getAddress(input) {
            var address;
            if (typeof (input) != 'undefined') {
                address = {
                    Address: (input.AddressLine1 != null ? toUpperCase(input.AddressLine1.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + ' ' : ' ') +
                             (input.AddressLine2 != null ? toUpperCase(input.AddressLine2.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + ' ' : ' ') +
                             (input.AddressLine3 != null ? toUpperCase(input.AddressLine3.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + ' ' : ' ') +
                             (input.AddressLine4 != null ? toUpperCase(input.AddressLine4.replace(/[^A-Z0-9-_.&@]+/ig, " ")) + '' : ''),
                    City: input.City && input.City != null ? toUpperCase(input.City) : '',
                    State: input.State && input.State != null ? toUpperCase(input.State) : '',
                    Pincode: input.PinCode && input.PinCode != null ? input.PinCode : '',
                }
            }
            return address;
        }

        function toUpperCase(input) {
            return input.toUpperCase();
        }
        var errorData = [];
        try {
		var data = {
            'Month': '',
            'BatchNumber': '',
            'Identifier': '',
            'Product': '',
            'Classification': '',
            'DateOfApplication': formatDate(input.DataAttributes.application.applicationDate.Time),
            'FileCreationDate': ( '0' + new Date().getDate()).slice(-2) + '-' + ( '0' + (new Date().getMonth() + 1)).slice(-2) + '-' + new Date().getFullYear(),
            'LoanTenureInMonths': input.DataAttributes.selectedFinalOffer.LoanTenure,
            'LoanAmountApplied': input.DataAttributes.application.requestedAmount.toFixed(2),
            'ApprovedLoanAmount': input.DataAttributes.selectedFinalOffer.FinalOfferAmount.toFixed(2),
            'PanNumber': toUpperCase(input.DataAttributes.application.pan),
            'ApplicantFirstName': toUpperCase(input.DataAttributes.application.firstName),
            'ApplicantMiddleName': input.DataAttributes.application.middleName ? toUpperCase(input.DataAttributes.application.middleName) : '',
            'ApplicantLastName': toUpperCase(input.DataAttributes.application.lastName),
            'ApplicantDOB': formatDate(input.DataAttributes.application.dateOfBirth),
            'ApplicantResiAddress': '',
            'ApplicantResiCity': '',
            'ApplicantResiState': '',
            'ApplicantResiCountry': 'INDIA',
            'ApplicantResiPinCode': '',
            'ApplicantMobileNumber': input.DataAttributes.application.mobileNo,
            'ApplicantMobilePhoneNumber': '', /* This field will always be exported as blank */
            'ApplicantBusinessPhoneNumber': '', /* This field will always be exported as blank */
            'ApplicantSalAccBank': toUpperCase(input.DataAttributes.application.bankInformation[0].BankName.replace(/[^a-zA-Z0-9_-]/ig, " ")),
            'ApplicantSalAccNumber': input.DataAttributes.application.bankInformation[0].AccountNumber,
            'ApplicantIdDocType': 'PAN CARD',
            'ApplicantIdDocNumber': toUpperCase(input.DataAttributes.application.pan),
            'ApplicantAltIdDocType': 'GOVERNMENT ID CARD',
            'ApplicantAltIdDocNumber': toUpperCase(input.DataAttributes.application.aadharNumber != null ? input.DataAttributes.application.aadharNumber : ''),
            'ApplicantEmployerName': toUpperCase(input.DataAttributes.application.companyName),
            'ApplicantEmploymentAddress': '',
            'ApplicantEmploymentCity': '',
            'ApplicantEmploymentState': '',
            'ApplicantEmploymentCountry': 'INDIA',
            'ApplicantEmploymentPinCode': '',
            'UploadedDate': '',
            'HunterMatchStatus': '',
            'Suspect': '',
            'Reason': '',
            'ClosureDate': '',
            'DeviationTakenby': '',
            'DeviationMitigate': '',
            'ifofmatch': ''
        };

        var residentialAddress = getAddress(input.DataAttributes.application.currentAddress);
        data.ApplicantResiAddress = residentialAddress.Address + ' ' + residentialAddress.City + ' ' + residentialAddress.State + ' ' +residentialAddress.Pincode;
        data.ApplicantResiCity = residentialAddress.City;
        data.ApplicantResiState = residentialAddress.State;
        data.ApplicantResiPinCode = residentialAddress.Pincode;

        var employmentAddress = getAddress(input.DataAttributes.application.employmentAddress[0]);
        data.ApplicantEmploymentAddress = employmentAddress.Address + ' ' + employmentAddress.City + ' ' + employmentAddress.State + ' ' + employmentAddress.Pincode;
        data.ApplicantEmploymentCity = employmentAddress.City;
        data.ApplicantEmploymentState = employmentAddress.State;
        data.ApplicantEmploymentPinCode = employmentAddress.Pincode;

    } catch (e) {       
        return null;
    }

    return data;
};


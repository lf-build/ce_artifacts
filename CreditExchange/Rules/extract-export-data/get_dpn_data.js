function get_dpn_data(input) {
	var data = undefined;
	function getBeneName(input) {
		var beneName = '';
		if (typeof (input) != 'undefined') {
			if (input.application.firstName != null) {
				beneName = beneName + input.application.firstName;
			}
			
			if (input.application.middleName != null) {
				beneName = beneName + ' ' + input.application.middleName;
			}
			
			if (input.application.lastName != null) {
				beneName = beneName + ' ' +  input.application.lastName;
			}
		}
		return beneName;
	}

	function getFinalAmountInWords(x) {
		var r = 0;
		var txter = x;
		var sizer = txter.length;
		var numStr = '';
		if (isNaN(txter)) {
			return '';		
		}
		var n = parseInt(x);
		var places = 0;
		var str = '';
		var entry = 0;
		while (n >= 1) {
			r = parseInt(n % 10);

			if (places < 3 && entry == 0) {
				numStr = txter.substring(txter.length, txter.length - 3);
				str = onlyDigit(numStr);
				entry = 1;
			}

			if (places == 3) {
				numStr = txter.substring(txter.length - 5, txter.length - 3);
				if (numStr != '') {
					str = onlyDigit(numStr) + ' Thousand ' + str;
				}
			}

			if (places == 5) {
				numStr = txter.substring(txter.length - 7, txter.length - 5);
				if (numStr != '') {
					if (numStr == '1'){
						str = onlyDigit(numStr) + ' Lakh ' + str;
					}
					else{
						str = onlyDigit(numStr) + ' Lakhs ' + str;
					}
				}
			}

			if (places == 6) {
				numStr = txter.substring(txter.length - 9, txter.length - 7);
				if (numStr != '') {
					if (numStr == '1'){
						str = onlyDigit(numStr) + ' Crore ' + str;
					}
					else{
						str = onlyDigit(numStr) + ' Crores ' + str;
					}
				}
			}

			n = parseInt(n / 10);
			places++;
		}
		return str;
	}

	function onlyDigit(n) {
		var units = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
		var randomer = ['', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tens = ['', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
		var r = 0;
		var num = parseInt(n);
		var str = '';
		var pl = '';
		var tenser = '';
		while (num >= 1) {
			r = parseInt(num % 10);
			tenser = r + tenser;
			if (tenser <= 19 && tenser > 10) {
				str = randomer[tenser - 10];
			} else {
				if (pl == 0) {
					str = units[r];
				} else if (pl == 1) {
					str = tens[r] + ' ' + str;
				}
			}
			if (pl == 2) {
				str = units[r] + ' Hundred ' + str;
				str = units[r] + ' Hundred ' + str;
			}

			num = parseInt(num / 10);
			pl++;
		}
		return str;
	}

	try {
		data = {
			ApplicationNumber: input.ApplicationNumber,
			LoanAmount: input.SelectedFinalOffer.FinalOfferAmount,
			LoanAmountWords: getFinalAmountInWords(input.SelectedFinalOffer.FinalOfferAmount.toString()),
			InterestRate: input.SelectedFinalOffer.InterestRate,
			BorrowerName: getBeneName(input.DataAttributes),
			ResidenceCity: input.DataAttributes.application.currentAddress.City
		};
	} catch (e) {
		return null;
	}
	return data;
};

function factVerification() {
    var baseUrl = 'http://verification-engine:5000';
    var self=this;
    return {
        get: function (entityType, entityId) {
            var url = [baseUrl, entityType, entityId].join('/');
            return new Promise(function (resolve, reject) {
                try {
                    self.http.get(url).catch(function (error) {
                        reject('fact verification get failed for1 ' + entityType + ':' + entityId);
                    })
                        .then(function (response) {
                            resolve(response);
                        });
                } catch (exception) {
                    reject('fact verification get failed for2 ' + entityType + ':' + entityId + ":" + exception);
                }
            });
        }
    };
}
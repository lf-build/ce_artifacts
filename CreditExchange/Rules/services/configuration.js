function configuration() {
	var baseUrl = 'http://configuration:5000';
	var self = this;
	return {
		get: function (configurationName) {
			var url = [baseUrl, configurationName].join('/');
			return new Promise(function (resolve, reject) {
				try {
					self.http.get(url).catch (function (error) {
						reject('configuration not found ' + url);
					})
						.then(function (response) {
							resolve(response);
						});
				} catch (exception) {
					reject('configuration not found ' + url + ':' + ':' + exception);
				}
			});
		}
	};
}

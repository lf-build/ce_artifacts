function applicationfilterstagservice() {
    var baseUrl = 'http://application-filters:5000';
    var self = this;
    return {
        get: function (applicationNumber) {
            var url = [baseUrl, 'getapplicationtaginfo', applicationNumber].join('/');
            return new Promise(function (resolve, reject) {
                try {
                    self.http.get(url).catch(function (error) {
                        reject('Not able to get staus ' + url);
                    })
                        .then(function (response) {
                            resolve(response);
                        });
                } catch (exception) {
                    reject('Not able to get users data ' + url + ':' + ':' + exception);
                }
            });
        }
    };
}

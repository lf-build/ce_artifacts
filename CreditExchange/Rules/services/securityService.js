function securityService() {
    var baseUrl = 'http://identity:5000';
    var self = this;
    return {
        get: function (roles) {
            var url = [baseUrl, 'all', 'users', roles].join('/');
            return new Promise(function (resolve, reject) {
                try {
                    self.http.get(url).catch(function (error) {
                        reject('Not able to get users data ' + url);
                    })
                        .then(function (response) {
                            resolve(response);
                        });
                } catch (exception) {
                    reject('Not able to get users data ' + url + ':' + ':' + exception);
                }
            });
        }
    };
}

function verification_rule_pan(payload) {
    try {
        if (payload == null || payload.applicationNumber == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        }
        var finalData = payload.data.CrifPanVerifcation.attributes.crifPanReport;
        if (finalData.score != null || finalData.score != '') {
            if (finalData.score >= 40) {
               return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            } else {
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            }
        } else {
            return {
                    'result': 'Failed',
                    'detail': ['Unable to verify'],
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
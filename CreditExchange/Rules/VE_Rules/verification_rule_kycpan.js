function verification_rule_kycpan(payload) {
    try {
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        };
        var finalData = payload.kycPanData;
        if (finalData != null || finalData != '') {
            if (finalData.applicantNameMatch != true || finalData.photoGraphMatch != true || finalData.dateOfBirthMatch != true || finalData.panCardMatch != true) {
                  return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            } else {
               return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            }
        } else {
            return {
                    'result': 'Failed',
                    'detail': ['Unable to verify'],
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
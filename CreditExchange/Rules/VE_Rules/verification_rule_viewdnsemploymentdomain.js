function verification_rule_workemailanddomainmatch(payload) {
    try {
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        }
        var finalData = payload.application;
        var input = payload.workEmailVerificationData.data.data;
        var domainmatched = payload.domainSearched.domainMatched;
        if (finalData != null && input != null && finalData.companyEmailAddress != '' 
            && finalData.companyEmailAddress == input.Email && input.VerificationStatus == 'Verified'
            && domainmatched == true) {
            return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        } else {
            return {
                'result': 'UnDefined',
                'detail': ['UnDefined'],
                'data': null,
                'rejectcode': '',
                'exception': []
            }; 
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
function verification_rule_personalemail(payload) {
    try {
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        }
        var finalData = payload.application;
        var input = payload.personalEmailVerificationData.data.data;
        if (finalData != null && input != null && finalData.personalEmail != '' && finalData.personalEmail == input.Email && input.VerificationStatus == 'Verified') {
            return {
                'result': 'Passed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': []
            };
        } else {
            return {
                'result': 'UnDefined',
                'detail': ['UnDefined'],
                'data': null,
                'rejectcode': '',
                'exception': []
            };
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
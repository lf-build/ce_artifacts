function verification_rule_yodleeaccountnumber(payload) {
    try {
        var result = 'Failed';
        if (payload == null || payload.applicationNumber == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        }
        var application = payload.data.yodlee.attributes.application;
        var finalData = payload.data.yodlee.attributes.yodleeCashFlowReport;
        if (application == null || finalData == null || application.bankInformation == null) {
           return {
                'result': 'Failed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': []
            }; 
        }
        var bankInfo = application.bankInformation[0];
        var applicationAccountNumber = bankInfo.AccountNumber;
        var applicationBankHolderName = application.firstName;
        var yodleeAccountNumber = finalData.accountNo;
        var yodleeName = finalData.name;
        if ((yodleeName.toLowerCase()).includes(applicationBankHolderName.toLowerCase()) == false)
             return {
                'result': 'Failed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': []
            }; 
        if (applicationAccountNumber.length == yodleeAccountNumber.length) {
            var matched = true;          
            for (var i = 0; i < applicationAccountNumber.length; i++) {
                if (!isNaN(yodleeAccountNumber[i])) {
                    if (applicationAccountNumber[i] != yodleeAccountNumber[i]) {
                        matched = false;
                        break;
                    }
                }
            }
            if (matched) {
                result = 'Passed';
            } else {
                result= 'Failed';
            }
        } else {
            result= false;
        }
      return {
                'result': result,
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': []
            }; 
    } catch (e) {
         return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
function verification_rule_cibiladdress(payload) {
    try {
         
        var cibilReport = payload.cibilReport;
        if (cibilReport == null) {
               return {
                    'result': 'Failed',
                    'detail': ['Unable to verify'],
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        } else {
            if (cibilReport.addressMatch != true) {
                  return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            } else {
                 return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            }
        }
       
    }
    catch (e) {
         return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
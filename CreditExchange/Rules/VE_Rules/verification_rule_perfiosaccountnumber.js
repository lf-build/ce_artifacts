function verification_rule_perfiosaccountnumber(payload) {
    try {
        var result = 'Failed';
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        }
        var application = payload.application;
        var finalData = payload.perfiosReport;
        if (application == null || finalData == null || application.bankInformation == null) {
            result = 'Failed';
        }
        var bankInfo = application.bankInformation[0];
        var applicationAccountNumber = bankInfo.AccountNumber;
        var applicationBankHolderName = application.firstName;
        var perfiosAccountNumber = finalData.accountNo;
        var perfiosName = finalData.name;
        if ((perfiosName.toLowerCase()).includes(applicationBankHolderName.toLowerCase()) == false) {
            result = 'Failed';
        }
        if (applicationAccountNumber.length == perfiosAccountNumber.length) {
            var matched = true;
            for (var i = 0; i < applicationAccountNumber.length; i++) {
                if (!isNaN(perfiosAccountNumber[i])) {
                    if (applicationAccountNumber[i] != perfiosAccountNumber[i]) {
                        matched = false;
                        break;
                    }
                }
            }
            if (matched) {
                result = 'Passed';
            } else {
                result = 'Failed';
            }
        } else {
            result = 'Failed';
        }


        return {
            'result': result,
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': []
        };

    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
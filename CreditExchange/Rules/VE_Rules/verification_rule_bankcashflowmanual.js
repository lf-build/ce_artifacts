function verification_rule_bankcashflowmanual(payload) {
    try {
        var result = 'Failed';
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        };
        var finalData = payload.bankManualData;
        var finalDatafromAttributes = payload.application;
        if (finalData != null && finalData != '') {
            if (finalData.accountNumber != null && finalData.accountNumber != '' && finalDatafromAttributes.bankInformation[0].AccountNumber != null) {
                if (finalData.accountNumber != finalDatafromAttributes.bankInformation[0].AccountNumber || finalData.applicantNameMatch != true || finalData.bankNameMatch != true) {
                    result = 'false';
                } else {
                    result = 'true';
                }
            } else {
                result = 'Unable to verify';
            }
        } else {
            result = 'Unable to verify';
        }
        switch (result) {
            case 'false':
                return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            case 'true':
                return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            default:
                return {
                    'result': 'UnDefined',
                    'detail':null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        }

    }
    catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
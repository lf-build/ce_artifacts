function verification_rule_pan_manual_verification(payload) {
    try {
        if (payload == null || payload.entityId == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        };
        var finalData = payload.panManualVerificationData;
        if (finalData != null || finalData != '') {
            if (finalData.panNameMatch != true || finalData.panNoMatch != true) {
                  return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            } else {
                return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            }
        } else {
            throw new Error('Payload is not filled properly');
        }
    } catch (e) {
         return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
function verification_rule_fraudmanual(payload) {
    try {
        if (payload == null || payload.applicationNumber == null) {
            throw new Error('Payload/ApplicationNumber is not filled properly');
        };
        var finalData = payload.data.HunterForm.input;
        if (finalData != null || finalData != '') {
            if (finalData.hunterScore != null && finalData.hunterScore != '' && finalData.hunterScore > 0) {
                 return {
                    'result': 'Passed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            } else {
                 return {
                    'result': 'Failed',
                    'detail': null,
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
            }
        } else {
            return {
                    'result': 'Failed',
                    'detail': ['Unable to verify'],
                    'data': null,
                    'rejectcode': '',
                    'exception': []
                };
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
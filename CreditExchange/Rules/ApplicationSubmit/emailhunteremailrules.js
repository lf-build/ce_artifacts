function emailhunter_verify_emailrules(payload) {
    try {

        function validateRequiredInput(input) {
            var emailhunterProperty = ['Regexp', 'Disposable', 'SmtpServer', 'SmtpCheck'];
            var errorData = [];
            var errorMessage;
            if (typeof (input) != 'undefined') {
                for (var i = 0; i < emailhunterProperty.length; i++) {
                    var el = input[emailhunterProperty[i]];
                    if (typeof (el) === 'undefined') {
                        errorMessage = 'Input property : ' + emailhunterProperty[i] + ' not found.';
                        errorData.push(errorMessage);
                    }
                };
                if (errorData.length > 0) {
                    return {
                        'result': false,
                        'detail': errorData,
                        'data': null
                    };
                } else {
                    return {
                        'result': true,
                        'detail': errorData,
                        'data': null
                    };
                }
            } else {
                errorMessage = 'input data not found.';
                errorData.push(errorMessage);
                return {
                    'result': false,
                    'detail': errorData,
                    'data': null
                };
            }
        };

        function verifyValidEmail(input) {
            var isEmailVerified = input.Regexp;
            if (!isEmailVerified) {
                return {
                    success: false,
                    message: 'Email :' + input.email + ' not valid.'
                };
            };
            return {
                success: true
            };
        };

        function verifyDisposable(input) {
            var isDisposable = input.Disposable;
            if (isDisposable) {
                return {
                    success: false,
                    message: 'Disposable : email address has a domain name used for temporary email addresses'
                };
            };
            return {
                success: true
            };
        };



        function verifySmtpConnect(input) {
            var isSmtpConnect = input.SmtpServer;
            if (!isSmtpConnect) {
                return {
                    success: false,
                    message: 'SMTP Server does not presence.'
                };
            };
            return {
                success: true
            };
        };

        function verifySmtpCheck(input) {
            var isSmtpCheck = input.SmtpCheck;
            if (!isSmtpCheck) {
                return {
                    success: false,
                    message: 'Email address smtp check bounces.'
                };
            };
            return {
                success: true
            };
        };


        var objValidateInput = validateRequiredInput(payload);
        if (objValidateInput.result) {
            var data = {
                isEmailVerified: verifyValidEmail(payload),
                /*default - false*/ isDisposable: verifyDisposable(payload),
                /*default - true*/ isSmtpConnect: verifySmtpConnect(payload),
                /*default - true*/ isSmtpCheck: verifySmtpCheck(payload),
                /*default - true*/
            };
            var errorData = [];
            for (var propertyName in data) {
                var el = data[propertyName];
                if (!el.success) {
                    errorData.push(el.message);
                }
            };
            var status = true;
            if (errorData.length > 0) {
                status = false;
            }
            return {
                'result': status,
                'detail': errorData,
                'data': null
            };
        } else {
            return objValidateInput;
        }
    } catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'detail': errorData,
            'data': null
        };
    }
}


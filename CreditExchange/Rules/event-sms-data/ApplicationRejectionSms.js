function ApplicationRejectionSms(input) {
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText: 'Hi, unfortunately your loan application did not meet our qualifying criteria. However, you may be eligible for an RBL Credit Card, kindly apply here: https://goo.gl/cb18pH'
	};
}
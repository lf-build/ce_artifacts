function FinalOfferGeneratedSms(input){
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText : 'Hi, congratulations! your loan application has been approved. Please accept the loan offer by logging into your Qbera account. Regards, Team Qbera'
	};
}
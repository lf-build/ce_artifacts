function ConfirmSuccessfullApplicationSms(input) {
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText: 'Hi, Congratulations! Your Qbera personal loan application has been successfully submitted. We will get back to you within 24 hours with the application status.'
	};
}
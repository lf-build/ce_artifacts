function InitialOfferAndLeadSquaredApplicationSms(input) {
	if (input.EventData.InitialOffer.Status == 'Completed') {
		if (input.DataAttributes.application.source.SystemChannel == 'Leadsquared' && input.DataAttributes.application.source.SourceReferenceId != 'UpdateApplication') {
			return {
				RecipientPhone: input.DataAttributes.application.mobileNo,
				CommonSMSText: 'Hi ' + input.DataAttributes.application.firstName + ', our partner ' + input.DataAttributes.application.source.TrackingCode + ' has created a Qbera loan application for you. Please login using your mobile number here https://goo.gl/WgY6L7 and complete the steps to get a loan offer instantly.'
			};
		}
	}
	else {
		return {};
	}
}
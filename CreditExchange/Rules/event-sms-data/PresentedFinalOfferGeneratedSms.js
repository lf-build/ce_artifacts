function PresentedFinalOfferGeneratedSms(input) {
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText: 'Congratulations! Your Qbera loan has been approved. Please accept the offer by logging in: https://goo.gl/WgY6L7. Call us on 18004198121 for any queries'
	};
}
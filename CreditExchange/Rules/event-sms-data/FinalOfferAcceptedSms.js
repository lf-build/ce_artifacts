function FinalOfferAcceptedSms(input){
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText : 'Congrats, you have accepted the loan offer. We will contact you shortly for arranging the disbursal of the loan. Regards, Team Qbera'
	};
}
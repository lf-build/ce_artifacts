function PendingDocumentsSms(input){
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText : 'Hi, please upload your bank statement or connect your netbanking to your Qbera account so that we can process your loan application quickly. Regards, Team Qbera'
	};
}
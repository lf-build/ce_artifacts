function InActiveApplicationSms(input){
	return {
		RecipientPhone: input.DataAttributes.application.mobileNo,
		CommonSMSText : 'Please continue your loan application. It will only take 15 minutes to apply and get your loan offer instantly. Regards, Team Qbera'
	};
}
var result = db.getCollection('application').aggregate([
			{
				$match: {
					"ApplicationNumber": { $gt: "0000874" }
				}
			},
			{
				$sort: { "ApplicationNumber": 1 }
			},
			{
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
                                        "ResidenceType" : "$ResidenceType",
					"CurrentAddress": { $arrayElemAt: [ "$Addresses", 0 ] },
					"PermanantAddress": { $arrayElemAt: [ "$Addresses", 1 ] },
					"_id": 0
				}

			},
                        {
				$project: {
					"ApplicationNumber": "$ApplicationNumber",
					"Current AddressLine1": "$CurrentAddress.AddressLine1",
                                        "Current AddressLine1": "$CurrentAddress.AddressLine2",
                                        "Current City": "$CurrentAddress.City",
                                        "Current State": "$CurrentAddress.State",
                                        "Current PinCode": "$CurrentAddress.PinCode",
					"Permanant AddressLine1": "$CurrentAddress.AddressLine1",
                                        "Permanant AddressLine2": "$CurrentAddress.AddressLine2",
                                        "Permanant City": "$CurrentAddress.City",
                                        "Permanant State": "$CurrentAddress.State",
                                        "Permanant PinCode": "$CurrentAddress.PinCode",
					"_id": 0
				}

			}
                        ]).toArray()
                        
                         printjson(result);
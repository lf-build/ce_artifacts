let MongoClient = require('mongodb').MongoClient;
let express = require('express');
let bodyParser = require('body-parser');
let app = express();

const MONGODB_URL = "mongodb://54.227.251.126:27017/";
const DB_NAME = "application-filters";
const COLLECTION_NAME_1 = "application-filters";
const COLLECTION_NAME_2 = "application-filters-tags";
const MERGED_COLLECTION_NAME = "application-filters-merged";

app.use(bodyParser.json({ limit: '200mb' }));
app.use(bodyParser.text({ type: 'text/html', limit: '6mb' }));

const doAggregate = (db, callback) => {
    const cursor = db.collection(COLLECTION_NAME_1).aggregate([
        {
            $lookup: {
                from: COLLECTION_NAME_2, localField: "ApplicationNumber", foreignField: "ApplicationNumber", as: "Tags"
            }
        },
        {
            $unwind: { path: "$Tags", preserveNullAndEmptyArrays: true }
        },
        {
            $replaceRoot: { newRoot: { $mergeObjects: [{ tags: "$Tags.Tags" }, "$$ROOT"] } }
        },
        {
            $project: { Tags: 0 }
        },
        {
            $out: MERGED_COLLECTION_NAME
        }
    ], { allowDiskUse: true, bypassDocumentValidation: true });

    cursor.each((err, item) => {
        if (err) throw err;

        if (item === null) {
            callback();
        }
    });
};

const bulkUpdate = (db, callback) => {
    db.collection(MERGED_COLLECTION_NAME).updateMany({}, { $rename: { "tags": "Tags" } }, (err, result) => {
        if (err) throw err;
        callback(result);
    });
}

MongoClient.connect(MONGODB_URL, (err, client) => {
    if (err) throw err;
    const db = client.db(DB_NAME);

    app.route('/').get(function (req, res) {
        doAggregate(db, () => {
            bulkUpdate(db, (output) => {
                console.log("inside bulk insert callback");
                console.log(output);
                client.close();
                res.json({ message: "inserted successfully" });
            });
        });
    });

    app.listen(3000);
});
db.getCollection('application-filters').find({}, { _id:1, TenantId : 1, ApplicationNumber: 1, Tags : 1}).forEach(function(doc){
    var tags = doc.Tags;
    var newTags = [];
    for(var i = 0; i < tags.length; i++){
        newTags.push({TagName: tags[i], TaggedOn : Date.now()});
    }
    
    var newDoc = {
        _id : doc._id,
        TenantId : doc.TenantId,
        ApplicationNumber : doc.ApplicationNumber,
        Tags : newTags
    }
    db.getCollection('application-filters-tags').insert(newDoc);
});
db.getCollection('application-filters').update({}, {$unset: {Tags : 1}}, {multi:true});